# DevNaPraticaIi

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Locadora
Locadora:
	- Sistema:
		- Login pessoa;
	- Cliente:
		- Crud;
			- Campos:
				* Nome - Sobrenome;
				* Data de nascimento;
				* CPF;
				* Usuário;
				* Senha;
		- Alugar;
			- Montrar qual é o dia final de entrega juntamente com o valor para cada dia.
		- Lista de filmes alugados;
			
	- Estabelecimento:
		- Cancelar uma locação;
			- Deve solicitar confirmação para esta ação; (Exp: "Deseja realmente cancelar?")
		- Crud de produto (Insert, delete, update, select);
			- Campos:
				* Nome;
				* Tipo de locação;
				* Imagem;
				* Descrição;
				* Quantidade em estoque;
			- Tela de pesquisa;
				- Grid:
					- Nome do produto;
					- Quantidade;
					- Tipo de locação;
		- Crud de tipo de produto; (Exp: Lançamento, normal, premium)
			- Campos:
				* Descrição;
				* Valor;
				* Dias que podem ficar alugados;
			- Tela de pesquisa;
				- Descrição;
				- Valor;
		- Tela que mostre o estoque;
			- Grid:
				- Nome produto;
				- Quantidade dísponivel;
				- Quantidade alugado;
		- Locação:
			- Tela para realizar a locação;
				- Deve abrir uma tela com as seguintes informações:
					- Botão para selecionar os produtos;
						- Após feito a pesquisa deve mostrar as seguintes informações ao lado:
							* Nome produto;
							* Tipo de produto;
			- Tela para concluir com locação;
				- Deve ser possível pesquisar pelas locações e selecionar;
					- Abrir uma tela modal com as seguintes informações:
						* Nome do produto;
						* Quantidade;
						* Tipo de produto;
						* Dia que foi alugado - Data de entrega;
						* Valor a ser cobrado;
						- Deve vir com botão de confirmação e cancelamentgo;