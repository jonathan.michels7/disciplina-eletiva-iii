/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

/**
 * Trabalho furb
 */
public interface BasicoConstants {
    String DOMAIN = "furb";
    String SERVICE = "basico";
    
    interface Commands {
    	/**
    	 * Obtém a quantidade do produto no estoque
    	 * @see RetornaEstoqueInput the request payload
    	 */
    	String RETORNA_ESTOQUE = "retornaEstoque";
    	/**
    	 * The success response primitive for retornaEstoque.
    	 *
    	 * @see #RETORNA_ESTOQUE the request primitive
    	 * @see RetornaEstoqueOutput the response payload
    	 */
    	String RETORNA_ESTOQUE_RESPONSE = "retornaEstoqueResponse";
    	/**
    	 * An error response primitive for retornaEstoque.
    	 *
    	 * @see #RETORNA_ESTOQUE the request primitive
    	 */
    	String RETORNA_ESTOQUE_ERROR = "retornaEstoqueError";
    	/**
    	 * Busca produto conforme informações passadas
    	 * @see BuscaProdutoInput the request payload
    	 */
    	String BUSCA_PRODUTO = "buscaProduto";
    	/**
    	 * The success response primitive for buscaProduto.
    	 *
    	 * @see #BUSCA_PRODUTO the request primitive
    	 * @see BuscaProdutoOutput the response payload
    	 */
    	String BUSCA_PRODUTO_RESPONSE = "buscaProdutoResponse";
    	/**
    	 * An error response primitive for buscaProduto.
    	 *
    	 * @see #BUSCA_PRODUTO the request primitive
    	 */
    	String BUSCA_PRODUTO_ERROR = "buscaProdutoError";
    	/**
    	 * Busca usuario conforme nome
    	 * @see BuscaClienteInput the request payload
    	 */
    	String BUSCA_CLIENTE = "buscaCliente";
    	/**
    	 * The success response primitive for buscaCliente.
    	 *
    	 * @see #BUSCA_CLIENTE the request primitive
    	 * @see BuscaClienteOutput the response payload
    	 */
    	String BUSCA_CLIENTE_RESPONSE = "buscaClienteResponse";
    	/**
    	 * An error response primitive for buscaCliente.
    	 *
    	 * @see #BUSCA_CLIENTE the request primitive
    	 */
    	String BUSCA_CLIENTE_ERROR = "buscaClienteError";
    	/**
    	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
    	 * @see GetMetadataInput the request payload
    	 */
    	String GET_METADATA = "getMetadata";
    	/**
    	 * The success response primitive for getMetadata.
    	 *
    	 * @see #GET_METADATA the request primitive
    	 * @see GetMetadataOutput the response payload
    	 */
    	String GET_METADATA_RESPONSE = "getMetadataResponse";
    	/**
    	 * An error response primitive for getMetadata.
    	 *
    	 * @see #GET_METADATA the request primitive
    	 */
    	String GET_METADATA_ERROR = "getMetadataError";
    	/**
    	 * @see ImportLocadoraInput the request payload
    	 */
    	String IMPORT_LOCADORA = "importLocadora";
    	/**
    	 * The success response primitive for importLocadora.
    	 *
    	 * @see #IMPORT_LOCADORA the request primitive
    	 * @see ImportLocadoraOutput the response payload
    	 */
    	String IMPORT_LOCADORA_RESPONSE = "importLocadoraResponse";
    	/**
    	 * An error response primitive for importLocadora.
    	 *
    	 * @see #IMPORT_LOCADORA the request primitive
    	 */
    	String IMPORT_LOCADORA_ERROR = "importLocadoraError";
    	/**
    	 * @see ExportLocadoraInput the request payload
    	 */
    	String EXPORT_LOCADORA = "exportLocadora";
    	/**
    	 * The success response primitive for exportLocadora.
    	 *
    	 * @see #EXPORT_LOCADORA the request primitive
    	 * @see ExportLocadoraOutput the response payload
    	 */
    	String EXPORT_LOCADORA_RESPONSE = "exportLocadoraResponse";
    	/**
    	 * An error response primitive for exportLocadora.
    	 *
    	 * @see #EXPORT_LOCADORA the request primitive
    	 */
    	String EXPORT_LOCADORA_ERROR = "exportLocadoraError";
    	/**
    	 * @see ImportTipoUsuarioInput the request payload
    	 */
    	String IMPORT_TIPO_USUARIO = "importTipoUsuario";
    	/**
    	 * The success response primitive for importTipoUsuario.
    	 *
    	 * @see #IMPORT_TIPO_USUARIO the request primitive
    	 * @see ImportTipoUsuarioOutput the response payload
    	 */
    	String IMPORT_TIPO_USUARIO_RESPONSE = "importTipoUsuarioResponse";
    	/**
    	 * An error response primitive for importTipoUsuario.
    	 *
    	 * @see #IMPORT_TIPO_USUARIO the request primitive
    	 */
    	String IMPORT_TIPO_USUARIO_ERROR = "importTipoUsuarioError";
    	/**
    	 * @see ExportTipoUsuarioInput the request payload
    	 */
    	String EXPORT_TIPO_USUARIO = "exportTipoUsuario";
    	/**
    	 * The success response primitive for exportTipoUsuario.
    	 *
    	 * @see #EXPORT_TIPO_USUARIO the request primitive
    	 * @see ExportTipoUsuarioOutput the response payload
    	 */
    	String EXPORT_TIPO_USUARIO_RESPONSE = "exportTipoUsuarioResponse";
    	/**
    	 * An error response primitive for exportTipoUsuario.
    	 *
    	 * @see #EXPORT_TIPO_USUARIO the request primitive
    	 */
    	String EXPORT_TIPO_USUARIO_ERROR = "exportTipoUsuarioError";
    	/**
    	 * @see ImportClienteInput the request payload
    	 */
    	String IMPORT_CLIENTE = "importCliente";
    	/**
    	 * The success response primitive for importCliente.
    	 *
    	 * @see #IMPORT_CLIENTE the request primitive
    	 * @see ImportClienteOutput the response payload
    	 */
    	String IMPORT_CLIENTE_RESPONSE = "importClienteResponse";
    	/**
    	 * An error response primitive for importCliente.
    	 *
    	 * @see #IMPORT_CLIENTE the request primitive
    	 */
    	String IMPORT_CLIENTE_ERROR = "importClienteError";
    	/**
    	 * @see ExportClienteInput the request payload
    	 */
    	String EXPORT_CLIENTE = "exportCliente";
    	/**
    	 * The success response primitive for exportCliente.
    	 *
    	 * @see #EXPORT_CLIENTE the request primitive
    	 * @see ExportClienteOutput the response payload
    	 */
    	String EXPORT_CLIENTE_RESPONSE = "exportClienteResponse";
    	/**
    	 * An error response primitive for exportCliente.
    	 *
    	 * @see #EXPORT_CLIENTE the request primitive
    	 */
    	String EXPORT_CLIENTE_ERROR = "exportClienteError";
    	/**
    	 * @see ImportTipoProdutoInput the request payload
    	 */
    	String IMPORT_TIPO_PRODUTO = "importTipoProduto";
    	/**
    	 * The success response primitive for importTipoProduto.
    	 *
    	 * @see #IMPORT_TIPO_PRODUTO the request primitive
    	 * @see ImportTipoProdutoOutput the response payload
    	 */
    	String IMPORT_TIPO_PRODUTO_RESPONSE = "importTipoProdutoResponse";
    	/**
    	 * An error response primitive for importTipoProduto.
    	 *
    	 * @see #IMPORT_TIPO_PRODUTO the request primitive
    	 */
    	String IMPORT_TIPO_PRODUTO_ERROR = "importTipoProdutoError";
    	/**
    	 * @see ExportTipoProdutoInput the request payload
    	 */
    	String EXPORT_TIPO_PRODUTO = "exportTipoProduto";
    	/**
    	 * The success response primitive for exportTipoProduto.
    	 *
    	 * @see #EXPORT_TIPO_PRODUTO the request primitive
    	 * @see ExportTipoProdutoOutput the response payload
    	 */
    	String EXPORT_TIPO_PRODUTO_RESPONSE = "exportTipoProdutoResponse";
    	/**
    	 * An error response primitive for exportTipoProduto.
    	 *
    	 * @see #EXPORT_TIPO_PRODUTO the request primitive
    	 */
    	String EXPORT_TIPO_PRODUTO_ERROR = "exportTipoProdutoError";
    	/**
    	 * @see ImportProdutoInput the request payload
    	 */
    	String IMPORT_PRODUTO = "importProduto";
    	/**
    	 * The success response primitive for importProduto.
    	 *
    	 * @see #IMPORT_PRODUTO the request primitive
    	 * @see ImportProdutoOutput the response payload
    	 */
    	String IMPORT_PRODUTO_RESPONSE = "importProdutoResponse";
    	/**
    	 * An error response primitive for importProduto.
    	 *
    	 * @see #IMPORT_PRODUTO the request primitive
    	 */
    	String IMPORT_PRODUTO_ERROR = "importProdutoError";
    	/**
    	 * @see ExportProdutoInput the request payload
    	 */
    	String EXPORT_PRODUTO = "exportProduto";
    	/**
    	 * The success response primitive for exportProduto.
    	 *
    	 * @see #EXPORT_PRODUTO the request primitive
    	 * @see ExportProdutoOutput the response payload
    	 */
    	String EXPORT_PRODUTO_RESPONSE = "exportProdutoResponse";
    	/**
    	 * An error response primitive for exportProduto.
    	 *
    	 * @see #EXPORT_PRODUTO the request primitive
    	 */
    	String EXPORT_PRODUTO_ERROR = "exportProdutoError";
    	/**
    	 * @see ImportProduto_alugadoInput the request payload
    	 */
    	String IMPORT_PRODUTOALUGADO = "importProduto_alugado";
    	/**
    	 * The success response primitive for importProduto_alugado.
    	 *
    	 * @see #IMPORT_PRODUTOALUGADO the request primitive
    	 * @see ImportProduto_alugadoOutput the response payload
    	 */
    	String IMPORT_PRODUTOALUGADO_RESPONSE = "importProduto_alugadoResponse";
    	/**
    	 * An error response primitive for importProduto_alugado.
    	 *
    	 * @see #IMPORT_PRODUTOALUGADO the request primitive
    	 */
    	String IMPORT_PRODUTOALUGADO_ERROR = "importProduto_alugadoError";
    	/**
    	 * @see ExportProduto_alugadoInput the request payload
    	 */
    	String EXPORT_PRODUTOALUGADO = "exportProduto_alugado";
    	/**
    	 * The success response primitive for exportProduto_alugado.
    	 *
    	 * @see #EXPORT_PRODUTOALUGADO the request primitive
    	 * @see ExportProduto_alugadoOutput the response payload
    	 */
    	String EXPORT_PRODUTOALUGADO_RESPONSE = "exportProduto_alugadoResponse";
    	/**
    	 * An error response primitive for exportProduto_alugado.
    	 *
    	 * @see #EXPORT_PRODUTOALUGADO the request primitive
    	 */
    	String EXPORT_PRODUTOALUGADO_ERROR = "exportProduto_alugadoError";
    	/**
    	 * @see ImportLocacaoInput the request payload
    	 */
    	String IMPORT_LOCACAO = "importLocacao";
    	/**
    	 * The success response primitive for importLocacao.
    	 *
    	 * @see #IMPORT_LOCACAO the request primitive
    	 * @see ImportLocacaoOutput the response payload
    	 */
    	String IMPORT_LOCACAO_RESPONSE = "importLocacaoResponse";
    	/**
    	 * An error response primitive for importLocacao.
    	 *
    	 * @see #IMPORT_LOCACAO the request primitive
    	 */
    	String IMPORT_LOCACAO_ERROR = "importLocacaoError";
    	/**
    	 * @see ExportLocacaoInput the request payload
    	 */
    	String EXPORT_LOCACAO = "exportLocacao";
    	/**
    	 * The success response primitive for exportLocacao.
    	 *
    	 * @see #EXPORT_LOCACAO the request primitive
    	 * @see ExportLocacaoOutput the response payload
    	 */
    	String EXPORT_LOCACAO_RESPONSE = "exportLocacaoResponse";
    	/**
    	 * An error response primitive for exportLocacao.
    	 *
    	 * @see #EXPORT_LOCACAO the request primitive
    	 */
    	String EXPORT_LOCACAO_ERROR = "exportLocacaoError";
    	/**
    	 * The 'list' request primitive for the Locadora entity.
    	 * @see locadora.pageRequest the request payload
    	 */
    	String LIST_LOCADORA = "listLocadora";
    	/**
    	 * The success response primitive for listLocadora.
    	 *
    	 * @see #LIST_LOCADORA the request primitive
    	 * @see Locadora.PagedResults the response payload
    	 */
    	String LIST_LOCADORA_RESPONSE = "listLocadoraResponse";
    	/**
    	 * An error response primitive for listLocadora.
    	 *
    	 * @see #LIST_LOCADORA the request primitive
    	 */
    	String LIST_LOCADORA_ERROR = "listLocadoraError";
    	/**
    	 * The 'list' request primitive for the TipoUsuario entity.
    	 * @see tipoUsuario.pageRequest the request payload
    	 */
    	String LIST_TIPO_USUARIO = "listTipoUsuario";
    	/**
    	 * The success response primitive for listTipoUsuario.
    	 *
    	 * @see #LIST_TIPO_USUARIO the request primitive
    	 * @see TipoUsuario.PagedResults the response payload
    	 */
    	String LIST_TIPO_USUARIO_RESPONSE = "listTipoUsuarioResponse";
    	/**
    	 * An error response primitive for listTipoUsuario.
    	 *
    	 * @see #LIST_TIPO_USUARIO the request primitive
    	 */
    	String LIST_TIPO_USUARIO_ERROR = "listTipoUsuarioError";
    	/**
    	 * The 'list' request primitive for the Cliente entity.
    	 * @see cliente.pageRequest the request payload
    	 */
    	String LIST_CLIENTE = "listCliente";
    	/**
    	 * The success response primitive for listCliente.
    	 *
    	 * @see #LIST_CLIENTE the request primitive
    	 * @see Cliente.PagedResults the response payload
    	 */
    	String LIST_CLIENTE_RESPONSE = "listClienteResponse";
    	/**
    	 * An error response primitive for listCliente.
    	 *
    	 * @see #LIST_CLIENTE the request primitive
    	 */
    	String LIST_CLIENTE_ERROR = "listClienteError";
    	/**
    	 * The 'list' request primitive for the TipoProduto entity.
    	 * @see tipoProduto.pageRequest the request payload
    	 */
    	String LIST_TIPO_PRODUTO = "listTipoProduto";
    	/**
    	 * The success response primitive for listTipoProduto.
    	 *
    	 * @see #LIST_TIPO_PRODUTO the request primitive
    	 * @see TipoProduto.PagedResults the response payload
    	 */
    	String LIST_TIPO_PRODUTO_RESPONSE = "listTipoProdutoResponse";
    	/**
    	 * An error response primitive for listTipoProduto.
    	 *
    	 * @see #LIST_TIPO_PRODUTO the request primitive
    	 */
    	String LIST_TIPO_PRODUTO_ERROR = "listTipoProdutoError";
    	/**
    	 * The 'list' request primitive for the Produto entity.
    	 * @see produto.pageRequest the request payload
    	 */
    	String LIST_PRODUTO = "listProduto";
    	/**
    	 * The success response primitive for listProduto.
    	 *
    	 * @see #LIST_PRODUTO the request primitive
    	 * @see Produto.PagedResults the response payload
    	 */
    	String LIST_PRODUTO_RESPONSE = "listProdutoResponse";
    	/**
    	 * An error response primitive for listProduto.
    	 *
    	 * @see #LIST_PRODUTO the request primitive
    	 */
    	String LIST_PRODUTO_ERROR = "listProdutoError";
    	/**
    	 * The 'list' request primitive for the Produto_alugado entity.
    	 * @see produto_alugado.pageRequest the request payload
    	 */
    	String LIST_PRODUTOALUGADO = "listProduto_alugado";
    	/**
    	 * The success response primitive for listProduto_alugado.
    	 *
    	 * @see #LIST_PRODUTOALUGADO the request primitive
    	 * @see Produto_alugado.PagedResults the response payload
    	 */
    	String LIST_PRODUTOALUGADO_RESPONSE = "listProduto_alugadoResponse";
    	/**
    	 * An error response primitive for listProduto_alugado.
    	 *
    	 * @see #LIST_PRODUTOALUGADO the request primitive
    	 */
    	String LIST_PRODUTOALUGADO_ERROR = "listProduto_alugadoError";
    	/**
    	 * The 'list' request primitive for the Locacao entity.
    	 * @see locacao.pageRequest the request payload
    	 */
    	String LIST_LOCACAO = "listLocacao";
    	/**
    	 * The success response primitive for listLocacao.
    	 *
    	 * @see #LIST_LOCACAO the request primitive
    	 * @see Locacao.PagedResults the response payload
    	 */
    	String LIST_LOCACAO_RESPONSE = "listLocacaoResponse";
    	/**
    	 * An error response primitive for listLocacao.
    	 *
    	 * @see #LIST_LOCACAO the request primitive
    	 */
    	String LIST_LOCACAO_ERROR = "listLocacaoError";
    	/**
    	 * Returns a list with all dependencies from this service, along with their respective versions
    	 */
    	String GET_DEPENDENCIES = "getDependencies";
    	/**
    	 * The success response primitive for getDependencies.
    	 *
    	 * @see #GET_DEPENDENCIES the request primitive
    	 * @see GetDependenciesOutput the response payload
    	 */
    	String GET_DEPENDENCIES_RESPONSE = "getDependenciesResponse";
    	/**
    	 * An error response primitive for getDependencies.
    	 *
    	 * @see #GET_DEPENDENCIES the request primitive
    	 */
    	String GET_DEPENDENCIES_ERROR = "getDependenciesError";
    	/**
    	 * The 'create' request primitive for the Locadora entity.
    	 * @see Locadora the request payload
    	 */
    	String CREATE_LOCADORA = "createLocadora";
    	/**
    	 * The success response primitive for createLocadora.
    	 *
    	 * @see #CREATE_LOCADORA the request primitive
    	 */
    	String CREATE_LOCADORA_RESPONSE = "createLocadoraResponse";
    	/**
    	 * An error response primitive for createLocadora.
    	 *
    	 * @see #CREATE_LOCADORA the request primitive
    	 */
    	String CREATE_LOCADORA_ERROR = "createLocadoraError";
    	/**
    	 * The 'createBulk' request primitive for the Locadora entity.
    	 * @see CreateBulkLocadoraInput the request payload
    	 */
    	String CREATE_BULK_LOCADORA = "createBulkLocadora";
    	/**
    	 * The success response primitive for createBulkLocadora.
    	 *
    	 * @see #CREATE_BULK_LOCADORA the request primitive
    	 * @see CreateBulkLocadoraOutput the response payload
    	 */
    	String CREATE_BULK_LOCADORA_RESPONSE = "createBulkLocadoraResponse";
    	/**
    	 * An error response primitive for createBulkLocadora.
    	 *
    	 * @see #CREATE_BULK_LOCADORA the request primitive
    	 */
    	String CREATE_BULK_LOCADORA_ERROR = "createBulkLocadoraError";
    	/**
    	 * The 'createMerge' request primitive for the Locadora entity.
    	 * @see Locadora the request payload
    	 */
    	String CREATE_MERGE_LOCADORA = "createMergeLocadora";
    	/**
    	 * The success response primitive for createMergeLocadora.
    	 *
    	 * @see #CREATE_MERGE_LOCADORA the request primitive
    	 */
    	String CREATE_MERGE_LOCADORA_RESPONSE = "createMergeLocadoraResponse";
    	/**
    	 * An error response primitive for createMergeLocadora.
    	 *
    	 * @see #CREATE_MERGE_LOCADORA the request primitive
    	 */
    	String CREATE_MERGE_LOCADORA_ERROR = "createMergeLocadoraError";
    	/**
    	 * The 'retrieve' request primitive for the Locadora entity.
    	 * @see Locadora.Id the request payload
    	 */
    	String RETRIEVE_LOCADORA = "retrieveLocadora";
    	/**
    	 * The success response primitive for retrieveLocadora.
    	 *
    	 * @see #RETRIEVE_LOCADORA the request primitive
    	 */
    	String RETRIEVE_LOCADORA_RESPONSE = "retrieveLocadoraResponse";
    	/**
    	 * An error response primitive for retrieveLocadora.
    	 *
    	 * @see #RETRIEVE_LOCADORA the request primitive
    	 */
    	String RETRIEVE_LOCADORA_ERROR = "retrieveLocadoraError";
    	/**
    	 * The 'update' request primitive for the Locadora entity.
    	 * @see Locadora the request payload
    	 */
    	String UPDATE_LOCADORA = "updateLocadora";
    	/**
    	 * The success response primitive for updateLocadora.
    	 *
    	 * @see #UPDATE_LOCADORA the request primitive
    	 */
    	String UPDATE_LOCADORA_RESPONSE = "updateLocadoraResponse";
    	/**
    	 * An error response primitive for updateLocadora.
    	 *
    	 * @see #UPDATE_LOCADORA the request primitive
    	 */
    	String UPDATE_LOCADORA_ERROR = "updateLocadoraError";
    	/**
    	 * The 'updateMerge' request primitive for the Locadora entity.
    	 * @see Locadora the request payload
    	 */
    	String UPDATE_MERGE_LOCADORA = "updateMergeLocadora";
    	/**
    	 * The success response primitive for updateMergeLocadora.
    	 *
    	 * @see #UPDATE_MERGE_LOCADORA the request primitive
    	 */
    	String UPDATE_MERGE_LOCADORA_RESPONSE = "updateMergeLocadoraResponse";
    	/**
    	 * An error response primitive for updateMergeLocadora.
    	 *
    	 * @see #UPDATE_MERGE_LOCADORA the request primitive
    	 */
    	String UPDATE_MERGE_LOCADORA_ERROR = "updateMergeLocadoraError";
    	/**
    	 * The 'delete' request primitive for the Locadora entity.
    	 * @see Locadora.Id the request payload
    	 */
    	String DELETE_LOCADORA = "deleteLocadora";
    	/**
    	 * The success response primitive for deleteLocadora.
    	 *
    	 * @see #DELETE_LOCADORA the request primitive
    	 */
    	String DELETE_LOCADORA_RESPONSE = "deleteLocadoraResponse";
    	/**
    	 * An error response primitive for deleteLocadora.
    	 *
    	 * @see #DELETE_LOCADORA the request primitive
    	 */
    	String DELETE_LOCADORA_ERROR = "deleteLocadoraError";
    	/**
    	 * The 'create' request primitive for the TipoUsuario entity.
    	 * @see TipoUsuario the request payload
    	 */
    	String CREATE_TIPO_USUARIO = "createTipoUsuario";
    	/**
    	 * The success response primitive for createTipoUsuario.
    	 *
    	 * @see #CREATE_TIPO_USUARIO the request primitive
    	 */
    	String CREATE_TIPO_USUARIO_RESPONSE = "createTipoUsuarioResponse";
    	/**
    	 * An error response primitive for createTipoUsuario.
    	 *
    	 * @see #CREATE_TIPO_USUARIO the request primitive
    	 */
    	String CREATE_TIPO_USUARIO_ERROR = "createTipoUsuarioError";
    	/**
    	 * The 'createBulk' request primitive for the TipoUsuario entity.
    	 * @see CreateBulkTipoUsuarioInput the request payload
    	 */
    	String CREATE_BULK_TIPO_USUARIO = "createBulkTipoUsuario";
    	/**
    	 * The success response primitive for createBulkTipoUsuario.
    	 *
    	 * @see #CREATE_BULK_TIPO_USUARIO the request primitive
    	 * @see CreateBulkTipoUsuarioOutput the response payload
    	 */
    	String CREATE_BULK_TIPO_USUARIO_RESPONSE = "createBulkTipoUsuarioResponse";
    	/**
    	 * An error response primitive for createBulkTipoUsuario.
    	 *
    	 * @see #CREATE_BULK_TIPO_USUARIO the request primitive
    	 */
    	String CREATE_BULK_TIPO_USUARIO_ERROR = "createBulkTipoUsuarioError";
    	/**
    	 * The 'createMerge' request primitive for the TipoUsuario entity.
    	 * @see TipoUsuario the request payload
    	 */
    	String CREATE_MERGE_TIPO_USUARIO = "createMergeTipoUsuario";
    	/**
    	 * The success response primitive for createMergeTipoUsuario.
    	 *
    	 * @see #CREATE_MERGE_TIPO_USUARIO the request primitive
    	 */
    	String CREATE_MERGE_TIPO_USUARIO_RESPONSE = "createMergeTipoUsuarioResponse";
    	/**
    	 * An error response primitive for createMergeTipoUsuario.
    	 *
    	 * @see #CREATE_MERGE_TIPO_USUARIO the request primitive
    	 */
    	String CREATE_MERGE_TIPO_USUARIO_ERROR = "createMergeTipoUsuarioError";
    	/**
    	 * The 'retrieve' request primitive for the TipoUsuario entity.
    	 * @see TipoUsuario.Id the request payload
    	 */
    	String RETRIEVE_TIPO_USUARIO = "retrieveTipoUsuario";
    	/**
    	 * The success response primitive for retrieveTipoUsuario.
    	 *
    	 * @see #RETRIEVE_TIPO_USUARIO the request primitive
    	 */
    	String RETRIEVE_TIPO_USUARIO_RESPONSE = "retrieveTipoUsuarioResponse";
    	/**
    	 * An error response primitive for retrieveTipoUsuario.
    	 *
    	 * @see #RETRIEVE_TIPO_USUARIO the request primitive
    	 */
    	String RETRIEVE_TIPO_USUARIO_ERROR = "retrieveTipoUsuarioError";
    	/**
    	 * The 'update' request primitive for the TipoUsuario entity.
    	 * @see TipoUsuario the request payload
    	 */
    	String UPDATE_TIPO_USUARIO = "updateTipoUsuario";
    	/**
    	 * The success response primitive for updateTipoUsuario.
    	 *
    	 * @see #UPDATE_TIPO_USUARIO the request primitive
    	 */
    	String UPDATE_TIPO_USUARIO_RESPONSE = "updateTipoUsuarioResponse";
    	/**
    	 * An error response primitive for updateTipoUsuario.
    	 *
    	 * @see #UPDATE_TIPO_USUARIO the request primitive
    	 */
    	String UPDATE_TIPO_USUARIO_ERROR = "updateTipoUsuarioError";
    	/**
    	 * The 'updateMerge' request primitive for the TipoUsuario entity.
    	 * @see TipoUsuario the request payload
    	 */
    	String UPDATE_MERGE_TIPO_USUARIO = "updateMergeTipoUsuario";
    	/**
    	 * The success response primitive for updateMergeTipoUsuario.
    	 *
    	 * @see #UPDATE_MERGE_TIPO_USUARIO the request primitive
    	 */
    	String UPDATE_MERGE_TIPO_USUARIO_RESPONSE = "updateMergeTipoUsuarioResponse";
    	/**
    	 * An error response primitive for updateMergeTipoUsuario.
    	 *
    	 * @see #UPDATE_MERGE_TIPO_USUARIO the request primitive
    	 */
    	String UPDATE_MERGE_TIPO_USUARIO_ERROR = "updateMergeTipoUsuarioError";
    	/**
    	 * The 'delete' request primitive for the TipoUsuario entity.
    	 * @see TipoUsuario.Id the request payload
    	 */
    	String DELETE_TIPO_USUARIO = "deleteTipoUsuario";
    	/**
    	 * The success response primitive for deleteTipoUsuario.
    	 *
    	 * @see #DELETE_TIPO_USUARIO the request primitive
    	 */
    	String DELETE_TIPO_USUARIO_RESPONSE = "deleteTipoUsuarioResponse";
    	/**
    	 * An error response primitive for deleteTipoUsuario.
    	 *
    	 * @see #DELETE_TIPO_USUARIO the request primitive
    	 */
    	String DELETE_TIPO_USUARIO_ERROR = "deleteTipoUsuarioError";
    	/**
    	 * The 'create' request primitive for the Cliente entity.
    	 * @see Cliente the request payload
    	 */
    	String CREATE_CLIENTE = "createCliente";
    	/**
    	 * The success response primitive for createCliente.
    	 *
    	 * @see #CREATE_CLIENTE the request primitive
    	 */
    	String CREATE_CLIENTE_RESPONSE = "createClienteResponse";
    	/**
    	 * An error response primitive for createCliente.
    	 *
    	 * @see #CREATE_CLIENTE the request primitive
    	 */
    	String CREATE_CLIENTE_ERROR = "createClienteError";
    	/**
    	 * The 'createBulk' request primitive for the Cliente entity.
    	 * @see CreateBulkClienteInput the request payload
    	 */
    	String CREATE_BULK_CLIENTE = "createBulkCliente";
    	/**
    	 * The success response primitive for createBulkCliente.
    	 *
    	 * @see #CREATE_BULK_CLIENTE the request primitive
    	 * @see CreateBulkClienteOutput the response payload
    	 */
    	String CREATE_BULK_CLIENTE_RESPONSE = "createBulkClienteResponse";
    	/**
    	 * An error response primitive for createBulkCliente.
    	 *
    	 * @see #CREATE_BULK_CLIENTE the request primitive
    	 */
    	String CREATE_BULK_CLIENTE_ERROR = "createBulkClienteError";
    	/**
    	 * The 'createMerge' request primitive for the Cliente entity.
    	 * @see Cliente the request payload
    	 */
    	String CREATE_MERGE_CLIENTE = "createMergeCliente";
    	/**
    	 * The success response primitive for createMergeCliente.
    	 *
    	 * @see #CREATE_MERGE_CLIENTE the request primitive
    	 */
    	String CREATE_MERGE_CLIENTE_RESPONSE = "createMergeClienteResponse";
    	/**
    	 * An error response primitive for createMergeCliente.
    	 *
    	 * @see #CREATE_MERGE_CLIENTE the request primitive
    	 */
    	String CREATE_MERGE_CLIENTE_ERROR = "createMergeClienteError";
    	/**
    	 * The 'retrieve' request primitive for the Cliente entity.
    	 * @see Cliente.Id the request payload
    	 */
    	String RETRIEVE_CLIENTE = "retrieveCliente";
    	/**
    	 * The success response primitive for retrieveCliente.
    	 *
    	 * @see #RETRIEVE_CLIENTE the request primitive
    	 */
    	String RETRIEVE_CLIENTE_RESPONSE = "retrieveClienteResponse";
    	/**
    	 * An error response primitive for retrieveCliente.
    	 *
    	 * @see #RETRIEVE_CLIENTE the request primitive
    	 */
    	String RETRIEVE_CLIENTE_ERROR = "retrieveClienteError";
    	/**
    	 * The 'update' request primitive for the Cliente entity.
    	 * @see Cliente the request payload
    	 */
    	String UPDATE_CLIENTE = "updateCliente";
    	/**
    	 * The success response primitive for updateCliente.
    	 *
    	 * @see #UPDATE_CLIENTE the request primitive
    	 */
    	String UPDATE_CLIENTE_RESPONSE = "updateClienteResponse";
    	/**
    	 * An error response primitive for updateCliente.
    	 *
    	 * @see #UPDATE_CLIENTE the request primitive
    	 */
    	String UPDATE_CLIENTE_ERROR = "updateClienteError";
    	/**
    	 * The 'updateMerge' request primitive for the Cliente entity.
    	 * @see Cliente the request payload
    	 */
    	String UPDATE_MERGE_CLIENTE = "updateMergeCliente";
    	/**
    	 * The success response primitive for updateMergeCliente.
    	 *
    	 * @see #UPDATE_MERGE_CLIENTE the request primitive
    	 */
    	String UPDATE_MERGE_CLIENTE_RESPONSE = "updateMergeClienteResponse";
    	/**
    	 * An error response primitive for updateMergeCliente.
    	 *
    	 * @see #UPDATE_MERGE_CLIENTE the request primitive
    	 */
    	String UPDATE_MERGE_CLIENTE_ERROR = "updateMergeClienteError";
    	/**
    	 * The 'delete' request primitive for the Cliente entity.
    	 * @see Cliente.Id the request payload
    	 */
    	String DELETE_CLIENTE = "deleteCliente";
    	/**
    	 * The success response primitive for deleteCliente.
    	 *
    	 * @see #DELETE_CLIENTE the request primitive
    	 */
    	String DELETE_CLIENTE_RESPONSE = "deleteClienteResponse";
    	/**
    	 * An error response primitive for deleteCliente.
    	 *
    	 * @see #DELETE_CLIENTE the request primitive
    	 */
    	String DELETE_CLIENTE_ERROR = "deleteClienteError";
    	/**
    	 * The 'create' request primitive for the TipoProduto entity.
    	 * @see TipoProduto the request payload
    	 */
    	String CREATE_TIPO_PRODUTO = "createTipoProduto";
    	/**
    	 * The success response primitive for createTipoProduto.
    	 *
    	 * @see #CREATE_TIPO_PRODUTO the request primitive
    	 */
    	String CREATE_TIPO_PRODUTO_RESPONSE = "createTipoProdutoResponse";
    	/**
    	 * An error response primitive for createTipoProduto.
    	 *
    	 * @see #CREATE_TIPO_PRODUTO the request primitive
    	 */
    	String CREATE_TIPO_PRODUTO_ERROR = "createTipoProdutoError";
    	/**
    	 * The 'createBulk' request primitive for the TipoProduto entity.
    	 * @see CreateBulkTipoProdutoInput the request payload
    	 */
    	String CREATE_BULK_TIPO_PRODUTO = "createBulkTipoProduto";
    	/**
    	 * The success response primitive for createBulkTipoProduto.
    	 *
    	 * @see #CREATE_BULK_TIPO_PRODUTO the request primitive
    	 * @see CreateBulkTipoProdutoOutput the response payload
    	 */
    	String CREATE_BULK_TIPO_PRODUTO_RESPONSE = "createBulkTipoProdutoResponse";
    	/**
    	 * An error response primitive for createBulkTipoProduto.
    	 *
    	 * @see #CREATE_BULK_TIPO_PRODUTO the request primitive
    	 */
    	String CREATE_BULK_TIPO_PRODUTO_ERROR = "createBulkTipoProdutoError";
    	/**
    	 * The 'createMerge' request primitive for the TipoProduto entity.
    	 * @see TipoProduto the request payload
    	 */
    	String CREATE_MERGE_TIPO_PRODUTO = "createMergeTipoProduto";
    	/**
    	 * The success response primitive for createMergeTipoProduto.
    	 *
    	 * @see #CREATE_MERGE_TIPO_PRODUTO the request primitive
    	 */
    	String CREATE_MERGE_TIPO_PRODUTO_RESPONSE = "createMergeTipoProdutoResponse";
    	/**
    	 * An error response primitive for createMergeTipoProduto.
    	 *
    	 * @see #CREATE_MERGE_TIPO_PRODUTO the request primitive
    	 */
    	String CREATE_MERGE_TIPO_PRODUTO_ERROR = "createMergeTipoProdutoError";
    	/**
    	 * The 'retrieve' request primitive for the TipoProduto entity.
    	 * @see TipoProduto.Id the request payload
    	 */
    	String RETRIEVE_TIPO_PRODUTO = "retrieveTipoProduto";
    	/**
    	 * The success response primitive for retrieveTipoProduto.
    	 *
    	 * @see #RETRIEVE_TIPO_PRODUTO the request primitive
    	 */
    	String RETRIEVE_TIPO_PRODUTO_RESPONSE = "retrieveTipoProdutoResponse";
    	/**
    	 * An error response primitive for retrieveTipoProduto.
    	 *
    	 * @see #RETRIEVE_TIPO_PRODUTO the request primitive
    	 */
    	String RETRIEVE_TIPO_PRODUTO_ERROR = "retrieveTipoProdutoError";
    	/**
    	 * The 'update' request primitive for the TipoProduto entity.
    	 * @see TipoProduto the request payload
    	 */
    	String UPDATE_TIPO_PRODUTO = "updateTipoProduto";
    	/**
    	 * The success response primitive for updateTipoProduto.
    	 *
    	 * @see #UPDATE_TIPO_PRODUTO the request primitive
    	 */
    	String UPDATE_TIPO_PRODUTO_RESPONSE = "updateTipoProdutoResponse";
    	/**
    	 * An error response primitive for updateTipoProduto.
    	 *
    	 * @see #UPDATE_TIPO_PRODUTO the request primitive
    	 */
    	String UPDATE_TIPO_PRODUTO_ERROR = "updateTipoProdutoError";
    	/**
    	 * The 'updateMerge' request primitive for the TipoProduto entity.
    	 * @see TipoProduto the request payload
    	 */
    	String UPDATE_MERGE_TIPO_PRODUTO = "updateMergeTipoProduto";
    	/**
    	 * The success response primitive for updateMergeTipoProduto.
    	 *
    	 * @see #UPDATE_MERGE_TIPO_PRODUTO the request primitive
    	 */
    	String UPDATE_MERGE_TIPO_PRODUTO_RESPONSE = "updateMergeTipoProdutoResponse";
    	/**
    	 * An error response primitive for updateMergeTipoProduto.
    	 *
    	 * @see #UPDATE_MERGE_TIPO_PRODUTO the request primitive
    	 */
    	String UPDATE_MERGE_TIPO_PRODUTO_ERROR = "updateMergeTipoProdutoError";
    	/**
    	 * The 'delete' request primitive for the TipoProduto entity.
    	 * @see TipoProduto.Id the request payload
    	 */
    	String DELETE_TIPO_PRODUTO = "deleteTipoProduto";
    	/**
    	 * The success response primitive for deleteTipoProduto.
    	 *
    	 * @see #DELETE_TIPO_PRODUTO the request primitive
    	 */
    	String DELETE_TIPO_PRODUTO_RESPONSE = "deleteTipoProdutoResponse";
    	/**
    	 * An error response primitive for deleteTipoProduto.
    	 *
    	 * @see #DELETE_TIPO_PRODUTO the request primitive
    	 */
    	String DELETE_TIPO_PRODUTO_ERROR = "deleteTipoProdutoError";
    	/**
    	 * The 'create' request primitive for the Produto entity.
    	 * @see Produto the request payload
    	 */
    	String CREATE_PRODUTO = "createProduto";
    	/**
    	 * The success response primitive for createProduto.
    	 *
    	 * @see #CREATE_PRODUTO the request primitive
    	 */
    	String CREATE_PRODUTO_RESPONSE = "createProdutoResponse";
    	/**
    	 * An error response primitive for createProduto.
    	 *
    	 * @see #CREATE_PRODUTO the request primitive
    	 */
    	String CREATE_PRODUTO_ERROR = "createProdutoError";
    	/**
    	 * The 'createBulk' request primitive for the Produto entity.
    	 * @see CreateBulkProdutoInput the request payload
    	 */
    	String CREATE_BULK_PRODUTO = "createBulkProduto";
    	/**
    	 * The success response primitive for createBulkProduto.
    	 *
    	 * @see #CREATE_BULK_PRODUTO the request primitive
    	 * @see CreateBulkProdutoOutput the response payload
    	 */
    	String CREATE_BULK_PRODUTO_RESPONSE = "createBulkProdutoResponse";
    	/**
    	 * An error response primitive for createBulkProduto.
    	 *
    	 * @see #CREATE_BULK_PRODUTO the request primitive
    	 */
    	String CREATE_BULK_PRODUTO_ERROR = "createBulkProdutoError";
    	/**
    	 * The 'createMerge' request primitive for the Produto entity.
    	 * @see Produto the request payload
    	 */
    	String CREATE_MERGE_PRODUTO = "createMergeProduto";
    	/**
    	 * The success response primitive for createMergeProduto.
    	 *
    	 * @see #CREATE_MERGE_PRODUTO the request primitive
    	 */
    	String CREATE_MERGE_PRODUTO_RESPONSE = "createMergeProdutoResponse";
    	/**
    	 * An error response primitive for createMergeProduto.
    	 *
    	 * @see #CREATE_MERGE_PRODUTO the request primitive
    	 */
    	String CREATE_MERGE_PRODUTO_ERROR = "createMergeProdutoError";
    	/**
    	 * The 'retrieve' request primitive for the Produto entity.
    	 * @see Produto.Id the request payload
    	 */
    	String RETRIEVE_PRODUTO = "retrieveProduto";
    	/**
    	 * The success response primitive for retrieveProduto.
    	 *
    	 * @see #RETRIEVE_PRODUTO the request primitive
    	 */
    	String RETRIEVE_PRODUTO_RESPONSE = "retrieveProdutoResponse";
    	/**
    	 * An error response primitive for retrieveProduto.
    	 *
    	 * @see #RETRIEVE_PRODUTO the request primitive
    	 */
    	String RETRIEVE_PRODUTO_ERROR = "retrieveProdutoError";
    	/**
    	 * The 'update' request primitive for the Produto entity.
    	 * @see Produto the request payload
    	 */
    	String UPDATE_PRODUTO = "updateProduto";
    	/**
    	 * The success response primitive for updateProduto.
    	 *
    	 * @see #UPDATE_PRODUTO the request primitive
    	 */
    	String UPDATE_PRODUTO_RESPONSE = "updateProdutoResponse";
    	/**
    	 * An error response primitive for updateProduto.
    	 *
    	 * @see #UPDATE_PRODUTO the request primitive
    	 */
    	String UPDATE_PRODUTO_ERROR = "updateProdutoError";
    	/**
    	 * The 'updateMerge' request primitive for the Produto entity.
    	 * @see Produto the request payload
    	 */
    	String UPDATE_MERGE_PRODUTO = "updateMergeProduto";
    	/**
    	 * The success response primitive for updateMergeProduto.
    	 *
    	 * @see #UPDATE_MERGE_PRODUTO the request primitive
    	 */
    	String UPDATE_MERGE_PRODUTO_RESPONSE = "updateMergeProdutoResponse";
    	/**
    	 * An error response primitive for updateMergeProduto.
    	 *
    	 * @see #UPDATE_MERGE_PRODUTO the request primitive
    	 */
    	String UPDATE_MERGE_PRODUTO_ERROR = "updateMergeProdutoError";
    	/**
    	 * The 'delete' request primitive for the Produto entity.
    	 * @see Produto.Id the request payload
    	 */
    	String DELETE_PRODUTO = "deleteProduto";
    	/**
    	 * The success response primitive for deleteProduto.
    	 *
    	 * @see #DELETE_PRODUTO the request primitive
    	 */
    	String DELETE_PRODUTO_RESPONSE = "deleteProdutoResponse";
    	/**
    	 * An error response primitive for deleteProduto.
    	 *
    	 * @see #DELETE_PRODUTO the request primitive
    	 */
    	String DELETE_PRODUTO_ERROR = "deleteProdutoError";
    	/**
    	 * The 'create' request primitive for the Produto_alugado entity.
    	 * @see Produto_alugado the request payload
    	 */
    	String CREATE_PRODUTOALUGADO = "createProduto_alugado";
    	/**
    	 * The success response primitive for createProduto_alugado.
    	 *
    	 * @see #CREATE_PRODUTOALUGADO the request primitive
    	 */
    	String CREATE_PRODUTOALUGADO_RESPONSE = "createProduto_alugadoResponse";
    	/**
    	 * An error response primitive for createProduto_alugado.
    	 *
    	 * @see #CREATE_PRODUTOALUGADO the request primitive
    	 */
    	String CREATE_PRODUTOALUGADO_ERROR = "createProduto_alugadoError";
    	/**
    	 * The 'createBulk' request primitive for the Produto_alugado entity.
    	 * @see CreateBulkProduto_alugadoInput the request payload
    	 */
    	String CREATE_BULK_PRODUTOALUGADO = "createBulkProduto_alugado";
    	/**
    	 * The success response primitive for createBulkProduto_alugado.
    	 *
    	 * @see #CREATE_BULK_PRODUTOALUGADO the request primitive
    	 * @see CreateBulkProduto_alugadoOutput the response payload
    	 */
    	String CREATE_BULK_PRODUTOALUGADO_RESPONSE = "createBulkProduto_alugadoResponse";
    	/**
    	 * An error response primitive for createBulkProduto_alugado.
    	 *
    	 * @see #CREATE_BULK_PRODUTOALUGADO the request primitive
    	 */
    	String CREATE_BULK_PRODUTOALUGADO_ERROR = "createBulkProduto_alugadoError";
    	/**
    	 * The 'createMerge' request primitive for the Produto_alugado entity.
    	 * @see Produto_alugado the request payload
    	 */
    	String CREATE_MERGE_PRODUTOALUGADO = "createMergeProduto_alugado";
    	/**
    	 * The success response primitive for createMergeProduto_alugado.
    	 *
    	 * @see #CREATE_MERGE_PRODUTOALUGADO the request primitive
    	 */
    	String CREATE_MERGE_PRODUTOALUGADO_RESPONSE = "createMergeProduto_alugadoResponse";
    	/**
    	 * An error response primitive for createMergeProduto_alugado.
    	 *
    	 * @see #CREATE_MERGE_PRODUTOALUGADO the request primitive
    	 */
    	String CREATE_MERGE_PRODUTOALUGADO_ERROR = "createMergeProduto_alugadoError";
    	/**
    	 * The 'retrieve' request primitive for the Produto_alugado entity.
    	 * @see Produto_alugado.Id the request payload
    	 */
    	String RETRIEVE_PRODUTOALUGADO = "retrieveProduto_alugado";
    	/**
    	 * The success response primitive for retrieveProduto_alugado.
    	 *
    	 * @see #RETRIEVE_PRODUTOALUGADO the request primitive
    	 */
    	String RETRIEVE_PRODUTOALUGADO_RESPONSE = "retrieveProduto_alugadoResponse";
    	/**
    	 * An error response primitive for retrieveProduto_alugado.
    	 *
    	 * @see #RETRIEVE_PRODUTOALUGADO the request primitive
    	 */
    	String RETRIEVE_PRODUTOALUGADO_ERROR = "retrieveProduto_alugadoError";
    	/**
    	 * The 'update' request primitive for the Produto_alugado entity.
    	 * @see Produto_alugado the request payload
    	 */
    	String UPDATE_PRODUTOALUGADO = "updateProduto_alugado";
    	/**
    	 * The success response primitive for updateProduto_alugado.
    	 *
    	 * @see #UPDATE_PRODUTOALUGADO the request primitive
    	 */
    	String UPDATE_PRODUTOALUGADO_RESPONSE = "updateProduto_alugadoResponse";
    	/**
    	 * An error response primitive for updateProduto_alugado.
    	 *
    	 * @see #UPDATE_PRODUTOALUGADO the request primitive
    	 */
    	String UPDATE_PRODUTOALUGADO_ERROR = "updateProduto_alugadoError";
    	/**
    	 * The 'updateMerge' request primitive for the Produto_alugado entity.
    	 * @see Produto_alugado the request payload
    	 */
    	String UPDATE_MERGE_PRODUTOALUGADO = "updateMergeProduto_alugado";
    	/**
    	 * The success response primitive for updateMergeProduto_alugado.
    	 *
    	 * @see #UPDATE_MERGE_PRODUTOALUGADO the request primitive
    	 */
    	String UPDATE_MERGE_PRODUTOALUGADO_RESPONSE = "updateMergeProduto_alugadoResponse";
    	/**
    	 * An error response primitive for updateMergeProduto_alugado.
    	 *
    	 * @see #UPDATE_MERGE_PRODUTOALUGADO the request primitive
    	 */
    	String UPDATE_MERGE_PRODUTOALUGADO_ERROR = "updateMergeProduto_alugadoError";
    	/**
    	 * The 'delete' request primitive for the Produto_alugado entity.
    	 * @see Produto_alugado.Id the request payload
    	 */
    	String DELETE_PRODUTOALUGADO = "deleteProduto_alugado";
    	/**
    	 * The success response primitive for deleteProduto_alugado.
    	 *
    	 * @see #DELETE_PRODUTOALUGADO the request primitive
    	 */
    	String DELETE_PRODUTOALUGADO_RESPONSE = "deleteProduto_alugadoResponse";
    	/**
    	 * An error response primitive for deleteProduto_alugado.
    	 *
    	 * @see #DELETE_PRODUTOALUGADO the request primitive
    	 */
    	String DELETE_PRODUTOALUGADO_ERROR = "deleteProduto_alugadoError";
    	/**
    	 * The 'create' request primitive for the Locacao entity.
    	 * @see Locacao the request payload
    	 */
    	String CREATE_LOCACAO = "createLocacao";
    	/**
    	 * The success response primitive for createLocacao.
    	 *
    	 * @see #CREATE_LOCACAO the request primitive
    	 */
    	String CREATE_LOCACAO_RESPONSE = "createLocacaoResponse";
    	/**
    	 * An error response primitive for createLocacao.
    	 *
    	 * @see #CREATE_LOCACAO the request primitive
    	 */
    	String CREATE_LOCACAO_ERROR = "createLocacaoError";
    	/**
    	 * The 'createBulk' request primitive for the Locacao entity.
    	 * @see CreateBulkLocacaoInput the request payload
    	 */
    	String CREATE_BULK_LOCACAO = "createBulkLocacao";
    	/**
    	 * The success response primitive for createBulkLocacao.
    	 *
    	 * @see #CREATE_BULK_LOCACAO the request primitive
    	 * @see CreateBulkLocacaoOutput the response payload
    	 */
    	String CREATE_BULK_LOCACAO_RESPONSE = "createBulkLocacaoResponse";
    	/**
    	 * An error response primitive for createBulkLocacao.
    	 *
    	 * @see #CREATE_BULK_LOCACAO the request primitive
    	 */
    	String CREATE_BULK_LOCACAO_ERROR = "createBulkLocacaoError";
    	/**
    	 * The 'createMerge' request primitive for the Locacao entity.
    	 * @see Locacao the request payload
    	 */
    	String CREATE_MERGE_LOCACAO = "createMergeLocacao";
    	/**
    	 * The success response primitive for createMergeLocacao.
    	 *
    	 * @see #CREATE_MERGE_LOCACAO the request primitive
    	 */
    	String CREATE_MERGE_LOCACAO_RESPONSE = "createMergeLocacaoResponse";
    	/**
    	 * An error response primitive for createMergeLocacao.
    	 *
    	 * @see #CREATE_MERGE_LOCACAO the request primitive
    	 */
    	String CREATE_MERGE_LOCACAO_ERROR = "createMergeLocacaoError";
    	/**
    	 * The 'retrieve' request primitive for the Locacao entity.
    	 * @see Locacao.Id the request payload
    	 */
    	String RETRIEVE_LOCACAO = "retrieveLocacao";
    	/**
    	 * The success response primitive for retrieveLocacao.
    	 *
    	 * @see #RETRIEVE_LOCACAO the request primitive
    	 */
    	String RETRIEVE_LOCACAO_RESPONSE = "retrieveLocacaoResponse";
    	/**
    	 * An error response primitive for retrieveLocacao.
    	 *
    	 * @see #RETRIEVE_LOCACAO the request primitive
    	 */
    	String RETRIEVE_LOCACAO_ERROR = "retrieveLocacaoError";
    	/**
    	 * The 'update' request primitive for the Locacao entity.
    	 * @see Locacao the request payload
    	 */
    	String UPDATE_LOCACAO = "updateLocacao";
    	/**
    	 * The success response primitive for updateLocacao.
    	 *
    	 * @see #UPDATE_LOCACAO the request primitive
    	 */
    	String UPDATE_LOCACAO_RESPONSE = "updateLocacaoResponse";
    	/**
    	 * An error response primitive for updateLocacao.
    	 *
    	 * @see #UPDATE_LOCACAO the request primitive
    	 */
    	String UPDATE_LOCACAO_ERROR = "updateLocacaoError";
    	/**
    	 * The 'updateMerge' request primitive for the Locacao entity.
    	 * @see Locacao the request payload
    	 */
    	String UPDATE_MERGE_LOCACAO = "updateMergeLocacao";
    	/**
    	 * The success response primitive for updateMergeLocacao.
    	 *
    	 * @see #UPDATE_MERGE_LOCACAO the request primitive
    	 */
    	String UPDATE_MERGE_LOCACAO_RESPONSE = "updateMergeLocacaoResponse";
    	/**
    	 * An error response primitive for updateMergeLocacao.
    	 *
    	 * @see #UPDATE_MERGE_LOCACAO the request primitive
    	 */
    	String UPDATE_MERGE_LOCACAO_ERROR = "updateMergeLocacaoError";
    	/**
    	 * The 'delete' request primitive for the Locacao entity.
    	 * @see Locacao.Id the request payload
    	 */
    	String DELETE_LOCACAO = "deleteLocacao";
    	/**
    	 * The success response primitive for deleteLocacao.
    	 *
    	 * @see #DELETE_LOCACAO the request primitive
    	 */
    	String DELETE_LOCACAO_RESPONSE = "deleteLocacaoResponse";
    	/**
    	 * An error response primitive for deleteLocacao.
    	 *
    	 * @see #DELETE_LOCACAO the request primitive
    	 */
    	String DELETE_LOCACAO_ERROR = "deleteLocacaoError";
    }
    
    interface Events {
    	/**
    	 * Default 'serviceStarted' event.
    	 */
    	String SERVICE_STARTED = "serviceStarted";
    	/**
    	 * Default 'notifyUser' event.
    	 */
    	String NOTIFY_USER_EVENT = "notifyUserEvent";
    	String IMPORT_LOCADORA_EVENT = "importLocadoraEvent";
    	String EXPORT_LOCADORA_EVENT = "exportLocadoraEvent";
    	String IMPORT_TIPO_USUARIO_EVENT = "importTipoUsuarioEvent";
    	String EXPORT_TIPO_USUARIO_EVENT = "exportTipoUsuarioEvent";
    	String IMPORT_CLIENTE_EVENT = "importClienteEvent";
    	String EXPORT_CLIENTE_EVENT = "exportClienteEvent";
    	String IMPORT_TIPO_PRODUTO_EVENT = "importTipoProdutoEvent";
    	String EXPORT_TIPO_PRODUTO_EVENT = "exportTipoProdutoEvent";
    	String IMPORT_PRODUTO_EVENT = "importProdutoEvent";
    	String EXPORT_PRODUTO_EVENT = "exportProdutoEvent";
    	String IMPORT_PRODUTOALUGADO_EVENT = "importProduto_alugadoEvent";
    	String EXPORT_PRODUTOALUGADO_EVENT = "exportProduto_alugadoEvent";
    	String IMPORT_LOCACAO_EVENT = "importLocacaoEvent";
    	String EXPORT_LOCACAO_EVENT = "exportLocacaoEvent";
    }
    
}
