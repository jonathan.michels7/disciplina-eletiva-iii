/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.concurrent.CompletableFuture;

import br.com.senior.furb.basico.RetornaEstoqueInput;
import br.com.senior.furb.basico.RetornaEstoqueOutput;
import br.com.senior.furb.basico.BuscaProdutoInput;
import br.com.senior.furb.basico.BuscaProdutoOutput;
import br.com.senior.furb.basico.BuscaClienteInput;
import br.com.senior.furb.basico.BuscaClienteOutput;
import br.com.senior.furb.basico.GetMetadataInput;
import br.com.senior.furb.basico.GetMetadataOutput;
import br.com.senior.furb.basico.ImportLocadoraInput;
import br.com.senior.furb.basico.ImportLocadoraOutput;
import br.com.senior.furb.basico.ExportLocadoraInput;
import br.com.senior.furb.basico.ExportLocadoraOutput;
import br.com.senior.furb.basico.ImportTipoUsuarioInput;
import br.com.senior.furb.basico.ImportTipoUsuarioOutput;
import br.com.senior.furb.basico.ExportTipoUsuarioInput;
import br.com.senior.furb.basico.ExportTipoUsuarioOutput;
import br.com.senior.furb.basico.ImportClienteInput;
import br.com.senior.furb.basico.ImportClienteOutput;
import br.com.senior.furb.basico.ExportClienteInput;
import br.com.senior.furb.basico.ExportClienteOutput;
import br.com.senior.furb.basico.ImportTipoProdutoInput;
import br.com.senior.furb.basico.ImportTipoProdutoOutput;
import br.com.senior.furb.basico.ExportTipoProdutoInput;
import br.com.senior.furb.basico.ExportTipoProdutoOutput;
import br.com.senior.furb.basico.ImportProdutoInput;
import br.com.senior.furb.basico.ImportProdutoOutput;
import br.com.senior.furb.basico.ExportProdutoInput;
import br.com.senior.furb.basico.ExportProdutoOutput;
import br.com.senior.furb.basico.ImportProduto_alugadoInput;
import br.com.senior.furb.basico.ImportProduto_alugadoOutput;
import br.com.senior.furb.basico.ExportProduto_alugadoInput;
import br.com.senior.furb.basico.ExportProduto_alugadoOutput;
import br.com.senior.furb.basico.ImportLocacaoInput;
import br.com.senior.furb.basico.ImportLocacaoOutput;
import br.com.senior.furb.basico.ExportLocacaoInput;
import br.com.senior.furb.basico.ExportLocacaoOutput;
import br.com.senior.furb.basico.GetDependenciesOutput;
import br.com.senior.furb.basico.Locadora;
import br.com.senior.furb.basico.CreateBulkLocadoraInput;
import br.com.senior.furb.basico.CreateBulkLocadoraOutput;
import br.com.senior.furb.basico.TipoUsuario;
import br.com.senior.furb.basico.CreateBulkTipoUsuarioInput;
import br.com.senior.furb.basico.CreateBulkTipoUsuarioOutput;
import br.com.senior.furb.basico.Cliente;
import br.com.senior.furb.basico.CreateBulkClienteInput;
import br.com.senior.furb.basico.CreateBulkClienteOutput;
import br.com.senior.furb.basico.TipoProduto;
import br.com.senior.furb.basico.CreateBulkTipoProdutoInput;
import br.com.senior.furb.basico.CreateBulkTipoProdutoOutput;
import br.com.senior.furb.basico.Produto;
import br.com.senior.furb.basico.CreateBulkProdutoInput;
import br.com.senior.furb.basico.CreateBulkProdutoOutput;
import br.com.senior.furb.basico.Produto_alugado;
import br.com.senior.furb.basico.CreateBulkProduto_alugadoInput;
import br.com.senior.furb.basico.CreateBulkProduto_alugadoOutput;
import br.com.senior.furb.basico.Locacao;
import br.com.senior.furb.basico.CreateBulkLocacaoInput;
import br.com.senior.furb.basico.CreateBulkLocacaoOutput;
import br.com.senior.furb.basico.ServiceStartedPayload;
import br.com.senior.furb.basico.NotifyUserEventPayload;
import br.com.senior.furb.basico.ImportLocadoraEventPayload;
import br.com.senior.furb.basico.ExportLocadoraEventPayload;
import br.com.senior.furb.basico.ImportTipoUsuarioEventPayload;
import br.com.senior.furb.basico.ExportTipoUsuarioEventPayload;
import br.com.senior.furb.basico.ImportClienteEventPayload;
import br.com.senior.furb.basico.ExportClienteEventPayload;
import br.com.senior.furb.basico.ImportTipoProdutoEventPayload;
import br.com.senior.furb.basico.ExportTipoProdutoEventPayload;
import br.com.senior.furb.basico.ImportProdutoEventPayload;
import br.com.senior.furb.basico.ExportProdutoEventPayload;
import br.com.senior.furb.basico.ImportProduto_alugadoEventPayload;
import br.com.senior.furb.basico.ExportProduto_alugadoEventPayload;
import br.com.senior.furb.basico.ImportLocacaoEventPayload;
import br.com.senior.furb.basico.ExportLocacaoEventPayload;

/**
* Trabalho furb
*/
public interface BasicoStub {

	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Obtém a quantidade do produto no estoque
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	RetornaEstoqueOutput retornaEstoque(RetornaEstoqueInput input, long timeout);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Obtém a quantidade do produto no estoque
	 * Chamada assíncrona
	 */
	void retornaEstoque(RetornaEstoqueInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Obtém a quantidade do produto no estoque
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<RetornaEstoqueOutput> retornaEstoqueRequest(RetornaEstoqueInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca produto conforme informações passadas
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	BuscaProdutoOutput buscaProduto(BuscaProdutoInput input, long timeout);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca produto conforme informações passadas
	 * Chamada assíncrona
	 */
	void buscaProduto(BuscaProdutoInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca produto conforme informações passadas
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<BuscaProdutoOutput> buscaProdutoRequest(BuscaProdutoInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca usuario conforme nome
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	BuscaClienteOutput buscaCliente(BuscaClienteInput input, long timeout);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca usuario conforme nome
	 * Chamada assíncrona
	 */
	void buscaCliente(BuscaClienteInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca usuario conforme nome
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<BuscaClienteOutput> buscaClienteRequest(BuscaClienteInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	GetMetadataOutput getMetadata(GetMetadataInput input, long timeout);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
	 * Chamada assíncrona
	 */
	void getMetadata(GetMetadataInput input);
	
	/**
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<GetMetadataOutput> getMetadataRequest(GetMetadataInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportLocadoraOutput importLocadora(ImportLocadoraInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importLocadora(ImportLocadoraInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportLocadoraOutput> importLocadoraRequest(ImportLocadoraInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportLocadoraOutput exportLocadora(ExportLocadoraInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportLocadora(ExportLocadoraInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportLocadoraOutput> exportLocadoraRequest(ExportLocadoraInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportTipoUsuarioOutput importTipoUsuario(ImportTipoUsuarioInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importTipoUsuario(ImportTipoUsuarioInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportTipoUsuarioOutput> importTipoUsuarioRequest(ImportTipoUsuarioInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportTipoUsuarioOutput exportTipoUsuario(ExportTipoUsuarioInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportTipoUsuario(ExportTipoUsuarioInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportTipoUsuarioOutput> exportTipoUsuarioRequest(ExportTipoUsuarioInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportClienteOutput importCliente(ImportClienteInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importCliente(ImportClienteInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportClienteOutput> importClienteRequest(ImportClienteInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportClienteOutput exportCliente(ExportClienteInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportCliente(ExportClienteInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportClienteOutput> exportClienteRequest(ExportClienteInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportTipoProdutoOutput importTipoProduto(ImportTipoProdutoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importTipoProduto(ImportTipoProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportTipoProdutoOutput> importTipoProdutoRequest(ImportTipoProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportTipoProdutoOutput exportTipoProduto(ExportTipoProdutoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportTipoProduto(ExportTipoProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportTipoProdutoOutput> exportTipoProdutoRequest(ExportTipoProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportProdutoOutput importProduto(ImportProdutoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importProduto(ImportProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportProdutoOutput> importProdutoRequest(ImportProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportProdutoOutput exportProduto(ExportProdutoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportProduto(ExportProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportProdutoOutput> exportProdutoRequest(ExportProdutoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportProduto_alugadoOutput importProduto_alugado(ImportProduto_alugadoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importProduto_alugado(ImportProduto_alugadoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportProduto_alugadoOutput> importProduto_alugadoRequest(ImportProduto_alugadoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportProduto_alugadoOutput exportProduto_alugado(ExportProduto_alugadoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportProduto_alugado(ExportProduto_alugadoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportProduto_alugadoOutput> exportProduto_alugadoRequest(ExportProduto_alugadoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ImportLocacaoOutput importLocacao(ImportLocacaoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void importLocacao(ImportLocacaoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ImportLocacaoOutput> importLocacaoRequest(ImportLocacaoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	ExportLocacaoOutput exportLocacao(ExportLocacaoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona
	 */
	void exportLocacao(ExportLocacaoInput input);
	
	/**
	 * This is a public operation
	 * 
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<ExportLocacaoOutput> exportLocacaoRequest(ExportLocacaoInput input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locadora.PagedResults listLocadora(Locadora.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void listLocadora(Locadora.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locadora.PagedResults> listLocadoraRequest(Locadora.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoUsuario.PagedResults listTipoUsuario(TipoUsuario.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void listTipoUsuario(TipoUsuario.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoUsuario.PagedResults> listTipoUsuarioRequest(TipoUsuario.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Cliente.PagedResults listCliente(Cliente.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void listCliente(Cliente.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Cliente.PagedResults> listClienteRequest(Cliente.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoProduto.PagedResults listTipoProduto(TipoProduto.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void listTipoProduto(TipoProduto.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoProduto.PagedResults> listTipoProdutoRequest(TipoProduto.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto.PagedResults listProduto(Produto.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void listProduto(Produto.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto.PagedResults> listProdutoRequest(Produto.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto_alugado.PagedResults listProduto_alugado(Produto_alugado.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void listProduto_alugado(Produto_alugado.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto_alugado.PagedResults> listProduto_alugadoRequest(Produto_alugado.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locacao.PagedResults listLocacao(Locacao.PageRequest input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void listLocacao(Locacao.PageRequest input);
	
	/**
	 * This is a public operation
	 * The 'list' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locacao.PagedResults> listLocacaoRequest(Locacao.PageRequest input);
	
	/**
	 * This is a public operation
	 * Returns a list with all dependencies from this service, along with their respective versions
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	GetDependenciesOutput getDependencies(long timeout);
	
	/**
	 * This is a public operation
	 * Returns a list with all dependencies from this service, along with their respective versions
	 * Chamada assíncrona
	 */
	void getDependencies();
	
	/**
	 * This is a public operation
	 * Returns a list with all dependencies from this service, along with their respective versions
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<GetDependenciesOutput> getDependenciesRequest();
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locadora createLocadora(Locadora input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void createLocadora(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locadora> createLocadoraRequest(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkLocadoraOutput createBulkLocadora(CreateBulkLocadoraInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void createBulkLocadora(CreateBulkLocadoraInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkLocadoraOutput> createBulkLocadoraRequest(CreateBulkLocadoraInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locadora createMergeLocadora(Locadora input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void createMergeLocadora(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locadora> createMergeLocadoraRequest(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locadora retrieveLocadora(Locadora.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void retrieveLocadora(Locadora.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locadora> retrieveLocadoraRequest(Locadora.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locadora updateLocadora(Locadora input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void updateLocadora(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locadora> updateLocadoraRequest(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locadora updateMergeLocadora(Locadora input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void updateMergeLocadora(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locadora entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locadora> updateMergeLocadoraRequest(Locadora input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Locadora entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteLocadora(Locadora.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Locadora entity.
	 * Chamada assíncrona
	 */
	void deleteLocadora(Locadora.Id input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoUsuario createTipoUsuario(TipoUsuario input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void createTipoUsuario(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoUsuario> createTipoUsuarioRequest(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkTipoUsuarioOutput createBulkTipoUsuario(CreateBulkTipoUsuarioInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void createBulkTipoUsuario(CreateBulkTipoUsuarioInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkTipoUsuarioOutput> createBulkTipoUsuarioRequest(CreateBulkTipoUsuarioInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoUsuario createMergeTipoUsuario(TipoUsuario input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void createMergeTipoUsuario(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoUsuario> createMergeTipoUsuarioRequest(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoUsuario retrieveTipoUsuario(TipoUsuario.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void retrieveTipoUsuario(TipoUsuario.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoUsuario> retrieveTipoUsuarioRequest(TipoUsuario.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoUsuario updateTipoUsuario(TipoUsuario input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void updateTipoUsuario(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoUsuario> updateTipoUsuarioRequest(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoUsuario updateMergeTipoUsuario(TipoUsuario input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void updateMergeTipoUsuario(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoUsuario> updateMergeTipoUsuarioRequest(TipoUsuario input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the TipoUsuario entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteTipoUsuario(TipoUsuario.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the TipoUsuario entity.
	 * Chamada assíncrona
	 */
	void deleteTipoUsuario(TipoUsuario.Id input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Cliente createCliente(Cliente input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void createCliente(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Cliente> createClienteRequest(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkClienteOutput createBulkCliente(CreateBulkClienteInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void createBulkCliente(CreateBulkClienteInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkClienteOutput> createBulkClienteRequest(CreateBulkClienteInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Cliente createMergeCliente(Cliente input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void createMergeCliente(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Cliente> createMergeClienteRequest(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Cliente retrieveCliente(Cliente.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void retrieveCliente(Cliente.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Cliente> retrieveClienteRequest(Cliente.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Cliente updateCliente(Cliente input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void updateCliente(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Cliente> updateClienteRequest(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Cliente updateMergeCliente(Cliente input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void updateMergeCliente(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Cliente entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Cliente> updateMergeClienteRequest(Cliente input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Cliente entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteCliente(Cliente.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Cliente entity.
	 * Chamada assíncrona
	 */
	void deleteCliente(Cliente.Id input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoProduto createTipoProduto(TipoProduto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void createTipoProduto(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoProduto> createTipoProdutoRequest(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkTipoProdutoOutput createBulkTipoProduto(CreateBulkTipoProdutoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void createBulkTipoProduto(CreateBulkTipoProdutoInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkTipoProdutoOutput> createBulkTipoProdutoRequest(CreateBulkTipoProdutoInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoProduto createMergeTipoProduto(TipoProduto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void createMergeTipoProduto(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoProduto> createMergeTipoProdutoRequest(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoProduto retrieveTipoProduto(TipoProduto.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void retrieveTipoProduto(TipoProduto.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoProduto> retrieveTipoProdutoRequest(TipoProduto.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoProduto updateTipoProduto(TipoProduto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void updateTipoProduto(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoProduto> updateTipoProdutoRequest(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	TipoProduto updateMergeTipoProduto(TipoProduto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void updateMergeTipoProduto(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoProduto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<TipoProduto> updateMergeTipoProdutoRequest(TipoProduto input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the TipoProduto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteTipoProduto(TipoProduto.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the TipoProduto entity.
	 * Chamada assíncrona
	 */
	void deleteTipoProduto(TipoProduto.Id input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto createProduto(Produto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void createProduto(Produto input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto> createProdutoRequest(Produto input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkProdutoOutput createBulkProduto(CreateBulkProdutoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void createBulkProduto(CreateBulkProdutoInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkProdutoOutput> createBulkProdutoRequest(CreateBulkProdutoInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto createMergeProduto(Produto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void createMergeProduto(Produto input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto> createMergeProdutoRequest(Produto input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto retrieveProduto(Produto.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void retrieveProduto(Produto.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto> retrieveProdutoRequest(Produto.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto updateProduto(Produto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void updateProduto(Produto input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto> updateProdutoRequest(Produto input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto updateMergeProduto(Produto input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void updateMergeProduto(Produto input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto> updateMergeProdutoRequest(Produto input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Produto entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteProduto(Produto.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Produto entity.
	 * Chamada assíncrona
	 */
	void deleteProduto(Produto.Id input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto_alugado createProduto_alugado(Produto_alugado input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void createProduto_alugado(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto_alugado> createProduto_alugadoRequest(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkProduto_alugadoOutput createBulkProduto_alugado(CreateBulkProduto_alugadoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void createBulkProduto_alugado(CreateBulkProduto_alugadoInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkProduto_alugadoOutput> createBulkProduto_alugadoRequest(CreateBulkProduto_alugadoInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto_alugado createMergeProduto_alugado(Produto_alugado input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void createMergeProduto_alugado(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto_alugado> createMergeProduto_alugadoRequest(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto_alugado retrieveProduto_alugado(Produto_alugado.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void retrieveProduto_alugado(Produto_alugado.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto_alugado> retrieveProduto_alugadoRequest(Produto_alugado.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto_alugado updateProduto_alugado(Produto_alugado input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void updateProduto_alugado(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto_alugado> updateProduto_alugadoRequest(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Produto_alugado updateMergeProduto_alugado(Produto_alugado input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void updateMergeProduto_alugado(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Produto_alugado> updateMergeProduto_alugadoRequest(Produto_alugado input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Produto_alugado entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteProduto_alugado(Produto_alugado.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Produto_alugado entity.
	 * Chamada assíncrona
	 */
	void deleteProduto_alugado(Produto_alugado.Id input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locacao createLocacao(Locacao input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void createLocacao(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'create' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locacao> createLocacaoRequest(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	CreateBulkLocacaoOutput createBulkLocacao(CreateBulkLocacaoInput input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void createBulkLocacao(CreateBulkLocacaoInput input);
	
	/**
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<CreateBulkLocacaoOutput> createBulkLocacaoRequest(CreateBulkLocacaoInput input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locacao createMergeLocacao(Locacao input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void createMergeLocacao(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locacao> createMergeLocacaoRequest(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locacao retrieveLocacao(Locacao.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void retrieveLocacao(Locacao.Id input);
	
	/**
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locacao> retrieveLocacaoRequest(Locacao.Id input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locacao updateLocacao(Locacao input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void updateLocacao(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'update' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locacao> updateLocacaoRequest(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	Locacao updateMergeLocacao(Locacao input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void updateMergeLocacao(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locacao entity.
	 * Chamada assíncrona utilizando request
	 */
	CompletableFuture<Locacao> updateMergeLocacaoRequest(Locacao input);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Locacao entity.
	 * Chamada síncrona, o valor de timeout deve ser informado em ms 
	 */
	void deleteLocacao(Locacao.Id input, long timeout);
	
	/**
	 * This is a public operation
	 * The 'delete' request primitive for the Locacao entity.
	 * Chamada assíncrona
	 */
	void deleteLocacao(Locacao.Id input);
	


	/**
	* Chamada assíncrona para o método publishServiceStarted 
	* Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	*/
	void publishServiceStarted( ServiceStartedPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishNotifyUserEvent 
	* Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	*/
	void publishNotifyUserEvent( NotifyUserEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportLocadoraEvent 
	* This is a public operation
	*/
	void publishImportLocadoraEvent( ImportLocadoraEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportLocadoraEvent 
	* This is a public operation
	*/
	void publishExportLocadoraEvent( ExportLocadoraEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportTipoUsuarioEvent 
	* This is a public operation
	*/
	void publishImportTipoUsuarioEvent( ImportTipoUsuarioEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportTipoUsuarioEvent 
	* This is a public operation
	*/
	void publishExportTipoUsuarioEvent( ExportTipoUsuarioEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportClienteEvent 
	* This is a public operation
	*/
	void publishImportClienteEvent( ImportClienteEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportClienteEvent 
	* This is a public operation
	*/
	void publishExportClienteEvent( ExportClienteEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportTipoProdutoEvent 
	* This is a public operation
	*/
	void publishImportTipoProdutoEvent( ImportTipoProdutoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportTipoProdutoEvent 
	* This is a public operation
	*/
	void publishExportTipoProdutoEvent( ExportTipoProdutoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportProdutoEvent 
	* This is a public operation
	*/
	void publishImportProdutoEvent( ImportProdutoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportProdutoEvent 
	* This is a public operation
	*/
	void publishExportProdutoEvent( ExportProdutoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportProduto_alugadoEvent 
	* This is a public operation
	*/
	void publishImportProduto_alugadoEvent( ImportProduto_alugadoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportProduto_alugadoEvent 
	* This is a public operation
	*/
	void publishExportProduto_alugadoEvent( ExportProduto_alugadoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishImportLocacaoEvent 
	* This is a public operation
	*/
	void publishImportLocacaoEvent( ImportLocacaoEventPayload input );
			
	
	/**
	* Chamada assíncrona para o método publishExportLocacaoEvent 
	* This is a public operation
	*/
	void publishExportLocacaoEvent( ExportLocacaoEventPayload input );
			
	

}
