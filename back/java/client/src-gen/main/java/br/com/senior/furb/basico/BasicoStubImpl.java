/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.function.Supplier;

import br.com.senior.messaging.Message;
import br.com.senior.messaging.IMessenger;
import br.com.senior.messaging.utils.DtoJsonConverter;
import br.com.senior.sdl.user.UserIdentifier;

import java.util.concurrent.CompletableFuture;
import br.com.senior.furb.basico.RetornaEstoqueInput;
import br.com.senior.furb.basico.RetornaEstoqueOutput;
import br.com.senior.furb.basico.BuscaProdutoInput;
import br.com.senior.furb.basico.BuscaProdutoOutput;
import br.com.senior.furb.basico.BuscaClienteInput;
import br.com.senior.furb.basico.BuscaClienteOutput;
import br.com.senior.furb.basico.GetMetadataInput;
import br.com.senior.furb.basico.GetMetadataOutput;
import br.com.senior.furb.basico.ImportLocadoraInput;
import br.com.senior.furb.basico.ImportLocadoraOutput;
import br.com.senior.furb.basico.ExportLocadoraInput;
import br.com.senior.furb.basico.ExportLocadoraOutput;
import br.com.senior.furb.basico.ImportTipoUsuarioInput;
import br.com.senior.furb.basico.ImportTipoUsuarioOutput;
import br.com.senior.furb.basico.ExportTipoUsuarioInput;
import br.com.senior.furb.basico.ExportTipoUsuarioOutput;
import br.com.senior.furb.basico.ImportClienteInput;
import br.com.senior.furb.basico.ImportClienteOutput;
import br.com.senior.furb.basico.ExportClienteInput;
import br.com.senior.furb.basico.ExportClienteOutput;
import br.com.senior.furb.basico.ImportTipoProdutoInput;
import br.com.senior.furb.basico.ImportTipoProdutoOutput;
import br.com.senior.furb.basico.ExportTipoProdutoInput;
import br.com.senior.furb.basico.ExportTipoProdutoOutput;
import br.com.senior.furb.basico.ImportProdutoInput;
import br.com.senior.furb.basico.ImportProdutoOutput;
import br.com.senior.furb.basico.ExportProdutoInput;
import br.com.senior.furb.basico.ExportProdutoOutput;
import br.com.senior.furb.basico.ImportProduto_alugadoInput;
import br.com.senior.furb.basico.ImportProduto_alugadoOutput;
import br.com.senior.furb.basico.ExportProduto_alugadoInput;
import br.com.senior.furb.basico.ExportProduto_alugadoOutput;
import br.com.senior.furb.basico.ImportLocacaoInput;
import br.com.senior.furb.basico.ImportLocacaoOutput;
import br.com.senior.furb.basico.ExportLocacaoInput;
import br.com.senior.furb.basico.ExportLocacaoOutput;
import br.com.senior.furb.basico.GetDependenciesOutput;
import br.com.senior.furb.basico.Locadora;
import br.com.senior.furb.basico.CreateBulkLocadoraInput;
import br.com.senior.furb.basico.CreateBulkLocadoraOutput;
import br.com.senior.furb.basico.TipoUsuario;
import br.com.senior.furb.basico.CreateBulkTipoUsuarioInput;
import br.com.senior.furb.basico.CreateBulkTipoUsuarioOutput;
import br.com.senior.furb.basico.Cliente;
import br.com.senior.furb.basico.CreateBulkClienteInput;
import br.com.senior.furb.basico.CreateBulkClienteOutput;
import br.com.senior.furb.basico.TipoProduto;
import br.com.senior.furb.basico.CreateBulkTipoProdutoInput;
import br.com.senior.furb.basico.CreateBulkTipoProdutoOutput;
import br.com.senior.furb.basico.Produto;
import br.com.senior.furb.basico.CreateBulkProdutoInput;
import br.com.senior.furb.basico.CreateBulkProdutoOutput;
import br.com.senior.furb.basico.Produto_alugado;
import br.com.senior.furb.basico.CreateBulkProduto_alugadoInput;
import br.com.senior.furb.basico.CreateBulkProduto_alugadoOutput;
import br.com.senior.furb.basico.Locacao;
import br.com.senior.furb.basico.CreateBulkLocacaoInput;
import br.com.senior.furb.basico.CreateBulkLocacaoOutput;
import br.com.senior.furb.basico.ServiceStartedPayload;
import br.com.senior.furb.basico.NotifyUserEventPayload;
import br.com.senior.furb.basico.ImportLocadoraEventPayload;
import br.com.senior.furb.basico.ExportLocadoraEventPayload;
import br.com.senior.furb.basico.ImportTipoUsuarioEventPayload;
import br.com.senior.furb.basico.ExportTipoUsuarioEventPayload;
import br.com.senior.furb.basico.ImportClienteEventPayload;
import br.com.senior.furb.basico.ExportClienteEventPayload;
import br.com.senior.furb.basico.ImportTipoProdutoEventPayload;
import br.com.senior.furb.basico.ExportTipoProdutoEventPayload;
import br.com.senior.furb.basico.ImportProdutoEventPayload;
import br.com.senior.furb.basico.ExportProdutoEventPayload;
import br.com.senior.furb.basico.ImportProduto_alugadoEventPayload;
import br.com.senior.furb.basico.ExportProduto_alugadoEventPayload;
import br.com.senior.furb.basico.ImportLocacaoEventPayload;
import br.com.senior.furb.basico.ExportLocacaoEventPayload;

import br.com.senior.furb.basico.BasicoValidator;
import br.com.senior.furb.basico.BasicoException;
import br.com.senior.furb.basico.BasicoConstants;

/**
* Trabalho furb
*/
public class BasicoStubImpl  implements BasicoStub {

	protected final Supplier<IMessenger> messengerSupplier;
	protected final UserIdentifier userId;
	protected final Supplier<Message> messageSupplier;

	/**
	 * Use {@link #BasicoStubImpl(MessengerSupplier, UserIdentifier, MessageSupplier)} instead.
	 */
	@Deprecated
	public BasicoStubImpl(IMessenger messenger, UserIdentifier userId) {
		this(new InstanceMessengerSupplier(messenger), userId, null);
	}

	/**
	 * @param messengerSupplier Supplies current service messenger.
	 * @param userId Provides tenant and user name to send or publish messages.
	 * @param messageSupplier Supplies current message being processed by service. Used to send messages by foolowup.
	 */
	public BasicoStubImpl(Supplier<IMessenger> messengerSupplier, UserIdentifier userId, Supplier<Message> messageSupplier) {
		this.messengerSupplier = messengerSupplier;
		this.userId = userId;
		this.messageSupplier = messageSupplier;
	}

	/**
	 * Chamada síncrona para o método retornaEstoque
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Obtém a quantidade do produto no estoque
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public RetornaEstoqueOutput retornaEstoque(RetornaEstoqueInput input, long timeout) {
		br.com.senior.furb.basico.impl.RetornaEstoqueImpl impl = new br.com.senior.furb.basico.impl.RetornaEstoqueImpl(messengerSupplier, userId, messageSupplier);
		return impl.retornaEstoque(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retornaEstoque
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Obtém a quantidade do produto no estoque
	 */
	@Override
	public void retornaEstoque(RetornaEstoqueInput input) {
		br.com.senior.furb.basico.impl.RetornaEstoqueImpl impl = new br.com.senior.furb.basico.impl.RetornaEstoqueImpl(messengerSupplier, userId, messageSupplier);
		impl.retornaEstoque(input);
	}
	
	/**
	 * Chamada assíncrona para o método retornaEstoque
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Obtém a quantidade do produto no estoque
	 */
	@Override
	public CompletableFuture<RetornaEstoqueOutput> retornaEstoqueRequest(RetornaEstoqueInput input) {
		br.com.senior.furb.basico.impl.RetornaEstoqueImpl impl = new br.com.senior.furb.basico.impl.RetornaEstoqueImpl(messengerSupplier, userId, messageSupplier);
		return impl.retornaEstoqueRequest(input);
	}
	/**
	 * Chamada síncrona para o método buscaProduto
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca produto conforme informações passadas
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public BuscaProdutoOutput buscaProduto(BuscaProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.BuscaProdutoImpl impl = new br.com.senior.furb.basico.impl.BuscaProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.buscaProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método buscaProduto
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca produto conforme informações passadas
	 */
	@Override
	public void buscaProduto(BuscaProdutoInput input) {
		br.com.senior.furb.basico.impl.BuscaProdutoImpl impl = new br.com.senior.furb.basico.impl.BuscaProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.buscaProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método buscaProduto
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca produto conforme informações passadas
	 */
	@Override
	public CompletableFuture<BuscaProdutoOutput> buscaProdutoRequest(BuscaProdutoInput input) {
		br.com.senior.furb.basico.impl.BuscaProdutoImpl impl = new br.com.senior.furb.basico.impl.BuscaProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.buscaProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método buscaCliente
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca usuario conforme nome
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public BuscaClienteOutput buscaCliente(BuscaClienteInput input, long timeout) {
		br.com.senior.furb.basico.impl.BuscaClienteImpl impl = new br.com.senior.furb.basico.impl.BuscaClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.buscaCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método buscaCliente
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca usuario conforme nome
	 */
	@Override
	public void buscaCliente(BuscaClienteInput input) {
		br.com.senior.furb.basico.impl.BuscaClienteImpl impl = new br.com.senior.furb.basico.impl.BuscaClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.buscaCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método buscaCliente
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Busca usuario conforme nome
	 */
	@Override
	public CompletableFuture<BuscaClienteOutput> buscaClienteRequest(BuscaClienteInput input) {
		br.com.senior.furb.basico.impl.BuscaClienteImpl impl = new br.com.senior.furb.basico.impl.BuscaClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.buscaClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método getMetadata
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public GetMetadataOutput getMetadata(GetMetadataInput input, long timeout) {
		br.com.senior.furb.basico.impl.GetMetadataImpl impl = new br.com.senior.furb.basico.impl.GetMetadataImpl(messengerSupplier, userId, messageSupplier);
		return impl.getMetadata(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método getMetadata
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
	 */
	@Override
	public void getMetadata(GetMetadataInput input) {
		br.com.senior.furb.basico.impl.GetMetadataImpl impl = new br.com.senior.furb.basico.impl.GetMetadataImpl(messengerSupplier, userId, messageSupplier);
		impl.getMetadata(input);
	}
	
	/**
	 * Chamada assíncrona para o método getMetadata
	 * Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	 * Default 'getMetadata' query. Every service must handle this command and return metadata in the format requested.
	 */
	@Override
	public CompletableFuture<GetMetadataOutput> getMetadataRequest(GetMetadataInput input) {
		br.com.senior.furb.basico.impl.GetMetadataImpl impl = new br.com.senior.furb.basico.impl.GetMetadataImpl(messengerSupplier, userId, messageSupplier);
		return impl.getMetadataRequest(input);
	}
	/**
	 * Chamada síncrona para o método importLocadora
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportLocadoraOutput importLocadora(ImportLocadoraInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportLocadoraImpl impl = new br.com.senior.furb.basico.impl.ImportLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.importLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importLocadora
	 * This is a public operation
	 * 
	 */
	@Override
	public void importLocadora(ImportLocadoraInput input) {
		br.com.senior.furb.basico.impl.ImportLocadoraImpl impl = new br.com.senior.furb.basico.impl.ImportLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.importLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método importLocadora
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportLocadoraOutput> importLocadoraRequest(ImportLocadoraInput input) {
		br.com.senior.furb.basico.impl.ImportLocadoraImpl impl = new br.com.senior.furb.basico.impl.ImportLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.importLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportLocadora
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportLocadoraOutput exportLocadora(ExportLocadoraInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportLocadoraImpl impl = new br.com.senior.furb.basico.impl.ExportLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportLocadora
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportLocadora(ExportLocadoraInput input) {
		br.com.senior.furb.basico.impl.ExportLocadoraImpl impl = new br.com.senior.furb.basico.impl.ExportLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.exportLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportLocadora
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportLocadoraOutput> exportLocadoraRequest(ExportLocadoraInput input) {
		br.com.senior.furb.basico.impl.ExportLocadoraImpl impl = new br.com.senior.furb.basico.impl.ExportLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método importTipoUsuario
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportTipoUsuarioOutput importTipoUsuario(ImportTipoUsuarioInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ImportTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.importTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importTipoUsuario
	 * This is a public operation
	 * 
	 */
	@Override
	public void importTipoUsuario(ImportTipoUsuarioInput input) {
		br.com.senior.furb.basico.impl.ImportTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ImportTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.importTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método importTipoUsuario
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportTipoUsuarioOutput> importTipoUsuarioRequest(ImportTipoUsuarioInput input) {
		br.com.senior.furb.basico.impl.ImportTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ImportTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.importTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportTipoUsuario
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportTipoUsuarioOutput exportTipoUsuario(ExportTipoUsuarioInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ExportTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportTipoUsuario
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportTipoUsuario(ExportTipoUsuarioInput input) {
		br.com.senior.furb.basico.impl.ExportTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ExportTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.exportTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportTipoUsuario
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportTipoUsuarioOutput> exportTipoUsuarioRequest(ExportTipoUsuarioInput input) {
		br.com.senior.furb.basico.impl.ExportTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ExportTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método importCliente
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportClienteOutput importCliente(ImportClienteInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportClienteImpl impl = new br.com.senior.furb.basico.impl.ImportClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.importCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importCliente
	 * This is a public operation
	 * 
	 */
	@Override
	public void importCliente(ImportClienteInput input) {
		br.com.senior.furb.basico.impl.ImportClienteImpl impl = new br.com.senior.furb.basico.impl.ImportClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.importCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método importCliente
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportClienteOutput> importClienteRequest(ImportClienteInput input) {
		br.com.senior.furb.basico.impl.ImportClienteImpl impl = new br.com.senior.furb.basico.impl.ImportClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.importClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportCliente
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportClienteOutput exportCliente(ExportClienteInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportClienteImpl impl = new br.com.senior.furb.basico.impl.ExportClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportCliente
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportCliente(ExportClienteInput input) {
		br.com.senior.furb.basico.impl.ExportClienteImpl impl = new br.com.senior.furb.basico.impl.ExportClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.exportCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportCliente
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportClienteOutput> exportClienteRequest(ExportClienteInput input) {
		br.com.senior.furb.basico.impl.ExportClienteImpl impl = new br.com.senior.furb.basico.impl.ExportClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método importTipoProduto
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportTipoProdutoOutput importTipoProduto(ImportTipoProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ImportTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importTipoProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public void importTipoProduto(ImportTipoProdutoInput input) {
		br.com.senior.furb.basico.impl.ImportTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ImportTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.importTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método importTipoProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportTipoProdutoOutput> importTipoProdutoRequest(ImportTipoProdutoInput input) {
		br.com.senior.furb.basico.impl.ImportTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ImportTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportTipoProduto
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportTipoProdutoOutput exportTipoProduto(ExportTipoProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ExportTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportTipoProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportTipoProduto(ExportTipoProdutoInput input) {
		br.com.senior.furb.basico.impl.ExportTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ExportTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.exportTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportTipoProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportTipoProdutoOutput> exportTipoProdutoRequest(ExportTipoProdutoInput input) {
		br.com.senior.furb.basico.impl.ExportTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ExportTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método importProduto
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportProdutoOutput importProduto(ImportProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportProdutoImpl impl = new br.com.senior.furb.basico.impl.ImportProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public void importProduto(ImportProdutoInput input) {
		br.com.senior.furb.basico.impl.ImportProdutoImpl impl = new br.com.senior.furb.basico.impl.ImportProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.importProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método importProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportProdutoOutput> importProdutoRequest(ImportProdutoInput input) {
		br.com.senior.furb.basico.impl.ImportProdutoImpl impl = new br.com.senior.furb.basico.impl.ImportProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportProduto
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportProdutoOutput exportProduto(ExportProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportProdutoImpl impl = new br.com.senior.furb.basico.impl.ExportProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportProduto(ExportProdutoInput input) {
		br.com.senior.furb.basico.impl.ExportProdutoImpl impl = new br.com.senior.furb.basico.impl.ExportProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.exportProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportProduto
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportProdutoOutput> exportProdutoRequest(ExportProdutoInput input) {
		br.com.senior.furb.basico.impl.ExportProdutoImpl impl = new br.com.senior.furb.basico.impl.ExportProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método importProduto_alugado
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportProduto_alugadoOutput importProduto_alugado(ImportProduto_alugadoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ImportProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importProduto_alugado
	 * This is a public operation
	 * 
	 */
	@Override
	public void importProduto_alugado(ImportProduto_alugadoInput input) {
		br.com.senior.furb.basico.impl.ImportProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ImportProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.importProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método importProduto_alugado
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportProduto_alugadoOutput> importProduto_alugadoRequest(ImportProduto_alugadoInput input) {
		br.com.senior.furb.basico.impl.ImportProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ImportProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportProduto_alugado
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportProduto_alugadoOutput exportProduto_alugado(ExportProduto_alugadoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ExportProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportProduto_alugado
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportProduto_alugado(ExportProduto_alugadoInput input) {
		br.com.senior.furb.basico.impl.ExportProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ExportProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.exportProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportProduto_alugado
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportProduto_alugadoOutput> exportProduto_alugadoRequest(ExportProduto_alugadoInput input) {
		br.com.senior.furb.basico.impl.ExportProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ExportProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método importLocacao
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ImportLocacaoOutput importLocacao(ImportLocacaoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ImportLocacaoImpl impl = new br.com.senior.furb.basico.impl.ImportLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método importLocacao
	 * This is a public operation
	 * 
	 */
	@Override
	public void importLocacao(ImportLocacaoInput input) {
		br.com.senior.furb.basico.impl.ImportLocacaoImpl impl = new br.com.senior.furb.basico.impl.ImportLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.importLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método importLocacao
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ImportLocacaoOutput> importLocacaoRequest(ImportLocacaoInput input) {
		br.com.senior.furb.basico.impl.ImportLocacaoImpl impl = new br.com.senior.furb.basico.impl.ImportLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.importLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método exportLocacao
	 * This is a public operation
	 * 
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public ExportLocacaoOutput exportLocacao(ExportLocacaoInput input, long timeout) {
		br.com.senior.furb.basico.impl.ExportLocacaoImpl impl = new br.com.senior.furb.basico.impl.ExportLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método exportLocacao
	 * This is a public operation
	 * 
	 */
	@Override
	public void exportLocacao(ExportLocacaoInput input) {
		br.com.senior.furb.basico.impl.ExportLocacaoImpl impl = new br.com.senior.furb.basico.impl.ExportLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.exportLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método exportLocacao
	 * This is a public operation
	 * 
	 */
	@Override
	public CompletableFuture<ExportLocacaoOutput> exportLocacaoRequest(ExportLocacaoInput input) {
		br.com.senior.furb.basico.impl.ExportLocacaoImpl impl = new br.com.senior.furb.basico.impl.ExportLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.exportLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método listLocadora
	 * This is a public operation
	 * The 'list' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locadora.PagedResults listLocadora(Locadora.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListLocadoraImpl impl = new br.com.senior.furb.basico.impl.ListLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.listLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listLocadora
	 * This is a public operation
	 * The 'list' request primitive for the Locadora entity.
	 */
	@Override
	public void listLocadora(Locadora.PageRequest input) {
		br.com.senior.furb.basico.impl.ListLocadoraImpl impl = new br.com.senior.furb.basico.impl.ListLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.listLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método listLocadora
	 * This is a public operation
	 * The 'list' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<Locadora.PagedResults> listLocadoraRequest(Locadora.PageRequest input) {
		br.com.senior.furb.basico.impl.ListLocadoraImpl impl = new br.com.senior.furb.basico.impl.ListLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.listLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método listTipoUsuario
	 * This is a public operation
	 * The 'list' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoUsuario.PagedResults listTipoUsuario(TipoUsuario.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ListTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.listTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listTipoUsuario
	 * This is a public operation
	 * The 'list' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void listTipoUsuario(TipoUsuario.PageRequest input) {
		br.com.senior.furb.basico.impl.ListTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ListTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.listTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método listTipoUsuario
	 * This is a public operation
	 * The 'list' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<TipoUsuario.PagedResults> listTipoUsuarioRequest(TipoUsuario.PageRequest input) {
		br.com.senior.furb.basico.impl.ListTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.ListTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.listTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método listCliente
	 * This is a public operation
	 * The 'list' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Cliente.PagedResults listCliente(Cliente.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListClienteImpl impl = new br.com.senior.furb.basico.impl.ListClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.listCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listCliente
	 * This is a public operation
	 * The 'list' request primitive for the Cliente entity.
	 */
	@Override
	public void listCliente(Cliente.PageRequest input) {
		br.com.senior.furb.basico.impl.ListClienteImpl impl = new br.com.senior.furb.basico.impl.ListClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.listCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método listCliente
	 * This is a public operation
	 * The 'list' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<Cliente.PagedResults> listClienteRequest(Cliente.PageRequest input) {
		br.com.senior.furb.basico.impl.ListClienteImpl impl = new br.com.senior.furb.basico.impl.ListClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.listClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método listTipoProduto
	 * This is a public operation
	 * The 'list' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoProduto.PagedResults listTipoProduto(TipoProduto.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ListTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listTipoProduto
	 * This is a public operation
	 * The 'list' request primitive for the TipoProduto entity.
	 */
	@Override
	public void listTipoProduto(TipoProduto.PageRequest input) {
		br.com.senior.furb.basico.impl.ListTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ListTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.listTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método listTipoProduto
	 * This is a public operation
	 * The 'list' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<TipoProduto.PagedResults> listTipoProdutoRequest(TipoProduto.PageRequest input) {
		br.com.senior.furb.basico.impl.ListTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.ListTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método listProduto
	 * This is a public operation
	 * The 'list' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto.PagedResults listProduto(Produto.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListProdutoImpl impl = new br.com.senior.furb.basico.impl.ListProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listProduto
	 * This is a public operation
	 * The 'list' request primitive for the Produto entity.
	 */
	@Override
	public void listProduto(Produto.PageRequest input) {
		br.com.senior.furb.basico.impl.ListProdutoImpl impl = new br.com.senior.furb.basico.impl.ListProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.listProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método listProduto
	 * This is a public operation
	 * The 'list' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<Produto.PagedResults> listProdutoRequest(Produto.PageRequest input) {
		br.com.senior.furb.basico.impl.ListProdutoImpl impl = new br.com.senior.furb.basico.impl.ListProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método listProduto_alugado
	 * This is a public operation
	 * The 'list' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto_alugado.PagedResults listProduto_alugado(Produto_alugado.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ListProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listProduto_alugado
	 * This is a public operation
	 * The 'list' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void listProduto_alugado(Produto_alugado.PageRequest input) {
		br.com.senior.furb.basico.impl.ListProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ListProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.listProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método listProduto_alugado
	 * This is a public operation
	 * The 'list' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<Produto_alugado.PagedResults> listProduto_alugadoRequest(Produto_alugado.PageRequest input) {
		br.com.senior.furb.basico.impl.ListProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.ListProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método listLocacao
	 * This is a public operation
	 * The 'list' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locacao.PagedResults listLocacao(Locacao.PageRequest input, long timeout) {
		br.com.senior.furb.basico.impl.ListLocacaoImpl impl = new br.com.senior.furb.basico.impl.ListLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método listLocacao
	 * This is a public operation
	 * The 'list' request primitive for the Locacao entity.
	 */
	@Override
	public void listLocacao(Locacao.PageRequest input) {
		br.com.senior.furb.basico.impl.ListLocacaoImpl impl = new br.com.senior.furb.basico.impl.ListLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.listLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método listLocacao
	 * This is a public operation
	 * The 'list' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<Locacao.PagedResults> listLocacaoRequest(Locacao.PageRequest input) {
		br.com.senior.furb.basico.impl.ListLocacaoImpl impl = new br.com.senior.furb.basico.impl.ListLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.listLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método getDependencies
	 * This is a public operation
	 * Returns a list with all dependencies from this service, along with their respective versions
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public GetDependenciesOutput getDependencies(long timeout) {
		br.com.senior.furb.basico.impl.GetDependenciesImpl impl = new br.com.senior.furb.basico.impl.GetDependenciesImpl(messengerSupplier, userId, messageSupplier);
		return impl.getDependencies(timeout);
	}
	
	/**
	 * Chamada assíncrona para o método getDependencies
	 * This is a public operation
	 * Returns a list with all dependencies from this service, along with their respective versions
	 */
	@Override
	public void getDependencies() {
		br.com.senior.furb.basico.impl.GetDependenciesImpl impl = new br.com.senior.furb.basico.impl.GetDependenciesImpl(messengerSupplier, userId, messageSupplier);
		impl.getDependencies();
	}
	
	/**
	 * Chamada assíncrona para o método getDependencies
	 * This is a public operation
	 * Returns a list with all dependencies from this service, along with their respective versions
	 */
	@Override
	public CompletableFuture<GetDependenciesOutput> getDependenciesRequest() {
		br.com.senior.furb.basico.impl.GetDependenciesImpl impl = new br.com.senior.furb.basico.impl.GetDependenciesImpl(messengerSupplier, userId, messageSupplier);
		return impl.getDependenciesRequest();
	}
	/**
	 * Chamada síncrona para o método createLocadora
	 * This is a public operation
	 * The 'create' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locadora createLocadora(Locadora input, long timeout) {
		br.com.senior.furb.basico.impl.CreateLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.createLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createLocadora
	 * This is a public operation
	 * The 'create' request primitive for the Locadora entity.
	 */
	@Override
	public void createLocadora(Locadora input) {
		br.com.senior.furb.basico.impl.CreateLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.createLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método createLocadora
	 * This is a public operation
	 * The 'create' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<Locadora> createLocadoraRequest(Locadora input) {
		br.com.senior.furb.basico.impl.CreateLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.createLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkLocadora
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkLocadoraOutput createBulkLocadora(CreateBulkLocadoraInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateBulkLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkLocadora
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locadora entity.
	 */
	@Override
	public void createBulkLocadora(CreateBulkLocadoraInput input) {
		br.com.senior.furb.basico.impl.CreateBulkLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateBulkLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkLocadora
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<CreateBulkLocadoraOutput> createBulkLocadoraRequest(CreateBulkLocadoraInput input) {
		br.com.senior.furb.basico.impl.CreateBulkLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateBulkLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeLocadora
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locadora createMergeLocadora(Locadora input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateMergeLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeLocadora
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locadora entity.
	 */
	@Override
	public void createMergeLocadora(Locadora input) {
		br.com.senior.furb.basico.impl.CreateMergeLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateMergeLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeLocadora
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<Locadora> createMergeLocadoraRequest(Locadora input) {
		br.com.senior.furb.basico.impl.CreateMergeLocadoraImpl impl = new br.com.senior.furb.basico.impl.CreateMergeLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveLocadora
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locadora retrieveLocadora(Locadora.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveLocadoraImpl impl = new br.com.senior.furb.basico.impl.RetrieveLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveLocadora
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locadora entity.
	 */
	@Override
	public void retrieveLocadora(Locadora.Id input) {
		br.com.senior.furb.basico.impl.RetrieveLocadoraImpl impl = new br.com.senior.furb.basico.impl.RetrieveLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveLocadora
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<Locadora> retrieveLocadoraRequest(Locadora.Id input) {
		br.com.senior.furb.basico.impl.RetrieveLocadoraImpl impl = new br.com.senior.furb.basico.impl.RetrieveLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateLocadora
	 * This is a public operation
	 * The 'update' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locadora updateLocadora(Locadora input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateLocadoraImpl impl = new br.com.senior.furb.basico.impl.UpdateLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateLocadora
	 * This is a public operation
	 * The 'update' request primitive for the Locadora entity.
	 */
	@Override
	public void updateLocadora(Locadora input) {
		br.com.senior.furb.basico.impl.UpdateLocadoraImpl impl = new br.com.senior.furb.basico.impl.UpdateLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.updateLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateLocadora
	 * This is a public operation
	 * The 'update' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<Locadora> updateLocadoraRequest(Locadora input) {
		br.com.senior.furb.basico.impl.UpdateLocadoraImpl impl = new br.com.senior.furb.basico.impl.UpdateLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeLocadora
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locadora updateMergeLocadora(Locadora input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeLocadoraImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeLocadora
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locadora entity.
	 */
	@Override
	public void updateMergeLocadora(Locadora input) {
		br.com.senior.furb.basico.impl.UpdateMergeLocadoraImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeLocadora(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeLocadora
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locadora entity.
	 */
	@Override
	public CompletableFuture<Locadora> updateMergeLocadoraRequest(Locadora input) {
		br.com.senior.furb.basico.impl.UpdateMergeLocadoraImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeLocadoraImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeLocadoraRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteLocadora
	 * This is a public operation
	 * The 'delete' request primitive for the Locadora entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteLocadora(Locadora.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteLocadoraImpl impl = new br.com.senior.furb.basico.impl.DeleteLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteLocadora(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteLocadora
	 * This is a public operation
	 * The 'delete' request primitive for the Locadora entity.
	 */
	@Override
	public void deleteLocadora(Locadora.Id input) {
		br.com.senior.furb.basico.impl.DeleteLocadoraImpl impl = new br.com.senior.furb.basico.impl.DeleteLocadoraImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteLocadora(input);
	}
	
	/**
	 * Chamada síncrona para o método createTipoUsuario
	 * This is a public operation
	 * The 'create' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoUsuario createTipoUsuario(TipoUsuario input, long timeout) {
		br.com.senior.furb.basico.impl.CreateTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.createTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createTipoUsuario
	 * This is a public operation
	 * The 'create' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void createTipoUsuario(TipoUsuario input) {
		br.com.senior.furb.basico.impl.CreateTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.createTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método createTipoUsuario
	 * This is a public operation
	 * The 'create' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<TipoUsuario> createTipoUsuarioRequest(TipoUsuario input) {
		br.com.senior.furb.basico.impl.CreateTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.createTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkTipoUsuario
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkTipoUsuarioOutput createBulkTipoUsuario(CreateBulkTipoUsuarioInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateBulkTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkTipoUsuario
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void createBulkTipoUsuario(CreateBulkTipoUsuarioInput input) {
		br.com.senior.furb.basico.impl.CreateBulkTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateBulkTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkTipoUsuario
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<CreateBulkTipoUsuarioOutput> createBulkTipoUsuarioRequest(CreateBulkTipoUsuarioInput input) {
		br.com.senior.furb.basico.impl.CreateBulkTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateBulkTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeTipoUsuario
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoUsuario createMergeTipoUsuario(TipoUsuario input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateMergeTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeTipoUsuario
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void createMergeTipoUsuario(TipoUsuario input) {
		br.com.senior.furb.basico.impl.CreateMergeTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateMergeTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeTipoUsuario
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<TipoUsuario> createMergeTipoUsuarioRequest(TipoUsuario input) {
		br.com.senior.furb.basico.impl.CreateMergeTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.CreateMergeTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveTipoUsuario
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoUsuario retrieveTipoUsuario(TipoUsuario.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.RetrieveTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveTipoUsuario
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void retrieveTipoUsuario(TipoUsuario.Id input) {
		br.com.senior.furb.basico.impl.RetrieveTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.RetrieveTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveTipoUsuario
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<TipoUsuario> retrieveTipoUsuarioRequest(TipoUsuario.Id input) {
		br.com.senior.furb.basico.impl.RetrieveTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.RetrieveTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateTipoUsuario
	 * This is a public operation
	 * The 'update' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoUsuario updateTipoUsuario(TipoUsuario input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.UpdateTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateTipoUsuario
	 * This is a public operation
	 * The 'update' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void updateTipoUsuario(TipoUsuario input) {
		br.com.senior.furb.basico.impl.UpdateTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.UpdateTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.updateTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateTipoUsuario
	 * This is a public operation
	 * The 'update' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<TipoUsuario> updateTipoUsuarioRequest(TipoUsuario input) {
		br.com.senior.furb.basico.impl.UpdateTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.UpdateTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeTipoUsuario
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoUsuario updateMergeTipoUsuario(TipoUsuario input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeTipoUsuario
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void updateMergeTipoUsuario(TipoUsuario input) {
		br.com.senior.furb.basico.impl.UpdateMergeTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeTipoUsuario(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeTipoUsuario
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoUsuario entity.
	 */
	@Override
	public CompletableFuture<TipoUsuario> updateMergeTipoUsuarioRequest(TipoUsuario input) {
		br.com.senior.furb.basico.impl.UpdateMergeTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeTipoUsuarioRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteTipoUsuario
	 * This is a public operation
	 * The 'delete' request primitive for the TipoUsuario entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteTipoUsuario(TipoUsuario.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.DeleteTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteTipoUsuario(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteTipoUsuario
	 * This is a public operation
	 * The 'delete' request primitive for the TipoUsuario entity.
	 */
	@Override
	public void deleteTipoUsuario(TipoUsuario.Id input) {
		br.com.senior.furb.basico.impl.DeleteTipoUsuarioImpl impl = new br.com.senior.furb.basico.impl.DeleteTipoUsuarioImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteTipoUsuario(input);
	}
	
	/**
	 * Chamada síncrona para o método createCliente
	 * This is a public operation
	 * The 'create' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Cliente createCliente(Cliente input, long timeout) {
		br.com.senior.furb.basico.impl.CreateClienteImpl impl = new br.com.senior.furb.basico.impl.CreateClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.createCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createCliente
	 * This is a public operation
	 * The 'create' request primitive for the Cliente entity.
	 */
	@Override
	public void createCliente(Cliente input) {
		br.com.senior.furb.basico.impl.CreateClienteImpl impl = new br.com.senior.furb.basico.impl.CreateClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.createCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método createCliente
	 * This is a public operation
	 * The 'create' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<Cliente> createClienteRequest(Cliente input) {
		br.com.senior.furb.basico.impl.CreateClienteImpl impl = new br.com.senior.furb.basico.impl.CreateClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.createClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkCliente
	 * This is a public operation
	 * The 'createBulk' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkClienteOutput createBulkCliente(CreateBulkClienteInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkClienteImpl impl = new br.com.senior.furb.basico.impl.CreateBulkClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkCliente
	 * This is a public operation
	 * The 'createBulk' request primitive for the Cliente entity.
	 */
	@Override
	public void createBulkCliente(CreateBulkClienteInput input) {
		br.com.senior.furb.basico.impl.CreateBulkClienteImpl impl = new br.com.senior.furb.basico.impl.CreateBulkClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkCliente
	 * This is a public operation
	 * The 'createBulk' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<CreateBulkClienteOutput> createBulkClienteRequest(CreateBulkClienteInput input) {
		br.com.senior.furb.basico.impl.CreateBulkClienteImpl impl = new br.com.senior.furb.basico.impl.CreateBulkClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeCliente
	 * This is a public operation
	 * The 'createMerge' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Cliente createMergeCliente(Cliente input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeClienteImpl impl = new br.com.senior.furb.basico.impl.CreateMergeClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeCliente
	 * This is a public operation
	 * The 'createMerge' request primitive for the Cliente entity.
	 */
	@Override
	public void createMergeCliente(Cliente input) {
		br.com.senior.furb.basico.impl.CreateMergeClienteImpl impl = new br.com.senior.furb.basico.impl.CreateMergeClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeCliente
	 * This is a public operation
	 * The 'createMerge' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<Cliente> createMergeClienteRequest(Cliente input) {
		br.com.senior.furb.basico.impl.CreateMergeClienteImpl impl = new br.com.senior.furb.basico.impl.CreateMergeClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveCliente
	 * This is a public operation
	 * The 'retrieve' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Cliente retrieveCliente(Cliente.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveClienteImpl impl = new br.com.senior.furb.basico.impl.RetrieveClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveCliente
	 * This is a public operation
	 * The 'retrieve' request primitive for the Cliente entity.
	 */
	@Override
	public void retrieveCliente(Cliente.Id input) {
		br.com.senior.furb.basico.impl.RetrieveClienteImpl impl = new br.com.senior.furb.basico.impl.RetrieveClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveCliente
	 * This is a public operation
	 * The 'retrieve' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<Cliente> retrieveClienteRequest(Cliente.Id input) {
		br.com.senior.furb.basico.impl.RetrieveClienteImpl impl = new br.com.senior.furb.basico.impl.RetrieveClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateCliente
	 * This is a public operation
	 * The 'update' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Cliente updateCliente(Cliente input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateClienteImpl impl = new br.com.senior.furb.basico.impl.UpdateClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateCliente
	 * This is a public operation
	 * The 'update' request primitive for the Cliente entity.
	 */
	@Override
	public void updateCliente(Cliente input) {
		br.com.senior.furb.basico.impl.UpdateClienteImpl impl = new br.com.senior.furb.basico.impl.UpdateClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.updateCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateCliente
	 * This is a public operation
	 * The 'update' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<Cliente> updateClienteRequest(Cliente input) {
		br.com.senior.furb.basico.impl.UpdateClienteImpl impl = new br.com.senior.furb.basico.impl.UpdateClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeCliente
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Cliente updateMergeCliente(Cliente input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeClienteImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeCliente
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Cliente entity.
	 */
	@Override
	public void updateMergeCliente(Cliente input) {
		br.com.senior.furb.basico.impl.UpdateMergeClienteImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeCliente(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeCliente
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Cliente entity.
	 */
	@Override
	public CompletableFuture<Cliente> updateMergeClienteRequest(Cliente input) {
		br.com.senior.furb.basico.impl.UpdateMergeClienteImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeClienteImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeClienteRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteCliente
	 * This is a public operation
	 * The 'delete' request primitive for the Cliente entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteCliente(Cliente.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteClienteImpl impl = new br.com.senior.furb.basico.impl.DeleteClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteCliente(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteCliente
	 * This is a public operation
	 * The 'delete' request primitive for the Cliente entity.
	 */
	@Override
	public void deleteCliente(Cliente.Id input) {
		br.com.senior.furb.basico.impl.DeleteClienteImpl impl = new br.com.senior.furb.basico.impl.DeleteClienteImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteCliente(input);
	}
	
	/**
	 * Chamada síncrona para o método createTipoProduto
	 * This is a public operation
	 * The 'create' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoProduto createTipoProduto(TipoProduto input, long timeout) {
		br.com.senior.furb.basico.impl.CreateTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createTipoProduto
	 * This is a public operation
	 * The 'create' request primitive for the TipoProduto entity.
	 */
	@Override
	public void createTipoProduto(TipoProduto input) {
		br.com.senior.furb.basico.impl.CreateTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.createTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método createTipoProduto
	 * This is a public operation
	 * The 'create' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<TipoProduto> createTipoProdutoRequest(TipoProduto input) {
		br.com.senior.furb.basico.impl.CreateTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkTipoProduto
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkTipoProdutoOutput createBulkTipoProduto(CreateBulkTipoProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkTipoProduto
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoProduto entity.
	 */
	@Override
	public void createBulkTipoProduto(CreateBulkTipoProdutoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkTipoProduto
	 * This is a public operation
	 * The 'createBulk' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<CreateBulkTipoProdutoOutput> createBulkTipoProdutoRequest(CreateBulkTipoProdutoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeTipoProduto
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoProduto createMergeTipoProduto(TipoProduto input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeTipoProduto
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoProduto entity.
	 */
	@Override
	public void createMergeTipoProduto(TipoProduto input) {
		br.com.senior.furb.basico.impl.CreateMergeTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeTipoProduto
	 * This is a public operation
	 * The 'createMerge' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<TipoProduto> createMergeTipoProdutoRequest(TipoProduto input) {
		br.com.senior.furb.basico.impl.CreateMergeTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveTipoProduto
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoProduto retrieveTipoProduto(TipoProduto.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.RetrieveTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveTipoProduto
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoProduto entity.
	 */
	@Override
	public void retrieveTipoProduto(TipoProduto.Id input) {
		br.com.senior.furb.basico.impl.RetrieveTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.RetrieveTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveTipoProduto
	 * This is a public operation
	 * The 'retrieve' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<TipoProduto> retrieveTipoProdutoRequest(TipoProduto.Id input) {
		br.com.senior.furb.basico.impl.RetrieveTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.RetrieveTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateTipoProduto
	 * This is a public operation
	 * The 'update' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoProduto updateTipoProduto(TipoProduto input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateTipoProduto
	 * This is a public operation
	 * The 'update' request primitive for the TipoProduto entity.
	 */
	@Override
	public void updateTipoProduto(TipoProduto input) {
		br.com.senior.furb.basico.impl.UpdateTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateTipoProduto
	 * This is a public operation
	 * The 'update' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<TipoProduto> updateTipoProdutoRequest(TipoProduto input) {
		br.com.senior.furb.basico.impl.UpdateTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeTipoProduto
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public TipoProduto updateMergeTipoProduto(TipoProduto input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeTipoProduto
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoProduto entity.
	 */
	@Override
	public void updateMergeTipoProduto(TipoProduto input) {
		br.com.senior.furb.basico.impl.UpdateMergeTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeTipoProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeTipoProduto
	 * This is a public operation
	 * The 'updateMerge' request primitive for the TipoProduto entity.
	 */
	@Override
	public CompletableFuture<TipoProduto> updateMergeTipoProdutoRequest(TipoProduto input) {
		br.com.senior.furb.basico.impl.UpdateMergeTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeTipoProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteTipoProduto
	 * This is a public operation
	 * The 'delete' request primitive for the TipoProduto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteTipoProduto(TipoProduto.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.DeleteTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteTipoProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteTipoProduto
	 * This is a public operation
	 * The 'delete' request primitive for the TipoProduto entity.
	 */
	@Override
	public void deleteTipoProduto(TipoProduto.Id input) {
		br.com.senior.furb.basico.impl.DeleteTipoProdutoImpl impl = new br.com.senior.furb.basico.impl.DeleteTipoProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteTipoProduto(input);
	}
	
	/**
	 * Chamada síncrona para o método createProduto
	 * This is a public operation
	 * The 'create' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto createProduto(Produto input, long timeout) {
		br.com.senior.furb.basico.impl.CreateProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createProduto
	 * This is a public operation
	 * The 'create' request primitive for the Produto entity.
	 */
	@Override
	public void createProduto(Produto input) {
		br.com.senior.furb.basico.impl.CreateProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.createProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método createProduto
	 * This is a public operation
	 * The 'create' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<Produto> createProdutoRequest(Produto input) {
		br.com.senior.furb.basico.impl.CreateProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkProduto
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkProdutoOutput createBulkProduto(CreateBulkProdutoInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkProduto
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto entity.
	 */
	@Override
	public void createBulkProduto(CreateBulkProdutoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkProduto
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<CreateBulkProdutoOutput> createBulkProdutoRequest(CreateBulkProdutoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeProduto
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto createMergeProduto(Produto input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeProduto
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto entity.
	 */
	@Override
	public void createMergeProduto(Produto input) {
		br.com.senior.furb.basico.impl.CreateMergeProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeProduto
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<Produto> createMergeProdutoRequest(Produto input) {
		br.com.senior.furb.basico.impl.CreateMergeProdutoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveProduto
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto retrieveProduto(Produto.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveProdutoImpl impl = new br.com.senior.furb.basico.impl.RetrieveProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveProduto
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto entity.
	 */
	@Override
	public void retrieveProduto(Produto.Id input) {
		br.com.senior.furb.basico.impl.RetrieveProdutoImpl impl = new br.com.senior.furb.basico.impl.RetrieveProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveProduto
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<Produto> retrieveProdutoRequest(Produto.Id input) {
		br.com.senior.furb.basico.impl.RetrieveProdutoImpl impl = new br.com.senior.furb.basico.impl.RetrieveProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateProduto
	 * This is a public operation
	 * The 'update' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto updateProduto(Produto input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateProduto
	 * This is a public operation
	 * The 'update' request primitive for the Produto entity.
	 */
	@Override
	public void updateProduto(Produto input) {
		br.com.senior.furb.basico.impl.UpdateProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateProduto
	 * This is a public operation
	 * The 'update' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<Produto> updateProdutoRequest(Produto input) {
		br.com.senior.furb.basico.impl.UpdateProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeProduto
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto updateMergeProduto(Produto input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeProduto
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto entity.
	 */
	@Override
	public void updateMergeProduto(Produto input) {
		br.com.senior.furb.basico.impl.UpdateMergeProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeProduto(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeProduto
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto entity.
	 */
	@Override
	public CompletableFuture<Produto> updateMergeProdutoRequest(Produto input) {
		br.com.senior.furb.basico.impl.UpdateMergeProdutoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeProdutoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeProdutoRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteProduto
	 * This is a public operation
	 * The 'delete' request primitive for the Produto entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteProduto(Produto.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteProdutoImpl impl = new br.com.senior.furb.basico.impl.DeleteProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteProduto(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteProduto
	 * This is a public operation
	 * The 'delete' request primitive for the Produto entity.
	 */
	@Override
	public void deleteProduto(Produto.Id input) {
		br.com.senior.furb.basico.impl.DeleteProdutoImpl impl = new br.com.senior.furb.basico.impl.DeleteProdutoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteProduto(input);
	}
	
	/**
	 * Chamada síncrona para o método createProduto_alugado
	 * This is a public operation
	 * The 'create' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto_alugado createProduto_alugado(Produto_alugado input, long timeout) {
		br.com.senior.furb.basico.impl.CreateProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createProduto_alugado
	 * This is a public operation
	 * The 'create' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void createProduto_alugado(Produto_alugado input) {
		br.com.senior.furb.basico.impl.CreateProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.createProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método createProduto_alugado
	 * This is a public operation
	 * The 'create' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<Produto_alugado> createProduto_alugadoRequest(Produto_alugado input) {
		br.com.senior.furb.basico.impl.CreateProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkProduto_alugado
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkProduto_alugadoOutput createBulkProduto_alugado(CreateBulkProduto_alugadoInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkProduto_alugado
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void createBulkProduto_alugado(CreateBulkProduto_alugadoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkProduto_alugado
	 * This is a public operation
	 * The 'createBulk' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<CreateBulkProduto_alugadoOutput> createBulkProduto_alugadoRequest(CreateBulkProduto_alugadoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeProduto_alugado
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto_alugado createMergeProduto_alugado(Produto_alugado input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeProduto_alugado
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void createMergeProduto_alugado(Produto_alugado input) {
		br.com.senior.furb.basico.impl.CreateMergeProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeProduto_alugado
	 * This is a public operation
	 * The 'createMerge' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<Produto_alugado> createMergeProduto_alugadoRequest(Produto_alugado input) {
		br.com.senior.furb.basico.impl.CreateMergeProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveProduto_alugado
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto_alugado retrieveProduto_alugado(Produto_alugado.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.RetrieveProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveProduto_alugado
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void retrieveProduto_alugado(Produto_alugado.Id input) {
		br.com.senior.furb.basico.impl.RetrieveProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.RetrieveProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveProduto_alugado
	 * This is a public operation
	 * The 'retrieve' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<Produto_alugado> retrieveProduto_alugadoRequest(Produto_alugado.Id input) {
		br.com.senior.furb.basico.impl.RetrieveProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.RetrieveProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateProduto_alugado
	 * This is a public operation
	 * The 'update' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto_alugado updateProduto_alugado(Produto_alugado input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.UpdateProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateProduto_alugado
	 * This is a public operation
	 * The 'update' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void updateProduto_alugado(Produto_alugado input) {
		br.com.senior.furb.basico.impl.UpdateProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.UpdateProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateProduto_alugado
	 * This is a public operation
	 * The 'update' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<Produto_alugado> updateProduto_alugadoRequest(Produto_alugado input) {
		br.com.senior.furb.basico.impl.UpdateProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.UpdateProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeProduto_alugado
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Produto_alugado updateMergeProduto_alugado(Produto_alugado input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeProduto_alugado
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void updateMergeProduto_alugado(Produto_alugado input) {
		br.com.senior.furb.basico.impl.UpdateMergeProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeProduto_alugado(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeProduto_alugado
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Produto_alugado entity.
	 */
	@Override
	public CompletableFuture<Produto_alugado> updateMergeProduto_alugadoRequest(Produto_alugado input) {
		br.com.senior.furb.basico.impl.UpdateMergeProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeProduto_alugadoRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteProduto_alugado
	 * This is a public operation
	 * The 'delete' request primitive for the Produto_alugado entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteProduto_alugado(Produto_alugado.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.DeleteProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteProduto_alugado(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteProduto_alugado
	 * This is a public operation
	 * The 'delete' request primitive for the Produto_alugado entity.
	 */
	@Override
	public void deleteProduto_alugado(Produto_alugado.Id input) {
		br.com.senior.furb.basico.impl.DeleteProduto_alugadoImpl impl = new br.com.senior.furb.basico.impl.DeleteProduto_alugadoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteProduto_alugado(input);
	}
	
	/**
	 * Chamada síncrona para o método createLocacao
	 * This is a public operation
	 * The 'create' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locacao createLocacao(Locacao input, long timeout) {
		br.com.senior.furb.basico.impl.CreateLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createLocacao
	 * This is a public operation
	 * The 'create' request primitive for the Locacao entity.
	 */
	@Override
	public void createLocacao(Locacao input) {
		br.com.senior.furb.basico.impl.CreateLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.createLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método createLocacao
	 * This is a public operation
	 * The 'create' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<Locacao> createLocacaoRequest(Locacao input) {
		br.com.senior.furb.basico.impl.CreateLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createBulkLocacao
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public CreateBulkLocacaoOutput createBulkLocacao(CreateBulkLocacaoInput input, long timeout) {
		br.com.senior.furb.basico.impl.CreateBulkLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkLocacao
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locacao entity.
	 */
	@Override
	public void createBulkLocacao(CreateBulkLocacaoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.createBulkLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método createBulkLocacao
	 * This is a public operation
	 * The 'createBulk' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<CreateBulkLocacaoOutput> createBulkLocacaoRequest(CreateBulkLocacaoInput input) {
		br.com.senior.furb.basico.impl.CreateBulkLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateBulkLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createBulkLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método createMergeLocacao
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locacao createMergeLocacao(Locacao input, long timeout) {
		br.com.senior.furb.basico.impl.CreateMergeLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeLocacao
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locacao entity.
	 */
	@Override
	public void createMergeLocacao(Locacao input) {
		br.com.senior.furb.basico.impl.CreateMergeLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.createMergeLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método createMergeLocacao
	 * This is a public operation
	 * The 'createMerge' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<Locacao> createMergeLocacaoRequest(Locacao input) {
		br.com.senior.furb.basico.impl.CreateMergeLocacaoImpl impl = new br.com.senior.furb.basico.impl.CreateMergeLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.createMergeLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método retrieveLocacao
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locacao retrieveLocacao(Locacao.Id input, long timeout) {
		br.com.senior.furb.basico.impl.RetrieveLocacaoImpl impl = new br.com.senior.furb.basico.impl.RetrieveLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveLocacao
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locacao entity.
	 */
	@Override
	public void retrieveLocacao(Locacao.Id input) {
		br.com.senior.furb.basico.impl.RetrieveLocacaoImpl impl = new br.com.senior.furb.basico.impl.RetrieveLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.retrieveLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método retrieveLocacao
	 * This is a public operation
	 * The 'retrieve' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<Locacao> retrieveLocacaoRequest(Locacao.Id input) {
		br.com.senior.furb.basico.impl.RetrieveLocacaoImpl impl = new br.com.senior.furb.basico.impl.RetrieveLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.retrieveLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateLocacao
	 * This is a public operation
	 * The 'update' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locacao updateLocacao(Locacao input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateLocacaoImpl impl = new br.com.senior.furb.basico.impl.UpdateLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateLocacao
	 * This is a public operation
	 * The 'update' request primitive for the Locacao entity.
	 */
	@Override
	public void updateLocacao(Locacao input) {
		br.com.senior.furb.basico.impl.UpdateLocacaoImpl impl = new br.com.senior.furb.basico.impl.UpdateLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateLocacao
	 * This is a public operation
	 * The 'update' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<Locacao> updateLocacaoRequest(Locacao input) {
		br.com.senior.furb.basico.impl.UpdateLocacaoImpl impl = new br.com.senior.furb.basico.impl.UpdateLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método updateMergeLocacao
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public Locacao updateMergeLocacao(Locacao input, long timeout) {
		br.com.senior.furb.basico.impl.UpdateMergeLocacaoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeLocacao
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locacao entity.
	 */
	@Override
	public void updateMergeLocacao(Locacao input) {
		br.com.senior.furb.basico.impl.UpdateMergeLocacaoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.updateMergeLocacao(input);
	}
	
	/**
	 * Chamada assíncrona para o método updateMergeLocacao
	 * This is a public operation
	 * The 'updateMerge' request primitive for the Locacao entity.
	 */
	@Override
	public CompletableFuture<Locacao> updateMergeLocacaoRequest(Locacao input) {
		br.com.senior.furb.basico.impl.UpdateMergeLocacaoImpl impl = new br.com.senior.furb.basico.impl.UpdateMergeLocacaoImpl(messengerSupplier, userId, messageSupplier);
		return impl.updateMergeLocacaoRequest(input);
	}
	/**
	 * Chamada síncrona para o método deleteLocacao
	 * This is a public operation
	 * The 'delete' request primitive for the Locacao entity.
	 * @throws BasicoMessageException quando um erro com payload for retornado pela mensageria
	 */
	@Override
	public void deleteLocacao(Locacao.Id input, long timeout) {
		br.com.senior.furb.basico.impl.DeleteLocacaoImpl impl = new br.com.senior.furb.basico.impl.DeleteLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteLocacao(input, timeout);
	}
	
	/**
	 * Chamada assíncrona para o método deleteLocacao
	 * This is a public operation
	 * The 'delete' request primitive for the Locacao entity.
	 */
	@Override
	public void deleteLocacao(Locacao.Id input) {
		br.com.senior.furb.basico.impl.DeleteLocacaoImpl impl = new br.com.senior.furb.basico.impl.DeleteLocacaoImpl(messengerSupplier, userId, messageSupplier);
		impl.deleteLocacao(input);
	}
	


	/**
	* Chamada assíncrona para o método publishServiceStarted
	* Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	*/
	public void publishServiceStarted( ServiceStartedPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.SERVICE_STARTED, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishNotifyUserEvent
	* Warning: this operation is PRIVATE and may have its behavior changed at any time without notice
	*/
	public void publishNotifyUserEvent( NotifyUserEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.NOTIFY_USER_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportLocadoraEvent
	* This is a public operation
	*/
	public void publishImportLocadoraEvent( ImportLocadoraEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_LOCADORA_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportLocadoraEvent
	* This is a public operation
	*/
	public void publishExportLocadoraEvent( ExportLocadoraEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_LOCADORA_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportTipoUsuarioEvent
	* This is a public operation
	*/
	public void publishImportTipoUsuarioEvent( ImportTipoUsuarioEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_TIPO_USUARIO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportTipoUsuarioEvent
	* This is a public operation
	*/
	public void publishExportTipoUsuarioEvent( ExportTipoUsuarioEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_TIPO_USUARIO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportClienteEvent
	* This is a public operation
	*/
	public void publishImportClienteEvent( ImportClienteEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_CLIENTE_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportClienteEvent
	* This is a public operation
	*/
	public void publishExportClienteEvent( ExportClienteEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_CLIENTE_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportTipoProdutoEvent
	* This is a public operation
	*/
	public void publishImportTipoProdutoEvent( ImportTipoProdutoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_TIPO_PRODUTO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportTipoProdutoEvent
	* This is a public operation
	*/
	public void publishExportTipoProdutoEvent( ExportTipoProdutoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_TIPO_PRODUTO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportProdutoEvent
	* This is a public operation
	*/
	public void publishImportProdutoEvent( ImportProdutoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_PRODUTO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportProdutoEvent
	* This is a public operation
	*/
	public void publishExportProdutoEvent( ExportProdutoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_PRODUTO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportProduto_alugadoEvent
	* This is a public operation
	*/
	public void publishImportProduto_alugadoEvent( ImportProduto_alugadoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_PRODUTOALUGADO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportProduto_alugadoEvent
	* This is a public operation
	*/
	public void publishExportProduto_alugadoEvent( ExportProduto_alugadoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_PRODUTOALUGADO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishImportLocacaoEvent
	* This is a public operation
	*/
	public void publishImportLocacaoEvent( ImportLocacaoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.IMPORT_LOCACAO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	
	/**
	* Chamada assíncrona para o método publishExportLocacaoEvent
	* This is a public operation
	*/
	public void publishExportLocacaoEvent( ExportLocacaoEventPayload input ) {
	
		Message message = new Message(userId.getTenant(), BasicoConstants.DOMAIN, BasicoConstants.SERVICE, BasicoConstants.Events.EXPORT_LOCACAO_EVENT, DtoJsonConverter.toJSON(input));
		try {
			addMessageHeaders(message);
			messengerSupplier.get().publish(message);
		} catch (Exception e) {
			throw new BasicoException("Erro ao enviar a mensagem", e);
		}
	}
	

	private void addMessageHeaders(Message message) {
		message.setUsername(userId.getUsername());
		if (userId.isTrusted()) {
			message.addHeader("trusted", true);
		}
	}
}
