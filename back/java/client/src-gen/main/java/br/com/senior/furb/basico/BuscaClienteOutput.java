package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.senior.furb.basico.BasicoValidator;
import br.com.senior.furb.basico.*;

public class BuscaClienteOutput {
    
    /**
     * Clientes encontrados
     */
    public java.util.List<Cliente> clientes;
    
    public BuscaClienteOutput() {
    }
    
    /** 
     * This constructor allows initialization of all fields, required and optional.
     */
    public BuscaClienteOutput(java.util.List<Cliente> clientes) {
        this.clientes = clientes;
    }
    
    public void validate() {
        validate(true);
    }
    
    public void validate(boolean required) {
        validate(null, required);
    }
    
    public void validate(Map<String, Object> headers, boolean required) {
    	validate(headers, required, new ArrayList<>());
    }
    
    void validate(Map<String, Object> headers, boolean required, List<Object> validated) {
    	BasicoValidator.validate(this, headers, required, validated);
    }
    @Override
    public int hashCode() {
        int ret = 1;
        if (clientes != null) {
            ret = 31 * ret + clientes.hashCode();
        }
        return ret;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BuscaClienteOutput)) {
            return false;
        }
        BuscaClienteOutput other = (BuscaClienteOutput) obj;
        if ((clientes == null) != (other.clientes == null)) {
            return false;
        }
        if ((clientes != null) && !clientes.equals(other.clientes)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	toString(sb, new ArrayList<>());
    	return sb.toString();
    }
    
    void toString(StringBuilder sb, List<Object> appended) {
    	sb.append(getClass().getSimpleName()).append(" [");
    	if (appended.contains(this)) {
    		sb.append("<Previously appended object>").append(']');
    		return;
    	}
    	appended.add(this);
    	sb.append("clientes=<");
    	if (clientes == null) {
    		sb.append("null");
    	} else {
    		sb.append('[');
    		int last = clientes.size() - 1;
    		for (int i = 0; i <= last; i++) {
    			clientes.get(i).toString(sb, appended);
    			if (i < last) {
    				sb.append(", ");
    			}
    		}
    		sb.append(']');
    	}
    	sb.append('>');
    	sb.append(']');
    }
    
}
