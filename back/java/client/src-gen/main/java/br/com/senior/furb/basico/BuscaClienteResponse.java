/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.BuscaClienteOutput;

/**
 * Response method for buscaCliente
 */
@CommandDescription(name="buscaClienteResponse", kind=CommandKind.ResponseCommand, requestPrimitive="buscaClienteResponse")
public interface BuscaClienteResponse extends MessageHandler {

	void buscaClienteResponse(BuscaClienteOutput response);
	
	void buscaClienteResponseError(ErrorPayload error);

}
