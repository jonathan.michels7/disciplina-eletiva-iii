package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import br.com.senior.furb.basico.BasicoValidator;
import br.com.senior.furb.basico.*;

public class BuscaProdutoOutput {
    
    /**
     * Produto encontrado
     */
    public java.util.List<Produto> produtos;
    
    public BuscaProdutoOutput() {
    }
    
    /** 
     * This constructor allows initialization of all fields, required and optional.
     */
    public BuscaProdutoOutput(java.util.List<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public void validate() {
        validate(true);
    }
    
    public void validate(boolean required) {
        validate(null, required);
    }
    
    public void validate(Map<String, Object> headers, boolean required) {
    	validate(headers, required, new ArrayList<>());
    }
    
    void validate(Map<String, Object> headers, boolean required, List<Object> validated) {
    	BasicoValidator.validate(this, headers, required, validated);
    }
    @Override
    public int hashCode() {
        int ret = 1;
        if (produtos != null) {
            ret = 31 * ret + produtos.hashCode();
        }
        return ret;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BuscaProdutoOutput)) {
            return false;
        }
        BuscaProdutoOutput other = (BuscaProdutoOutput) obj;
        if ((produtos == null) != (other.produtos == null)) {
            return false;
        }
        if ((produtos != null) && !produtos.equals(other.produtos)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	toString(sb, new ArrayList<>());
    	return sb.toString();
    }
    
    void toString(StringBuilder sb, List<Object> appended) {
    	sb.append(getClass().getSimpleName()).append(" [");
    	if (appended.contains(this)) {
    		sb.append("<Previously appended object>").append(']');
    		return;
    	}
    	appended.add(this);
    	sb.append("produtos=<");
    	if (produtos == null) {
    		sb.append("null");
    	} else {
    		sb.append('[');
    		int last = produtos.size() - 1;
    		for (int i = 0; i <= last; i++) {
    			produtos.get(i).toString(sb, appended);
    			if (i < last) {
    				sb.append(", ");
    			}
    		}
    		sb.append(']');
    	}
    	sb.append('>');
    	sb.append(']');
    }
    
}
