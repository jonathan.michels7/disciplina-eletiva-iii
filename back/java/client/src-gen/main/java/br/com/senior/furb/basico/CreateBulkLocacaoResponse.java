/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.CreateBulkLocacaoOutput;

/**
 * Response method for createBulkLocacao
 */
@CommandDescription(name="createBulkLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createBulkLocacaoResponse")
public interface CreateBulkLocacaoResponse extends MessageHandler {

	void createBulkLocacaoResponse(CreateBulkLocacaoOutput response);
	
	void createBulkLocacaoResponseError(ErrorPayload error);

}
