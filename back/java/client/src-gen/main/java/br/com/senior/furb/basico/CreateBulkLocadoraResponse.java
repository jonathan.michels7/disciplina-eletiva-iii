/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.CreateBulkLocadoraOutput;

/**
 * Response method for createBulkLocadora
 */
@CommandDescription(name="createBulkLocadoraResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createBulkLocadoraResponse")
public interface CreateBulkLocadoraResponse extends MessageHandler {

	void createBulkLocadoraResponse(CreateBulkLocadoraOutput response);
	
	void createBulkLocadoraResponseError(ErrorPayload error);

}
