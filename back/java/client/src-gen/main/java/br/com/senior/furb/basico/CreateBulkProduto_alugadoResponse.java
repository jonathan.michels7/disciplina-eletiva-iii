/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.CreateBulkProduto_alugadoOutput;

/**
 * Response method for createBulkProduto_alugado
 */
@CommandDescription(name="createBulkProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createBulkProduto_alugadoResponse")
public interface CreateBulkProduto_alugadoResponse extends MessageHandler {

	void createBulkProduto_alugadoResponse(CreateBulkProduto_alugadoOutput response);
	
	void createBulkProduto_alugadoResponseError(ErrorPayload error);

}
