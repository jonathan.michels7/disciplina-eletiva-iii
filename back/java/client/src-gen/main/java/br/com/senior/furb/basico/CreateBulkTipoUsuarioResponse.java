/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.CreateBulkTipoUsuarioOutput;

/**
 * Response method for createBulkTipoUsuario
 */
@CommandDescription(name="createBulkTipoUsuarioResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createBulkTipoUsuarioResponse")
public interface CreateBulkTipoUsuarioResponse extends MessageHandler {

	void createBulkTipoUsuarioResponse(CreateBulkTipoUsuarioOutput response);
	
	void createBulkTipoUsuarioResponseError(ErrorPayload error);

}
