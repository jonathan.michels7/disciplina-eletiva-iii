/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Locacao;

/**
 * Response method for createLocacao
 */
@CommandDescription(name="createLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createLocacaoResponse")
public interface CreateLocacaoResponse extends MessageHandler {

	void createLocacaoResponse(Locacao response);
	
	void createLocacaoResponseError(ErrorPayload error);

}
