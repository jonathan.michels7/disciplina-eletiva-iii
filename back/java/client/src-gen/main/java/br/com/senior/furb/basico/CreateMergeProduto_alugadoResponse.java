/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Produto_alugado;

/**
 * Response method for createMergeProduto_alugado
 */
@CommandDescription(name="createMergeProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createMergeProduto_alugadoResponse")
public interface CreateMergeProduto_alugadoResponse extends MessageHandler {

	void createMergeProduto_alugadoResponse(Produto_alugado response);
	
	void createMergeProduto_alugadoResponseError(ErrorPayload error);

}
