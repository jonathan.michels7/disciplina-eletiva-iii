/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.TipoProduto;

/**
 * Response method for createMergeTipoProduto
 */
@CommandDescription(name="createMergeTipoProdutoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createMergeTipoProdutoResponse")
public interface CreateMergeTipoProdutoResponse extends MessageHandler {

	void createMergeTipoProdutoResponse(TipoProduto response);
	
	void createMergeTipoProdutoResponseError(ErrorPayload error);

}
