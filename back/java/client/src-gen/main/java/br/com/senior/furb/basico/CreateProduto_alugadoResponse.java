/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Produto_alugado;

/**
 * Response method for createProduto_alugado
 */
@CommandDescription(name="createProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createProduto_alugadoResponse")
public interface CreateProduto_alugadoResponse extends MessageHandler {

	void createProduto_alugadoResponse(Produto_alugado response);
	
	void createProduto_alugadoResponseError(ErrorPayload error);

}
