/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.TipoProduto;

/**
 * Response method for createTipoProduto
 */
@CommandDescription(name="createTipoProdutoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="createTipoProdutoResponse")
public interface CreateTipoProdutoResponse extends MessageHandler {

	void createTipoProdutoResponse(TipoProduto response);
	
	void createTipoProdutoResponseError(ErrorPayload error);

}
