/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ExportLocacaoOutput;

/**
 * Response method for exportLocacao
 */
@CommandDescription(name="exportLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="exportLocacaoResponse")
public interface ExportLocacaoResponse extends MessageHandler {

	void exportLocacaoResponse(ExportLocacaoOutput response);
	
	void exportLocacaoResponseError(ErrorPayload error);

}
