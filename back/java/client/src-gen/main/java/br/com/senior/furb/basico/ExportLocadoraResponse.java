/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ExportLocadoraOutput;

/**
 * Response method for exportLocadora
 */
@CommandDescription(name="exportLocadoraResponse", kind=CommandKind.ResponseCommand, requestPrimitive="exportLocadoraResponse")
public interface ExportLocadoraResponse extends MessageHandler {

	void exportLocadoraResponse(ExportLocadoraOutput response);
	
	void exportLocadoraResponseError(ErrorPayload error);

}
