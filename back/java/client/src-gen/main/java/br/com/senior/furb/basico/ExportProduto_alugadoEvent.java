/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@SubscriptionDescription(domain="furb", service="basico", event="exportProduto_alugadoEvent")
public interface ExportProduto_alugadoEvent extends MessageHandler {
    public void exportProduto_alugadoEvent(ExportProduto_alugadoEventPayload payload);
    
}
