/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ExportProduto_alugadoOutput;

/**
 * Response method for exportProduto_alugado
 */
@CommandDescription(name="exportProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="exportProduto_alugadoResponse")
public interface ExportProduto_alugadoResponse extends MessageHandler {

	void exportProduto_alugadoResponse(ExportProduto_alugadoOutput response);
	
	void exportProduto_alugadoResponseError(ErrorPayload error);

}
