/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ExportTipoUsuarioOutput;

/**
 * Response method for exportTipoUsuario
 */
@CommandDescription(name="exportTipoUsuarioResponse", kind=CommandKind.ResponseCommand, requestPrimitive="exportTipoUsuarioResponse")
public interface ExportTipoUsuarioResponse extends MessageHandler {

	void exportTipoUsuarioResponse(ExportTipoUsuarioOutput response);
	
	void exportTipoUsuarioResponseError(ErrorPayload error);

}
