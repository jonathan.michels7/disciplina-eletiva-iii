/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@SubscriptionDescription(domain="furb", service="basico", event="importLocacaoEvent")
public interface ImportLocacaoEvent extends MessageHandler {
    public void importLocacaoEvent(ImportLocacaoEventPayload payload);
    
}
