/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ImportLocacaoOutput;

/**
 * Response method for importLocacao
 */
@CommandDescription(name="importLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="importLocacaoResponse")
public interface ImportLocacaoResponse extends MessageHandler {

	void importLocacaoResponse(ImportLocacaoOutput response);
	
	void importLocacaoResponseError(ErrorPayload error);

}
