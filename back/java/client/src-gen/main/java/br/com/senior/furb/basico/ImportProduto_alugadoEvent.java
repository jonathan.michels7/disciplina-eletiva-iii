/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@SubscriptionDescription(domain="furb", service="basico", event="importProduto_alugadoEvent")
public interface ImportProduto_alugadoEvent extends MessageHandler {
    public void importProduto_alugadoEvent(ImportProduto_alugadoEventPayload payload);
    
}
