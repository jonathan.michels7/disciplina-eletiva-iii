/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ImportProduto_alugadoOutput;

/**
 * Response method for importProduto_alugado
 */
@CommandDescription(name="importProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="importProduto_alugadoResponse")
public interface ImportProduto_alugadoResponse extends MessageHandler {

	void importProduto_alugadoResponse(ImportProduto_alugadoOutput response);
	
	void importProduto_alugadoResponseError(ErrorPayload error);

}
