/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ImportTipoProdutoOutput;

/**
 * Response method for importTipoProduto
 */
@CommandDescription(name="importTipoProdutoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="importTipoProdutoResponse")
public interface ImportTipoProdutoResponse extends MessageHandler {

	void importTipoProdutoResponse(ImportTipoProdutoOutput response);
	
	void importTipoProdutoResponseError(ErrorPayload error);

}
