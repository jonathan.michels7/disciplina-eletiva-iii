/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.ImportTipoUsuarioOutput;

/**
 * Response method for importTipoUsuario
 */
@CommandDescription(name="importTipoUsuarioResponse", kind=CommandKind.ResponseCommand, requestPrimitive="importTipoUsuarioResponse")
public interface ImportTipoUsuarioResponse extends MessageHandler {

	void importTipoUsuarioResponse(ImportTipoUsuarioOutput response);
	
	void importTipoUsuarioResponseError(ErrorPayload error);

}
