/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;

/**
 * Response method for listLocacao
 */
@CommandDescription(name="listLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="listLocacaoResponse")
public interface ListLocacaoResponse extends MessageHandler {

	void listLocacaoResponse(Locacao.PagedResults response);
	
	void listLocacaoResponseError(ErrorPayload error);

}
