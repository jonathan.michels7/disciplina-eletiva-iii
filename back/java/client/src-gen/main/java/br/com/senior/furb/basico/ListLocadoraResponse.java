/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;

/**
 * Response method for listLocadora
 */
@CommandDescription(name="listLocadoraResponse", kind=CommandKind.ResponseCommand, requestPrimitive="listLocadoraResponse")
public interface ListLocadoraResponse extends MessageHandler {

	void listLocadoraResponse(Locadora.PagedResults response);
	
	void listLocadoraResponseError(ErrorPayload error);

}
