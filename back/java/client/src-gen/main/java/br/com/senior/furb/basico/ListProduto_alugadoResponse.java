/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;

/**
 * Response method for listProduto_alugado
 */
@CommandDescription(name="listProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="listProduto_alugadoResponse")
public interface ListProduto_alugadoResponse extends MessageHandler {

	void listProduto_alugadoResponse(Produto_alugado.PagedResults response);
	
	void listProduto_alugadoResponseError(ErrorPayload error);

}
