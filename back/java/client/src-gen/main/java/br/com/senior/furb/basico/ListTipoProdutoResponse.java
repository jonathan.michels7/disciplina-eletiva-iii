/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;

/**
 * Response method for listTipoProduto
 */
@CommandDescription(name="listTipoProdutoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="listTipoProdutoResponse")
public interface ListTipoProdutoResponse extends MessageHandler {

	void listTipoProdutoResponse(TipoProduto.PagedResults response);
	
	void listTipoProdutoResponseError(ErrorPayload error);

}
