/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;

/**
 * Response method for listTipoUsuario
 */
@CommandDescription(name="listTipoUsuarioResponse", kind=CommandKind.ResponseCommand, requestPrimitive="listTipoUsuarioResponse")
public interface ListTipoUsuarioResponse extends MessageHandler {

	void listTipoUsuarioResponse(TipoUsuario.PagedResults response);
	
	void listTipoUsuarioResponseError(ErrorPayload error);

}
