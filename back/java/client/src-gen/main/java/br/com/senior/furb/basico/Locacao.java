package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import br.com.senior.messaging.model.EntityDescription;
import br.com.senior.messaging.model.EntityId;

/**
 * Locação
 */
@EntityDescription
public class Locacao {
    
    public static class Id {
    	
        public String id;
        
        public Id() {
        }
        
        public Id(String locacaoId) {
            this.id = locacaoId;
        }
        
        public String getLocacaoId() {
            return id;
        }
        
        public String getId() {
            return id;
        }
        
    	public void normalize(Map<String, Object> headers) {
    		BasicoNormalizer.normalize(this, headers);
    	}
    	
    }
    
    public static class PagedResults {
    	public Long totalPages;
    	public Long totalElements;
    	
        public List<Locacao> contents;
        
        public PagedResults() {
        }
        
        public PagedResults(List<Locacao> contents) {
            this.contents = contents;
        }
        
        public PagedResults(List<Locacao> contents, Long totalPages, Long totalElements) {
            this.contents = contents;
            this.totalPages = totalPages;
            this.totalElements = totalElements;
        }
    }
    
    public static class PageRequest {
        public Long offset;
        public Long size;
        public boolean translations;
        public String orderBy;
        public String filter;
        public List<String> displayFields;
        public boolean useCustomFilter;
        
        public PageRequest() {
        }
        
        public PageRequest(Long offset, Long size) {
            this(offset, size, null, null);
        }
        
        public PageRequest(Long offset, Long size, String orderBy) {
            this(offset, size, orderBy, null);
        }
        
        public PageRequest(Long offset, Long size, String orderBy, String filter) {
            this(offset, size, orderBy, filter, null);
       	}
       	
        public PageRequest(Long offset, Long size, String orderBy, String filter, List<String> displayFields) {
            this(offset, size, orderBy, filter, displayFields, false);
       	}
        
        public PageRequest(Long offset, Long size, String orderBy, String filter, List<String> displayFields, boolean useCustomFilter) {
        	this.offset = offset;
        	this.size = size;
        	this.orderBy = orderBy;
        	this.filter = filter;
        	this.displayFields = displayFields;
        	this.useCustomFilter = useCustomFilter;
        }
    }

    @EntityId
    /**
     * Chave primária
     */
    public String id;
    /**
     * Produtos
     */
    public java.util.List<Produto_alugado> produtos;
    /**
     * Data do pedido
     */
    public java.time.LocalDate data_pedido;
    /**
     * Observação
     */
    public String obsevacao;
    /**
     * Data de entrega
     */
    public java.time.LocalDate data_entrega;
    /**
     * Valor pago
     */
    public Double valor_pago;
    /**
     * cliente
     */
    public Cliente cliente;
    
    public Locacao() {
    }
    
    /** 
     * This constructor allows initialization of all fields, required and optional.
     */
    public Locacao(String id, java.util.List<Produto_alugado> produtos, java.time.LocalDate data_pedido, String obsevacao, java.time.LocalDate data_entrega, Double valor_pago, Cliente cliente) {
        this.id = id;
        this.produtos = produtos;
        this.data_pedido = data_pedido;
        this.obsevacao = obsevacao;
        this.data_entrega = data_entrega;
        this.valor_pago = valor_pago;
        this.cliente = cliente;
    }
    /** 
     * This convenience constructor allows initialization of all required fields.
     */
    public Locacao(java.util.List<Produto_alugado> produtos, java.time.LocalDate data_pedido, String obsevacao, Cliente cliente) {
        this.produtos = produtos;
        this.data_pedido = data_pedido;
        this.obsevacao = obsevacao;
        this.cliente = cliente;
    }
    
    public void normalize(Map<String, Object> headers) {
    	BasicoNormalizer.normalize(this, headers);
    }
    
    public void validate() {
    	validate(true);
    }
    
    public void validate(boolean required) {
    	validate(null, true);
    }
    
    public void validate(Map<String, Object> headers, boolean required) {
    	validate(headers, required, new ArrayList<>());
    }
    
    void validate(Map<String, Object> headers, boolean required, List<Object> validated) {
    	BasicoValidator.validate(this, headers, required, validated);
    }
    @Override
    public int hashCode() {
        int ret = 1;
        if (id != null) {
            ret = 31 * ret + id.hashCode();
        }
        return ret;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Locacao)) {
            return false;
        }
        Locacao other = (Locacao) obj;
        if ((id == null) != (other.id == null)) {
            return false;
        }
        if ((id != null) && !id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	toString(sb, new ArrayList<>());
    	return sb.toString();
    }
    
    void toString(StringBuilder sb, List<Object> appended) {
    	sb.append(getClass().getSimpleName()).append(" [");
    	if (appended.contains(this)) {
    		sb.append("<Previously appended object>").append(']');
    		return;
    	}
    	appended.add(this);
    	sb.append("id=").append(id == null ? "null" : id).append(", ");
    	sb.append("produtos=<");
    	if (produtos == null) {
    		sb.append("null");
    	} else {
    		sb.append('[');
    		int last = produtos.size() - 1;
    		for (int i = 0; i <= last; i++) {
    			produtos.get(i).toString(sb, appended);
    			if (i < last) {
    				sb.append(", ");
    			}
    		}
    		sb.append(']');
    	}
    	sb.append('>').append(", ");
    	sb.append("data_pedido=").append(data_pedido == null ? "null" : data_pedido).append(", ");
    	sb.append("obsevacao=").append(obsevacao == null ? "null" : obsevacao).append(", ");
    	sb.append("data_entrega=").append(data_entrega == null ? "null" : data_entrega).append(", ");
    	sb.append("valor_pago=").append(valor_pago == null ? "null" : valor_pago).append(", ");
    	sb.append("cliente=<");
    	if (cliente == null) {
    		sb.append("null");
    	} else {
    		cliente.toString(sb, appended);
    	}
    	sb.append('>');
    	sb.append(']');
    }
    
}
