/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Locacao;

/**
 * Response method for retrieveLocacao
 */
@CommandDescription(name="retrieveLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="retrieveLocacaoResponse")
public interface RetrieveLocacaoResponse extends MessageHandler {

	void retrieveLocacaoResponse(Locacao response);
	
	void retrieveLocacaoResponseError(ErrorPayload error);

}
