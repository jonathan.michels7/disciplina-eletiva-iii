/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Produto_alugado;

/**
 * Response method for retrieveProduto_alugado
 */
@CommandDescription(name="retrieveProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="retrieveProduto_alugadoResponse")
public interface RetrieveProduto_alugadoResponse extends MessageHandler {

	void retrieveProduto_alugadoResponse(Produto_alugado response);
	
	void retrieveProduto_alugadoResponseError(ErrorPayload error);

}
