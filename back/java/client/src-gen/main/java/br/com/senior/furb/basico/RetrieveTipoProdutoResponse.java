/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.TipoProduto;

/**
 * Response method for retrieveTipoProduto
 */
@CommandDescription(name="retrieveTipoProdutoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="retrieveTipoProdutoResponse")
public interface RetrieveTipoProdutoResponse extends MessageHandler {

	void retrieveTipoProdutoResponse(TipoProduto response);
	
	void retrieveTipoProdutoResponseError(ErrorPayload error);

}
