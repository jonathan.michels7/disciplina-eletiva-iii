/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.TipoUsuario;

/**
 * Response method for retrieveTipoUsuario
 */
@CommandDescription(name="retrieveTipoUsuarioResponse", kind=CommandKind.ResponseCommand, requestPrimitive="retrieveTipoUsuarioResponse")
public interface RetrieveTipoUsuarioResponse extends MessageHandler {

	void retrieveTipoUsuarioResponse(TipoUsuario response);
	
	void retrieveTipoUsuarioResponseError(ErrorPayload error);

}
