package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import br.com.senior.messaging.model.EntityDescription;
import br.com.senior.messaging.model.EntityId;

/**
 * Tipo de produto
 */
@EntityDescription
public class TipoProduto {
    
    public static class Id {
    	
        public String id;
        
        public Id() {
        }
        
        public Id(String tipoProdutoId) {
            this.id = tipoProdutoId;
        }
        
        public String getTipoProdutoId() {
            return id;
        }
        
        public String getId() {
            return id;
        }
        
    	public void normalize(Map<String, Object> headers) {
    		BasicoNormalizer.normalize(this, headers);
    	}
    	
    }
    
    public static class PagedResults {
    	public Long totalPages;
    	public Long totalElements;
    	
        public List<TipoProduto> contents;
        
        public PagedResults() {
        }
        
        public PagedResults(List<TipoProduto> contents) {
            this.contents = contents;
        }
        
        public PagedResults(List<TipoProduto> contents, Long totalPages, Long totalElements) {
            this.contents = contents;
            this.totalPages = totalPages;
            this.totalElements = totalElements;
        }
    }
    
    public static class PageRequest {
        public Long offset;
        public Long size;
        public boolean translations;
        public String orderBy;
        public String filter;
        public List<String> displayFields;
        public boolean useCustomFilter;
        
        public PageRequest() {
        }
        
        public PageRequest(Long offset, Long size) {
            this(offset, size, null, null);
        }
        
        public PageRequest(Long offset, Long size, String orderBy) {
            this(offset, size, orderBy, null);
        }
        
        public PageRequest(Long offset, Long size, String orderBy, String filter) {
            this(offset, size, orderBy, filter, null);
       	}
       	
        public PageRequest(Long offset, Long size, String orderBy, String filter, List<String> displayFields) {
            this(offset, size, orderBy, filter, displayFields, false);
       	}
        
        public PageRequest(Long offset, Long size, String orderBy, String filter, List<String> displayFields, boolean useCustomFilter) {
        	this.offset = offset;
        	this.size = size;
        	this.orderBy = orderBy;
        	this.filter = filter;
        	this.displayFields = displayFields;
        	this.useCustomFilter = useCustomFilter;
        }
    }

    @EntityId
    /**
     * Chave primária
     */
    public String id;
    /**
     * descrição
     */
    public String descricao;
    /**
     * valor que é cobfado
     */
    public Double valor;
    /**
     * dias que pode ficar alugado
     */
    public Long dias_alugado;
    
    public TipoProduto() {
    }
    
    /** 
     * This constructor allows initialization of all fields, required and optional.
     */
    public TipoProduto(String id, String descricao, Double valor, Long dias_alugado) {
        this.id = id;
        this.descricao = descricao;
        this.valor = valor;
        this.dias_alugado = dias_alugado;
    }
    /** 
     * This convenience constructor allows initialization of all required fields.
     */
    public TipoProduto(String descricao, Double valor, Long dias_alugado) {
        this.descricao = descricao;
        this.valor = valor;
        this.dias_alugado = dias_alugado;
    }
    
    public void normalize(Map<String, Object> headers) {
    	BasicoNormalizer.normalize(this, headers);
    }
    
    public void validate() {
    	validate(true);
    }
    
    public void validate(boolean required) {
    	validate(null, true);
    }
    
    public void validate(Map<String, Object> headers, boolean required) {
    	validate(headers, required, new ArrayList<>());
    }
    
    void validate(Map<String, Object> headers, boolean required, List<Object> validated) {
    	BasicoValidator.validate(this, headers, required, validated);
    }
    @Override
    public int hashCode() {
        int ret = 1;
        if (id != null) {
            ret = 31 * ret + id.hashCode();
        }
        return ret;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TipoProduto)) {
            return false;
        }
        TipoProduto other = (TipoProduto) obj;
        if ((id == null) != (other.id == null)) {
            return false;
        }
        if ((id != null) && !id.equals(other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
    	StringBuilder sb = new StringBuilder();
    	toString(sb, new ArrayList<>());
    	return sb.toString();
    }
    
    void toString(StringBuilder sb, List<Object> appended) {
    	sb.append(getClass().getSimpleName()).append(" [");
    	if (appended.contains(this)) {
    		sb.append("<Previously appended object>").append(']');
    		return;
    	}
    	appended.add(this);
    	sb.append("id=").append(id == null ? "null" : id).append(", ");
    	sb.append("descricao=").append(descricao == null ? "null" : descricao).append(", ");
    	sb.append("valor=").append(valor == null ? "null" : valor).append(", ");
    	sb.append("dias_alugado=").append(dias_alugado == null ? "null" : dias_alugado);
    	sb.append(']');
    }
    
}
