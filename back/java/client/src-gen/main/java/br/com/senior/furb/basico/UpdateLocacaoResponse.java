/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Locacao;

/**
 * Response method for updateLocacao
 */
@CommandDescription(name="updateLocacaoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="updateLocacaoResponse")
public interface UpdateLocacaoResponse extends MessageHandler {

	void updateLocacaoResponse(Locacao response);
	
	void updateLocacaoResponseError(ErrorPayload error);

}
