/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Locadora;

/**
 * Response method for updateLocadora
 */
@CommandDescription(name="updateLocadoraResponse", kind=CommandKind.ResponseCommand, requestPrimitive="updateLocadoraResponse")
public interface UpdateLocadoraResponse extends MessageHandler {

	void updateLocadoraResponse(Locadora response);
	
	void updateLocadoraResponseError(ErrorPayload error);

}
