/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Produto_alugado;

/**
 * Response method for updateMergeProduto_alugado
 */
@CommandDescription(name="updateMergeProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="updateMergeProduto_alugadoResponse")
public interface UpdateMergeProduto_alugadoResponse extends MessageHandler {

	void updateMergeProduto_alugadoResponse(Produto_alugado response);
	
	void updateMergeProduto_alugadoResponseError(ErrorPayload error);

}
