/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.TipoUsuario;

/**
 * Response method for updateMergeTipoUsuario
 */
@CommandDescription(name="updateMergeTipoUsuarioResponse", kind=CommandKind.ResponseCommand, requestPrimitive="updateMergeTipoUsuarioResponse")
public interface UpdateMergeTipoUsuarioResponse extends MessageHandler {

	void updateMergeTipoUsuarioResponse(TipoUsuario response);
	
	void updateMergeTipoUsuarioResponseError(ErrorPayload error);

}
