/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;	

import br.com.senior.messaging.ErrorPayload;
import br.com.senior.messaging.model.CommandDescription;
import br.com.senior.messaging.model.CommandKind;
import br.com.senior.messaging.model.MessageHandler;
import br.com.senior.furb.basico.Produto_alugado;

/**
 * Response method for updateProduto_alugado
 */
@CommandDescription(name="updateProduto_alugadoResponse", kind=CommandKind.ResponseCommand, requestPrimitive="updateProduto_alugadoResponse")
public interface UpdateProduto_alugadoResponse extends MessageHandler {

	void updateProduto_alugadoResponse(Produto_alugado response);
	
	void updateProduto_alugadoResponseError(ErrorPayload error);

}
