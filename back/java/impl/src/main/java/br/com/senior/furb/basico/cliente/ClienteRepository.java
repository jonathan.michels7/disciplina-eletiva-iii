package br.com.senior.furb.basico.cliente;

import org.springframework.stereotype.Repository;

import br.com.senior.furb.basico.ClienteBaseRepository;
@Repository
public interface ClienteRepository extends ClienteBaseRepository {

}
