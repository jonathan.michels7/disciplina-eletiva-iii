package br.com.senior.furb.basico.cliente;

import br.com.senior.furb.basico.BuscaClienteOutput;

public interface ClienteRepositoryCustom {

	BuscaClienteOutput pesquisaClientes(String nome);
}
