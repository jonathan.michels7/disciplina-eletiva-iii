package br.com.senior.furb.basico.cliente;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.furb.basico.BuscaClienteOutput;
import br.com.senior.furb.basico.Cliente;
import br.com.senior.furb.basico.ClienteEntity;
import br.com.senior.furb.basico.QClienteEntity;
import br.com.senior.furb.basico.QTipoUsuarioEntity;
import br.com.senior.furb.basico.TipoUsuario;
import br.com.senior.furb.basico.core.RepositoryBaseJpa;

@Repository
public class ClienteRepositoryCustomImpl  extends RepositoryBaseJpa implements ClienteRepositoryCustom{

	@Override
	public BuscaClienteOutput pesquisaClientes(String nome) {
		
		QClienteEntity cliente = QClienteEntity.clienteEntity;
		QTipoUsuarioEntity tipoUsuario = QTipoUsuarioEntity.tipoUsuarioEntity;
		
		JPAQuery<Tuple> query = 
				select(cliente, tipoUsuario).from(cliente).leftJoin(tipoUsuario).on(tipoUsuario.eq(cliente.tipo))
				.where(cliente.nome.containsIgnoreCase(nome));
		
		List<Tuple> listaUsuario = query.fetch();
		
		BuscaClienteOutput retorno = new BuscaClienteOutput(new ArrayList<Cliente>());
		
		if(listaUsuario.size() > 0) {
			for (int i = 0; i < listaUsuario.size(); i++) {
				retorno.clientes.add(new Cliente(
						listaUsuario.get(i).get(0, ClienteEntity.class).getId().toString(),
						listaUsuario.get(i).get(0, ClienteEntity.class).getNome(), 
						listaUsuario.get(i).get(0, ClienteEntity.class).getData_nascimento(), 
						listaUsuario.get(i).get(0, ClienteEntity.class).getCpf(),
						listaUsuario.get(i).get(0, ClienteEntity.class).getNome_login(),
						listaUsuario.get(i).get(0, ClienteEntity.class).getSenha_login(), 
						new TipoUsuario(
								listaUsuario.get(i).get(0, ClienteEntity.class).getTipo().getId().toString(),
								listaUsuario.get(i).get(0, ClienteEntity.class).getTipo().getNome()
								)));
			}
		}
		
		return retorno;
	}

}
