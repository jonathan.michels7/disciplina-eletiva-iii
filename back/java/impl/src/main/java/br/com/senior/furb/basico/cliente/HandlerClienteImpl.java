package br.com.senior.furb.basico.cliente;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.senior.furb.basico.BuscaCliente;
import br.com.senior.furb.basico.BuscaClienteInput;
import br.com.senior.furb.basico.BuscaClienteOutput;
import br.com.senior.messaging.model.HandlerImpl;

@HandlerImpl
public class HandlerClienteImpl implements BuscaCliente{

	@Autowired
	ClienteRepositoryCustom clienteRepositoryCustom;
	
	@Override
	public BuscaClienteOutput buscaCliente(BuscaClienteInput request) {
		return clienteRepositoryCustom.pesquisaClientes(request.nome);
	}
}
