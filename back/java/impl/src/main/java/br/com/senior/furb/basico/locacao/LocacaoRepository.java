package br.com.senior.furb.basico.locacao;

import org.springframework.stereotype.Repository;

import br.com.senior.furb.basico.LocacaoBaseRepository;

@Repository
public interface LocacaoRepository extends LocacaoBaseRepository {

}
