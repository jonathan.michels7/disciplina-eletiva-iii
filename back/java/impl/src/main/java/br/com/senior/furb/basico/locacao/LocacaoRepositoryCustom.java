package br.com.senior.furb.basico.locacao;

public interface LocacaoRepositoryCustom {
	/**
	 * Quantidade de itens
	 * @param id
	 * @return
	 */
	Long findQuantityById(String id);

}
