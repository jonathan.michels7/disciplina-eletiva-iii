package br.com.senior.furb.basico.locadora;

import org.springframework.stereotype.Repository;

import br.com.senior.furb.basico.LocadoraBaseRepository;

@Repository
public interface LocadoraRepository extends LocadoraBaseRepository {

}
