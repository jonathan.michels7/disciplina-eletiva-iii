package br.com.senior.furb.basico.locadora;

public interface LocadoraRepositoryCustom {
	/**
	 * Quantidade de itens
	 * @param id
	 * @return
	 */
	Long findQuantityById(String id);

}
