package br.com.senior.furb.basico.produto;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.senior.furb.basico.RetornaEstoque;
import br.com.senior.furb.basico.RetornaEstoqueInput;
import br.com.senior.furb.basico.RetornaEstoqueOutput;
import br.com.senior.messaging.model.HandlerImpl;

@HandlerImpl
public class HandlerEstoqueImpl implements RetornaEstoque {

	@Autowired
	ProdutoRepositoryCustom produtoRepositoryCustom;

	@Override
	public RetornaEstoqueOutput retornaEstoque(RetornaEstoqueInput request) {
		return new RetornaEstoqueOutput(produtoRepositoryCustom.buscaQuantidade(request.idProduto));
	}
}
