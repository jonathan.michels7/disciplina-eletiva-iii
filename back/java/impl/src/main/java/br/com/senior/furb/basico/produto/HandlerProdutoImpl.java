package br.com.senior.furb.basico.produto;

import org.springframework.beans.factory.annotation.Autowired;

import br.com.senior.furb.basico.BuscaProduto;
import br.com.senior.furb.basico.BuscaProdutoInput;
import br.com.senior.furb.basico.BuscaProdutoOutput;
import br.com.senior.messaging.model.HandlerImpl;

@HandlerImpl
public class HandlerProdutoImpl implements BuscaProduto {

	@Autowired
	ProdutoRepositoryCustom produtoRepositoryCustom;

	@Override
	public BuscaProdutoOutput buscaProduto(BuscaProdutoInput request) {
		return produtoRepositoryCustom.pesquisaProdutos(request.nome,
				request.descricao);
	}
}
