package br.com.senior.furb.basico.produto;

import org.springframework.stereotype.Repository;

import br.com.senior.furb.basico.ProdutoBaseRepository;

@Repository
public interface ProdutoRepository extends ProdutoBaseRepository {

}
