package br.com.senior.furb.basico.produto;

import br.com.senior.furb.basico.BuscaProdutoOutput;

public interface ProdutoRepositoryCustom {
	/**
	 * Quantidade de produtos
	 * @param id
	 * @return
	 */
	Long buscaQuantidade(String id);

	BuscaProdutoOutput pesquisaProdutos(String nome, String descricao);
}
