package br.com.senior.furb.basico.produto;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Repository;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;

import br.com.senior.furb.basico.BuscaProdutoOutput;
import br.com.senior.furb.basico.Produto;
import br.com.senior.furb.basico.ProdutoEntity;
import br.com.senior.furb.basico.QLocacaoEntity;
import br.com.senior.furb.basico.QProdutoEntity;
import br.com.senior.furb.basico.QProduto_alugadoEntity;
import br.com.senior.furb.basico.QTipoProdutoEntity;
import br.com.senior.furb.basico.TipoProduto;
import br.com.senior.furb.basico.core.RepositoryBaseJpa;

@Repository
public class ProdutoRepositoryCustomImpl extends RepositoryBaseJpa implements ProdutoRepositoryCustom {

	@Override
	public Long buscaQuantidade(String id) {
		try {
			UUID uuidToFind = UUID.fromString(id);

			QProdutoEntity produto = QProdutoEntity.produtoEntity;
			QLocacaoEntity locacao = QLocacaoEntity.locacaoEntity;
			QProduto_alugadoEntity produto_alugado = QProduto_alugadoEntity.produto_alugadoEntity;

			JPAQuery<Long> query = select(produto_alugado.qtd_alugado).from(produto_alugado)
					.innerJoin(locacao).on(locacao.produtos.contains(produto_alugado).and(locacao.valor_pago.eq(0.00)))
					.where(produto_alugado.produto.id.eq(uuidToFind));

			List<Long> listaValor = query.fetch();
			
			long totalAlugado = 0;
			for (int i = 0; i < listaValor.size(); i++) {
				totalAlugado += listaValor.get(i);
			} 

			JPAQuery<Long> query2 = select(produto.quantidade).from(produto).where(produto.id.eq(uuidToFind));
			List<Long> listaValor2 = query2.fetch();

			return (listaValor2.size() > 0 ? listaValor2.get(0) : new Long(0)) - totalAlugado;
		} catch (Exception e) {
			return new Long(0);
		}

	}

	@Override
	public BuscaProdutoOutput pesquisaProdutos(String nome, String descricao) {
		
		if(nome == null && descricao == null) {
			return null;
		}
		BuscaProdutoOutput retorno = new BuscaProdutoOutput(new ArrayList<Produto>());	
		
		QProdutoEntity produto = QProdutoEntity.produtoEntity;
		QTipoProdutoEntity tipoProduto = QTipoProdutoEntity.tipoProdutoEntity;
		
		JPAQuery<Tuple> query;
		
		if(nome != null && !nome.isEmpty() &&
				descricao != null && !descricao.isEmpty()) {
			query = select(produto, tipoProduto).from(produto).leftJoin(tipoProduto).on(tipoProduto.eq(produto.tipo)).where(produto.nome.containsIgnoreCase(nome).and(produto.descricao.containsIgnoreCase(descricao)));
		}else if(nome != null && !nome.isEmpty()) {
			query = select(produto, tipoProduto).from(produto).leftJoin(tipoProduto).on(tipoProduto.eq(produto.tipo)).where(produto.nome.containsIgnoreCase(nome));
		}else{
			query = select(produto, tipoProduto).from(produto).leftJoin(tipoProduto).on(tipoProduto.eq(produto.tipo)).where(produto.descricao.containsIgnoreCase(descricao));
		}
		
		List<Tuple> listaProduto = query.fetch();
		
		if(listaProduto.size() > 0 ) {
			for (int i = 0; i < listaProduto.size(); i++) {
				retorno.produtos.add(new Produto(
						listaProduto.get(i).get(0, ProdutoEntity.class).getId().toString(),
						listaProduto.get(i).get(0, ProdutoEntity.class).getNome(), 
						listaProduto.get(i).get(0, ProdutoEntity.class).getDescricao(), 
						listaProduto.get(i).get(0, ProdutoEntity.class).getQuantidade(),
						new TipoProduto(
								listaProduto.get(i).get(0, ProdutoEntity.class).getTipo().getId().toString(),
								listaProduto.get(i).get(0, ProdutoEntity.class).getTipo().getDescricao(), 
								listaProduto.get(i).get(0, ProdutoEntity.class).getTipo().getValor(), 
								listaProduto.get(i).get(0, ProdutoEntity.class).getTipo().getDias_alugado()), 
						listaProduto.get(i).get(0, ProdutoEntity.class).getIdImagem()));
				
			}
		}
		
		return retorno;
	}
}
