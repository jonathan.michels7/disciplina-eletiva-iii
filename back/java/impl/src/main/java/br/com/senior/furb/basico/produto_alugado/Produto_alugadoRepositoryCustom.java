package br.com.senior.furb.basico.produto_alugado;

public interface Produto_alugadoRepositoryCustom {
	/**
	 * Quantidade de itens
	 * @param id
	 * @return
	 */
	Long findQuantityById(String id);

}
