package br.com.senior.furb.basico.tipoProduto;

import org.springframework.stereotype.Repository;

import br.com.senior.furb.basico.TipoProdutoBaseRepository;

@Repository
public interface TipoProdutoRepository extends TipoProdutoBaseRepository {

}
