package br.com.senior.furb.basico.tipoProduto;

public interface TipoProdutoRepositoryCustom {
	/**
	 * Quantidade de itens
	 * @param id
	 * @return
	 */
	Long findQuantityById(String id);

}
