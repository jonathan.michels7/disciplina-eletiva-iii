package br.com.senior.furb.basico.tipoUsuario;

import org.springframework.stereotype.Repository;

import br.com.senior.furb.basico.TipoUsuarioBaseRepository;

@Repository
public interface TipoUsuarioRepository extends TipoUsuarioBaseRepository {

}
