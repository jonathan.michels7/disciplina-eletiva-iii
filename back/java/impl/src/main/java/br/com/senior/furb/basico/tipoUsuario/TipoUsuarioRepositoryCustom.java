package br.com.senior.furb.basico.tipoUsuario;

public interface TipoUsuarioRepositoryCustom {
	/**
	 * Quantidade de itens
	 * @param id
	 * @return
	 */
	Long findQuantityById(String id);

}
