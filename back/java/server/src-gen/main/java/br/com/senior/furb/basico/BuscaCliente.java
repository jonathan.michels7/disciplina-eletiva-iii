/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * Busca usuario conforme nome
 */
@CommandDescription(name="buscaCliente", kind=CommandKind.Query, requestPrimitive="buscaCliente", responsePrimitive="buscaClienteResponse")
public interface BuscaCliente extends MessageHandler {
    
    public BuscaClienteOutput buscaCliente(BuscaClienteInput request);
    
}
