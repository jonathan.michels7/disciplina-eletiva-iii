/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * Busca produto conforme informações passadas
 */
@CommandDescription(name="buscaProduto", kind=CommandKind.Query, requestPrimitive="buscaProduto", responsePrimitive="buscaProdutoResponse")
public interface BuscaProduto extends MessageHandler {
    
    public BuscaProdutoOutput buscaProduto(BuscaProdutoInput request);
    
}
