/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import br.com.senior.custom.ConversionContext;
import br.com.senior.custom.ConvertedObjectCondition;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import br.com.senior.custom.EntityDTOConverter;

@Component("furb.basico.ClienteDTOConverter")
@Lazy
public class ClienteDTOConverter extends EntityDTOConverter {

	@Autowired
	private TipoUsuarioDTOConverter tipoUsuarioConverter;

	@Override
	public void setupMapperToEntity(ModelMapper mapper, ConversionContext conversionContext) {
		Converter<TipoUsuario, TipoUsuarioEntity> tipoConverter = new ContextualizedConverter<TipoUsuario, TipoUsuarioEntity>() {
			@Override
			public TipoUsuarioEntity convert(TipoUsuario source) {
				return toEntity(source, TipoUsuarioEntity.class, conversionContext);
			}
		};

		PropertyMap<Cliente, ClienteEntity> clienteMap = new PropertyMap<Cliente, ClienteEntity>() {
			@Override
			protected void configure() {
				using(tipoConverter).map(source.tipo).setTipo(null);
			}
		};
		mapper.addMappings(clienteMap);
		if (mapper.getTypeMap(TipoUsuario.class, TipoUsuarioEntity.class) == null) {
			this.tipoUsuarioConverter.setupMapperToEntity(mapper, conversionContext);
		}
		mapper.getTypeMap(Cliente.class, ClienteEntity.class).setPropertyCondition(new ConvertedObjectCondition(conversionContext));
	}

	@Override
	public void setupMapperToDTO(ModelMapper mapper, ConversionContext conversionContext) {
		
		Converter<TipoUsuarioEntity, TipoUsuario> tipoConverter = new ContextualizedConverter<TipoUsuarioEntity, TipoUsuario>() {
			@Override
			public TipoUsuario convert(TipoUsuarioEntity source) {
				if (conversionContext.isIncludeTranslations()) {
					return toDTOWithTranslations(source, TipoUsuario.class, conversionContext.getRelationshipFields("tipo"), conversionContext);
				}
				return toDTO(source, TipoUsuario.class, conversionContext.getRelationshipFields("tipo"), conversionContext);
			}
		};
		
		//eager relationships
		PropertyMap<ClienteEntity, Cliente> clienteMap = new PropertyMap<ClienteEntity, Cliente>() {
			@Override
			public void configure() {
				using(tipoConverter).map(source.getTipo(), destination.tipo);
			}
		};
		//lazy relationships
		
		mapper.addMappings(clienteMap);
		
		if (mapper.getTypeMap(TipoUsuarioEntity.class, TipoUsuario.class) == null) {
		    this.tipoUsuarioConverter.setupMapperToDTO(mapper, conversionContext);
		}
		mapper.getTypeMap(ClienteEntity.class, Cliente.class).setPropertyCondition(new ConvertedObjectCondition(conversionContext));
	}
}
