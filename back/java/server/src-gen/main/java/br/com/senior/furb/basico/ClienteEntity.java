/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name="cliente")
public class ClienteEntity {
	
	public static final String SECURITY_RESOURCE = "res://senior.com.br/furb/basico/entities/cliente";

	/**
	 * Chave primaria
	 */
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false)
	private java.util.UUID id;
	
	/**
	 * Nome
	 */
	@Column(name = "nome")
	private String nome;
	
	/**
	 * Data de nascimento
	 */
	@Column(name = "datanascimento")
	private java.time.LocalDate data_nascimento;
	
	/**
	 * CPF do cliente
	 */
	@Column(name = "cpf")
	private String cpf;
	
	/**
	 * Nome login
	 */
	@Column(name = "nomelogin")
	private String nome_login;
	
	/**
	 * Senha login
	 */
	@Column(name = "senhalogin")
	private String senha_login;
	
	/**
	 * Tipo de usuário
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "tipo")
	private TipoUsuarioEntity tipo;
	
	public java.util.UUID getId() {
		return id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public java.time.LocalDate getData_nascimento() {
		return data_nascimento;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public String getNome_login() {
		return nome_login;
	}
	
	public String getSenha_login() {
		return senha_login;
	}
	
	public TipoUsuarioEntity getTipo() {
		return tipo;
	}
	
	public void setId(java.util.UUID id) {
		this.id = id;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setData_nascimento(java.time.LocalDate data_nascimento) {
		this.data_nascimento = data_nascimento;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public void setNome_login(String nome_login) {
		this.nome_login = nome_login;
	}
	
	public void setSenha_login(String senha_login) {
		this.senha_login = senha_login;
	}
	
	public void setTipo(TipoUsuarioEntity tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public int hashCode() {
	    int ret = 1;
	    if (id != null) {
	        ret = 31 * ret + id.hashCode();
	    }
	    return ret;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (!(obj instanceof ClienteEntity)) {
	        return false;
	    }
	    ClienteEntity other = (ClienteEntity) obj;
	    if ((id == null) != (other.id == null)) {
	        return false;
	    }
	    if ((id != null) && !id.equals(other.id)) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, new ArrayList<>());
		return sb.toString();
	}
	
	void toString(StringBuilder sb, List<Object> appended) {
		sb.append(getClass().getSimpleName()).append(" [");
		if (appended.contains(this)) {
			sb.append("<Previously appended object>").append(']');
			return;
		}
		appended.add(this);
		sb.append("id=").append(id == null ? "null" : id).append(", ");
		sb.append("nome=").append(nome == null ? "null" : nome).append(", ");
		sb.append("data_nascimento=").append(data_nascimento == null ? "null" : data_nascimento).append(", ");
		sb.append("cpf=").append(cpf == null ? "null" : cpf).append(", ");
		sb.append("nome_login=").append(nome_login == null ? "null" : nome_login).append(", ");
		sb.append("senha_login=").append(senha_login == null ? "null" : senha_login).append(", ");
		sb.append("tipo=<");
		if (tipo == null) {
			sb.append("null");
		} else {
			tipo.toString(sb, appended);
		}
		sb.append('>');
		sb.append(']');
	}
	
}
