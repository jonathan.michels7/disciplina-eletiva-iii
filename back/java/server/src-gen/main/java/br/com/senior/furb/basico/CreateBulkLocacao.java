/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name = "createBulkLocacao", kind = CommandKind.Create, requestPrimitive = "createBulkLocacao", responsePrimitive="")
public interface CreateBulkLocacao extends MessageHandler {
    public CreateBulkLocacaoOutput createBulkLocacao(CreateBulkLocacaoInput toCreate);
}
