/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name = "createBulkLocadora", kind = CommandKind.Create, requestPrimitive = "createBulkLocadora", responsePrimitive="")
public interface CreateBulkLocadora extends MessageHandler {
    public CreateBulkLocadoraOutput createBulkLocadora(CreateBulkLocadoraInput toCreate);
}
