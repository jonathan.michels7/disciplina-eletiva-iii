/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name = "createBulkProduto_alugado", kind = CommandKind.Create, requestPrimitive = "createBulkProduto_alugado", responsePrimitive="")
public interface CreateBulkProduto_alugado extends MessageHandler {
    public CreateBulkProduto_alugadoOutput createBulkProduto_alugado(CreateBulkProduto_alugadoInput toCreate);
}
