/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name = "createBulkTipoProduto", kind = CommandKind.Create, requestPrimitive = "createBulkTipoProduto", responsePrimitive="")
public interface CreateBulkTipoProduto extends MessageHandler {
    public CreateBulkTipoProdutoOutput createBulkTipoProduto(CreateBulkTipoProdutoInput toCreate);
}
