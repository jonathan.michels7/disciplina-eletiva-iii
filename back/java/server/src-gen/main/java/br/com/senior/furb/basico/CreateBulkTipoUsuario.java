/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name = "createBulkTipoUsuario", kind = CommandKind.Create, requestPrimitive = "createBulkTipoUsuario", responsePrimitive="")
public interface CreateBulkTipoUsuario extends MessageHandler {
    public CreateBulkTipoUsuarioOutput createBulkTipoUsuario(CreateBulkTipoUsuarioInput toCreate);
}
