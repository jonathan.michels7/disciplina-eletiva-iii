/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'create' request primitive for the Locacao entity.
 */
@CommandDescription(name="createLocacao", kind=CommandKind.Create, requestPrimitive="createLocacao", responsePrimitive="createLocacaoResponse")
public interface CreateLocacao extends MessageHandler {
    
    public Locacao createLocacao(Locacao toCreate);
    
}
