/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'create' request primitive for the Locadora entity.
 */
@CommandDescription(name="createLocadora", kind=CommandKind.Create, requestPrimitive="createLocadora", responsePrimitive="createLocadoraResponse")
public interface CreateLocadora extends MessageHandler {
    
    public Locadora createLocadora(Locadora toCreate);
    
}
