/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'createMerge' request primitive for the Locacao entity.
 */
@CommandDescription(name="createMergeLocacao", kind=CommandKind.CreateMerge, requestPrimitive="createMergeLocacao", responsePrimitive="createMergeLocacaoResponse")
public interface CreateMergeLocacao extends MessageHandler {
    
    public Locacao createMergeLocacao(Locacao toCreateMerge);
    
}
