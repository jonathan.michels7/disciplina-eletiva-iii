/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'createMerge' request primitive for the Locadora entity.
 */
@CommandDescription(name="createMergeLocadora", kind=CommandKind.CreateMerge, requestPrimitive="createMergeLocadora", responsePrimitive="createMergeLocadoraResponse")
public interface CreateMergeLocadora extends MessageHandler {
    
    public Locadora createMergeLocadora(Locadora toCreateMerge);
    
}
