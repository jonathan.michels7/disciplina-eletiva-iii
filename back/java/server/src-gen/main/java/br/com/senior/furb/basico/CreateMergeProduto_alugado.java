/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'createMerge' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="createMergeProduto_alugado", kind=CommandKind.CreateMerge, requestPrimitive="createMergeProduto_alugado", responsePrimitive="createMergeProduto_alugadoResponse")
public interface CreateMergeProduto_alugado extends MessageHandler {
    
    public Produto_alugado createMergeProduto_alugado(Produto_alugado toCreateMerge);
    
}
