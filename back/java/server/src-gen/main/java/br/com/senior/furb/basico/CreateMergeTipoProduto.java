/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'createMerge' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="createMergeTipoProduto", kind=CommandKind.CreateMerge, requestPrimitive="createMergeTipoProduto", responsePrimitive="createMergeTipoProdutoResponse")
public interface CreateMergeTipoProduto extends MessageHandler {
    
    public TipoProduto createMergeTipoProduto(TipoProduto toCreateMerge);
    
}
