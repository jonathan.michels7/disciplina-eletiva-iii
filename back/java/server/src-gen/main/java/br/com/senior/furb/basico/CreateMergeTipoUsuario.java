/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'createMerge' request primitive for the TipoUsuario entity.
 */
@CommandDescription(name="createMergeTipoUsuario", kind=CommandKind.CreateMerge, requestPrimitive="createMergeTipoUsuario", responsePrimitive="createMergeTipoUsuarioResponse")
public interface CreateMergeTipoUsuario extends MessageHandler {
    
    public TipoUsuario createMergeTipoUsuario(TipoUsuario toCreateMerge);
    
}
