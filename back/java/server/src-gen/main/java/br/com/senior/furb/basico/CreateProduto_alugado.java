/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'create' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="createProduto_alugado", kind=CommandKind.Create, requestPrimitive="createProduto_alugado", responsePrimitive="createProduto_alugadoResponse")
public interface CreateProduto_alugado extends MessageHandler {
    
    public Produto_alugado createProduto_alugado(Produto_alugado toCreate);
    
}
