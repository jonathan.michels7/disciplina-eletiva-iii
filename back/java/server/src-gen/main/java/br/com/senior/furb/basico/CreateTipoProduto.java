/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'create' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="createTipoProduto", kind=CommandKind.Create, requestPrimitive="createTipoProduto", responsePrimitive="createTipoProdutoResponse")
public interface CreateTipoProduto extends MessageHandler {
    
    public TipoProduto createTipoProduto(TipoProduto toCreate);
    
}
