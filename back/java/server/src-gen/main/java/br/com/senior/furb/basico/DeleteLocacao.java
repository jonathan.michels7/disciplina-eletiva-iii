/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'delete' request primitive for the Locacao entity.
 */
@CommandDescription(name="deleteLocacao", kind=CommandKind.Delete, requestPrimitive="deleteLocacao", responsePrimitive="deleteLocacaoResponse")
public interface DeleteLocacao extends MessageHandler {
    
    public void deleteLocacao(Locacao.Id id);
    
}
