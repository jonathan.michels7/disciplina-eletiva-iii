/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'delete' request primitive for the Locadora entity.
 */
@CommandDescription(name="deleteLocadora", kind=CommandKind.Delete, requestPrimitive="deleteLocadora", responsePrimitive="deleteLocadoraResponse")
public interface DeleteLocadora extends MessageHandler {
    
    public void deleteLocadora(Locadora.Id id);
    
}
