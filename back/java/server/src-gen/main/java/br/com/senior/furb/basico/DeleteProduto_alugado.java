/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'delete' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="deleteProduto_alugado", kind=CommandKind.Delete, requestPrimitive="deleteProduto_alugado", responsePrimitive="deleteProduto_alugadoResponse")
public interface DeleteProduto_alugado extends MessageHandler {
    
    public void deleteProduto_alugado(Produto_alugado.Id id);
    
}
