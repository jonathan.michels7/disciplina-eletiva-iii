/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'delete' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="deleteTipoProduto", kind=CommandKind.Delete, requestPrimitive="deleteTipoProduto", responsePrimitive="deleteTipoProdutoResponse")
public interface DeleteTipoProduto extends MessageHandler {
    
    public void deleteTipoProduto(TipoProduto.Id id);
    
}
