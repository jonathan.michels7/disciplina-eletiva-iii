/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'delete' request primitive for the TipoUsuario entity.
 */
@CommandDescription(name="deleteTipoUsuario", kind=CommandKind.Delete, requestPrimitive="deleteTipoUsuario", responsePrimitive="deleteTipoUsuarioResponse")
public interface DeleteTipoUsuario extends MessageHandler {
    
    public void deleteTipoUsuario(TipoUsuario.Id id);
    
}
