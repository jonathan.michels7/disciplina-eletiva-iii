/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="exportLocacao", kind=CommandKind.Query, requestPrimitive="exportLocacao", responsePrimitive="exportLocacaoResponse")
public interface ExportLocacao extends MessageHandler {
    
    public ExportLocacaoOutput exportLocacao(ExportLocacaoInput request);
    
}
