/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="exportLocadora", kind=CommandKind.Query, requestPrimitive="exportLocadora", responsePrimitive="exportLocadoraResponse")
public interface ExportLocadora extends MessageHandler {
    
    public ExportLocadoraOutput exportLocadora(ExportLocadoraInput request);
    
}
