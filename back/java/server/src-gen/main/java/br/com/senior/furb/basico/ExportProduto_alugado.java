/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="exportProduto_alugado", kind=CommandKind.Query, requestPrimitive="exportProduto_alugado", responsePrimitive="exportProduto_alugadoResponse")
public interface ExportProduto_alugado extends MessageHandler {
    
    public ExportProduto_alugadoOutput exportProduto_alugado(ExportProduto_alugadoInput request);
    
}
