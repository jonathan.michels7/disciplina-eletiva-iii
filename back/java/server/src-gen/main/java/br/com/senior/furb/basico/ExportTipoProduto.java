/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="exportTipoProduto", kind=CommandKind.Query, requestPrimitive="exportTipoProduto", responsePrimitive="exportTipoProdutoResponse")
public interface ExportTipoProduto extends MessageHandler {
    
    public ExportTipoProdutoOutput exportTipoProduto(ExportTipoProdutoInput request);
    
}
