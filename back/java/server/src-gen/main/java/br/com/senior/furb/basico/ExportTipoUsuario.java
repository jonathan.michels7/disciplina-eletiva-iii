/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="exportTipoUsuario", kind=CommandKind.Query, requestPrimitive="exportTipoUsuario", responsePrimitive="exportTipoUsuarioResponse")
public interface ExportTipoUsuario extends MessageHandler {
    
    public ExportTipoUsuarioOutput exportTipoUsuario(ExportTipoUsuarioInput request);
    
}
