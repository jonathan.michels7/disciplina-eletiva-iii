/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="importLocacao", kind=CommandKind.Action, requestPrimitive="importLocacao", responsePrimitive="importLocacaoResponse")
public interface ImportLocacao extends MessageHandler {
    
    public ImportLocacaoOutput importLocacao(ImportLocacaoInput request);
    
}
