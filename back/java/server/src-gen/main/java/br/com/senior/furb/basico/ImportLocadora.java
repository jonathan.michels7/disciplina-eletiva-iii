/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="importLocadora", kind=CommandKind.Action, requestPrimitive="importLocadora", responsePrimitive="importLocadoraResponse")
public interface ImportLocadora extends MessageHandler {
    
    public ImportLocadoraOutput importLocadora(ImportLocadoraInput request);
    
}
