/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="importProduto_alugado", kind=CommandKind.Action, requestPrimitive="importProduto_alugado", responsePrimitive="importProduto_alugadoResponse")
public interface ImportProduto_alugado extends MessageHandler {
    
    public ImportProduto_alugadoOutput importProduto_alugado(ImportProduto_alugadoInput request);
    
}
