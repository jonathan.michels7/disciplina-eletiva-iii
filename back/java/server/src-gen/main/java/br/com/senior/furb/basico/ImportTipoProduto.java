/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="importTipoProduto", kind=CommandKind.Action, requestPrimitive="importTipoProduto", responsePrimitive="importTipoProdutoResponse")
public interface ImportTipoProduto extends MessageHandler {
    
    public ImportTipoProdutoOutput importTipoProduto(ImportTipoProdutoInput request);
    
}
