/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

@CommandDescription(name="importTipoUsuario", kind=CommandKind.Action, requestPrimitive="importTipoUsuario", responsePrimitive="importTipoUsuarioResponse")
public interface ImportTipoUsuario extends MessageHandler {
    
    public ImportTipoUsuarioOutput importTipoUsuario(ImportTipoUsuarioInput request);
    
}
