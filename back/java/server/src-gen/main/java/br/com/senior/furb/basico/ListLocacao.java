/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'list' request primitive for the Locacao entity.
 */
@CommandDescription(name="listLocacao", kind=CommandKind.List, requestPrimitive="listLocacao", responsePrimitive="listLocacaoResponse")
public interface ListLocacao extends MessageHandler {
    
    public Locacao.PagedResults listLocacao(Locacao.PageRequest pageRequest);
    
}
