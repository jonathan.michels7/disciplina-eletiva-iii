/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'list' request primitive for the Locadora entity.
 */
@CommandDescription(name="listLocadora", kind=CommandKind.List, requestPrimitive="listLocadora", responsePrimitive="listLocadoraResponse")
public interface ListLocadora extends MessageHandler {
    
    public Locadora.PagedResults listLocadora(Locadora.PageRequest pageRequest);
    
}
