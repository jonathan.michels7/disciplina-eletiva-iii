/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'list' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="listProduto_alugado", kind=CommandKind.List, requestPrimitive="listProduto_alugado", responsePrimitive="listProduto_alugadoResponse")
public interface ListProduto_alugado extends MessageHandler {
    
    public Produto_alugado.PagedResults listProduto_alugado(Produto_alugado.PageRequest pageRequest);
    
}
