/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'list' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="listTipoProduto", kind=CommandKind.List, requestPrimitive="listTipoProduto", responsePrimitive="listTipoProdutoResponse")
public interface ListTipoProduto extends MessageHandler {
    
    public TipoProduto.PagedResults listTipoProduto(TipoProduto.PageRequest pageRequest);
    
}
