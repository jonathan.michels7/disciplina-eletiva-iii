/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.List;
import br.com.senior.furb.basico.Locacao.Id;
import br.com.senior.springbatchintegration.importer.CrudService;
import org.springframework.data.domain.Page;

public interface LocacaoCrudService extends CrudService<LocacaoEntity> {
	
	public LocacaoEntity createLocacao(LocacaoEntity toCreate);
	
	@Deprecated
	public LocacaoEntity createMergeLocacao(LocacaoEntity toCreateMerge);
	
	public LocacaoEntity updateLocacao(LocacaoEntity toUpdate);
	
	@Deprecated
	public LocacaoEntity updateMergeLocacao(LocacaoEntity toUpdateMerge);
	
	public void deleteLocacao(Id id);
	
	public LocacaoEntity retrieveLocacao(Id id);
	
	@Deprecated
	public List<LocacaoEntity> listLocacao(int skip, int top);
	
	public Page<LocacaoEntity> listLocacaoPageable(int skip, int top);
	
	public Page<LocacaoEntity> listLocacaoPageable(int skip, int top, String orderBy);
	
	public Page<LocacaoEntity> listLocacaoPageable(int skip, int top, String orderBy, String filter);
	
	public void createBulkLocacao(List<LocacaoEntity> entities);

	public LocacaoBaseRepository getRepository();
	
}
