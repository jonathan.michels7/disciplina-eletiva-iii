/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import br.com.senior.custom.ConversionContext;
import br.com.senior.custom.ConvertedObjectCondition;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import br.com.senior.custom.EntityDTOConverter;

@Component("furb.basico.LocacaoDTOConverter")
@Lazy
public class LocacaoDTOConverter extends EntityDTOConverter {

	@Autowired
	private Produto_alugadoDTOConverter produto_alugadoConverter;
	@Autowired
	private ClienteDTOConverter clienteConverter;

	@Override
	public void setupMapperToEntity(ModelMapper mapper, ConversionContext conversionContext) {
		Converter <List<Produto_alugado>, List<Produto_alugadoEntity>> produtosConverter = new ContextualizedConverter<List<Produto_alugado>, List<Produto_alugadoEntity>>() {
			@Override
			public List<Produto_alugadoEntity> convert(List<Produto_alugado> source) {
				if (source == null) {
					return Collections.emptyList();
				}
				return source.stream().map(d -> toEntity(d, Produto_alugadoEntity.class, conversionContext)).collect(Collectors.toList());
			}
		};
		Converter<Cliente, ClienteEntity> clienteConverter = new ContextualizedConverter<Cliente, ClienteEntity>() {
			@Override
			public ClienteEntity convert(Cliente source) {
				return toEntity(source, ClienteEntity.class, conversionContext);
			}
		};

		PropertyMap<Locacao, LocacaoEntity> locacaoMap = new PropertyMap<Locacao, LocacaoEntity>() {
			@Override
			protected void configure() {
				using(produtosConverter).map(source.produtos).setProdutos(null);
				using(clienteConverter).map(source.cliente).setCliente(null);
			}
		};
		mapper.addMappings(locacaoMap);
		if (mapper.getTypeMap(Produto_alugado.class, Produto_alugadoEntity.class) == null) {
			this.produto_alugadoConverter.setupMapperToEntity(mapper, conversionContext);
		}
		if (mapper.getTypeMap(Cliente.class, ClienteEntity.class) == null) {
			this.clienteConverter.setupMapperToEntity(mapper, conversionContext);
		}
		mapper.getTypeMap(Locacao.class, LocacaoEntity.class).setPropertyCondition(new ConvertedObjectCondition(conversionContext));
	}

	@Override
	public void setupMapperToDTO(ModelMapper mapper, ConversionContext conversionContext) {
		
		Converter <List<Produto_alugadoEntity>, List<Produto_alugado>> produtosConverter = new ContextualizedConverter<List<Produto_alugadoEntity>, List<Produto_alugado>>() {
			@Override
			public List<Produto_alugado> convert(List<Produto_alugadoEntity> source) {
				if (conversionContext.isIncludeTranslations()) {
					return source.stream().map(e -> toDTOWithTranslations(e, Produto_alugado.class, conversionContext.getRelationshipFields("produtos"), conversionContext)).collect(Collectors.toList());
				}
				return source.stream().map(e -> toDTO(e, Produto_alugado.class, conversionContext.getRelationshipFields("produtos"), conversionContext)).collect(Collectors.toList());
			}
		};
		
		
		Converter<ClienteEntity, Cliente> clienteConverter = new ContextualizedConverter<ClienteEntity, Cliente>() {
			@Override
			public Cliente convert(ClienteEntity source) {
				if (conversionContext.isIncludeTranslations()) {
					return toDTOWithTranslations(source, Cliente.class, conversionContext.getRelationshipFields("cliente"), conversionContext);
				}
				return toDTO(source, Cliente.class, conversionContext.getRelationshipFields("cliente"), conversionContext);
			}
		};
		
		//eager relationships
		PropertyMap<LocacaoEntity, Locacao> locacaoMap = new PropertyMap<LocacaoEntity, Locacao>() {
			@Override
			public void configure() {
				using(produtosConverter).map(source.getProdutos(), destination.produtos);
				using(clienteConverter).map(source.getCliente(), destination.cliente);
			}
		};
		//lazy relationships
		
		mapper.addMappings(locacaoMap);
		
		if (mapper.getTypeMap(Produto_alugadoEntity.class, Produto_alugado.class) == null) {
		    this.produto_alugadoConverter.setupMapperToDTO(mapper, conversionContext);
		}
		if (mapper.getTypeMap(ClienteEntity.class, Cliente.class) == null) {
		    this.clienteConverter.setupMapperToDTO(mapper, conversionContext);
		}
		mapper.getTypeMap(LocacaoEntity.class, Locacao.class).setPropertyCondition(new ConvertedObjectCondition(conversionContext));
	}
}
