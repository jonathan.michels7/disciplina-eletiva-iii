/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;

@Entity
@Table(name="locacao")
public class LocacaoEntity {
	
	public static final String SECURITY_RESOURCE = "res://senior.com.br/furb/basico/entities/locacao";

	/**
	 * Chave primária
	 */
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false)
	private java.util.UUID id;
	
	/**
	 * Produtos
	 */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "locacao_produtos",
	          joinColumns = @JoinColumn(name = "locacao_id", referencedColumnName = "id"),
	          inverseJoinColumns = @JoinColumn(name = "produtos_id", referencedColumnName = "id"))
	private java.util.List<Produto_alugadoEntity> produtos;
	
	/**
	 * Data do pedido
	 */
	@Column(name = "datapedido")
	private java.time.LocalDate data_pedido;
	
	/**
	 * Observação
	 */
	@Column(name = "obsevacao")
	private String obsevacao;
	
	/**
	 * Data de entrega
	 */
	@Column(name = "dataentrega")
	private java.time.LocalDate data_entrega;
	
	/**
	 * Valor pago
	 */
	@Column(name = "valorpago")
	private Double valor_pago;
	
	/**
	 * cliente
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cliente")
	private ClienteEntity cliente;
	
	public java.util.UUID getId() {
		return id;
	}
	
	public java.util.List<Produto_alugadoEntity> getProdutos() {
		return produtos;
	}
	
	public java.time.LocalDate getData_pedido() {
		return data_pedido;
	}
	
	public String getObsevacao() {
		return obsevacao;
	}
	
	public java.time.LocalDate getData_entrega() {
		return data_entrega;
	}
	
	public Double getValor_pago() {
		return valor_pago;
	}
	
	public ClienteEntity getCliente() {
		return cliente;
	}
	
	public void setId(java.util.UUID id) {
		this.id = id;
	}
	
	public void setProdutos(java.util.List<Produto_alugadoEntity> produtos) {
		this.produtos = produtos;
	}
	
	public void setData_pedido(java.time.LocalDate data_pedido) {
		this.data_pedido = data_pedido;
	}
	
	public void setObsevacao(String obsevacao) {
		this.obsevacao = obsevacao;
	}
	
	public void setData_entrega(java.time.LocalDate data_entrega) {
		this.data_entrega = data_entrega;
	}
	
	public void setValor_pago(Double valor_pago) {
		this.valor_pago = valor_pago;
	}
	
	public void setCliente(ClienteEntity cliente) {
		this.cliente = cliente;
	}
	
	@Override
	public int hashCode() {
	    int ret = 1;
	    if (id != null) {
	        ret = 31 * ret + id.hashCode();
	    }
	    return ret;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (!(obj instanceof LocacaoEntity)) {
	        return false;
	    }
	    LocacaoEntity other = (LocacaoEntity) obj;
	    if ((id == null) != (other.id == null)) {
	        return false;
	    }
	    if ((id != null) && !id.equals(other.id)) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, new ArrayList<>());
		return sb.toString();
	}
	
	void toString(StringBuilder sb, List<Object> appended) {
		sb.append(getClass().getSimpleName()).append(" [");
		if (appended.contains(this)) {
			sb.append("<Previously appended object>").append(']');
			return;
		}
		appended.add(this);
		sb.append("id=").append(id == null ? "null" : id).append(", ");
		sb.append("produtos=<");
		if (produtos == null) {
			sb.append("null");
		} else {
			sb.append('[');
			Iterator<Produto_alugadoEntity> iterator = produtos.iterator();
			while (iterator.hasNext()) {
				iterator.next().toString(sb, appended);
				if (iterator.hasNext()) {
					sb.append(", ");
				}
			}
			sb.append(']');
		}
		sb.append('>').append(", ");
		sb.append("data_pedido=").append(data_pedido == null ? "null" : data_pedido).append(", ");
		sb.append("obsevacao=").append(obsevacao == null ? "null" : obsevacao).append(", ");
		sb.append("data_entrega=").append(data_entrega == null ? "null" : data_entrega).append(", ");
		sb.append("valor_pago=").append(valor_pago == null ? "null" : valor_pago).append(", ");
		sb.append("cliente=<");
		if (cliente == null) {
			sb.append("null");
		} else {
			cliente.toString(sb, appended);
		}
		sb.append('>');
		sb.append(']');
	}
	
}
