/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.List;
import br.com.senior.furb.basico.Locadora.Id;
import br.com.senior.springbatchintegration.importer.CrudService;
import org.springframework.data.domain.Page;

public interface LocadoraCrudService extends CrudService<LocadoraEntity> {
	
	public LocadoraEntity createLocadora(LocadoraEntity toCreate);
	
	@Deprecated
	public LocadoraEntity createMergeLocadora(LocadoraEntity toCreateMerge);
	
	public LocadoraEntity updateLocadora(LocadoraEntity toUpdate);
	
	@Deprecated
	public LocadoraEntity updateMergeLocadora(LocadoraEntity toUpdateMerge);
	
	public void deleteLocadora(Id id);
	
	public LocadoraEntity retrieveLocadora(Id id);
	
	@Deprecated
	public List<LocadoraEntity> listLocadora(int skip, int top);
	
	public Page<LocadoraEntity> listLocadoraPageable(int skip, int top);
	
	public Page<LocadoraEntity> listLocadoraPageable(int skip, int top, String orderBy);
	
	public Page<LocadoraEntity> listLocadoraPageable(int skip, int top, String orderBy, String filter);
	
	public void createBulkLocadora(List<LocadoraEntity> entities);

	public LocadoraBaseRepository getRepository();
	
}
