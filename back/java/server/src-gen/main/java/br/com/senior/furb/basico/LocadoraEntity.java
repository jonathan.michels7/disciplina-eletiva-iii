/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="locadora")
public class LocadoraEntity {
	
	public static final String SECURITY_RESOURCE = "res://senior.com.br/furb/basico/entities/locadora";

	/**
	 * Chave primaria
	 */
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false)
	private java.util.UUID id;
	
	/**
	 * data de cadastro
	 */
	@Column(name = "datacadastro")
	private java.time.LocalDate data_cadastro;
	
	/**
	 * Nome da locadora
	 */
	@Column(name = "nome")
	private String nome;
	
	public java.util.UUID getId() {
		return id;
	}
	
	public java.time.LocalDate getData_cadastro() {
		return data_cadastro;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setId(java.util.UUID id) {
		this.id = id;
	}
	
	public void setData_cadastro(java.time.LocalDate data_cadastro) {
		this.data_cadastro = data_cadastro;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public int hashCode() {
	    int ret = 1;
	    if (id != null) {
	        ret = 31 * ret + id.hashCode();
	    }
	    return ret;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (!(obj instanceof LocadoraEntity)) {
	        return false;
	    }
	    LocadoraEntity other = (LocadoraEntity) obj;
	    if ((id == null) != (other.id == null)) {
	        return false;
	    }
	    if ((id != null) && !id.equals(other.id)) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, new ArrayList<>());
		return sb.toString();
	}
	
	void toString(StringBuilder sb, List<Object> appended) {
		sb.append(getClass().getSimpleName()).append(" [");
		if (appended.contains(this)) {
			sb.append("<Previously appended object>").append(']');
			return;
		}
		appended.add(this);
		sb.append("id=").append(id == null ? "null" : id).append(", ");
		sb.append("data_cadastro=").append(data_cadastro == null ? "null" : data_cadastro).append(", ");
		sb.append("nome=").append(nome == null ? "null" : nome);
		sb.append(']');
	}
	
}
