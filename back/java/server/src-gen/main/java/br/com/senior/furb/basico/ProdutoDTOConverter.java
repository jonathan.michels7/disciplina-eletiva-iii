/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.stream.Collectors;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;

import br.com.senior.custom.ConversionContext;
import br.com.senior.custom.ConvertedObjectCondition;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import br.com.senior.custom.EntityDTOConverter;

@Component("furb.basico.ProdutoDTOConverter")
@Lazy
public class ProdutoDTOConverter extends EntityDTOConverter {

	@Autowired
	private TipoProdutoDTOConverter tipoProdutoConverter;

	@Override
	public void setupMapperToEntity(ModelMapper mapper, ConversionContext conversionContext) {
		Converter<TipoProduto, TipoProdutoEntity> tipoConverter = new ContextualizedConverter<TipoProduto, TipoProdutoEntity>() {
			@Override
			public TipoProdutoEntity convert(TipoProduto source) {
				return toEntity(source, TipoProdutoEntity.class, conversionContext);
			}
		};

		PropertyMap<Produto, ProdutoEntity> produtoMap = new PropertyMap<Produto, ProdutoEntity>() {
			@Override
			protected void configure() {
				using(tipoConverter).map(source.tipo).setTipo(null);
			}
		};
		mapper.addMappings(produtoMap);
		if (mapper.getTypeMap(TipoProduto.class, TipoProdutoEntity.class) == null) {
			this.tipoProdutoConverter.setupMapperToEntity(mapper, conversionContext);
		}
		mapper.getTypeMap(Produto.class, ProdutoEntity.class).setPropertyCondition(new ConvertedObjectCondition(conversionContext));
	}

	@Override
	public void setupMapperToDTO(ModelMapper mapper, ConversionContext conversionContext) {
		
		Converter<TipoProdutoEntity, TipoProduto> tipoConverter = new ContextualizedConverter<TipoProdutoEntity, TipoProduto>() {
			@Override
			public TipoProduto convert(TipoProdutoEntity source) {
				if (conversionContext.isIncludeTranslations()) {
					return toDTOWithTranslations(source, TipoProduto.class, conversionContext.getRelationshipFields("tipo"), conversionContext);
				}
				return toDTO(source, TipoProduto.class, conversionContext.getRelationshipFields("tipo"), conversionContext);
			}
		};
		
		//eager relationships
		PropertyMap<ProdutoEntity, Produto> produtoMap = new PropertyMap<ProdutoEntity, Produto>() {
			@Override
			public void configure() {
				using(tipoConverter).map(source.getTipo(), destination.tipo);
			}
		};
		//lazy relationships
		
		mapper.addMappings(produtoMap);
		
		if (mapper.getTypeMap(TipoProdutoEntity.class, TipoProduto.class) == null) {
		    this.tipoProdutoConverter.setupMapperToDTO(mapper, conversionContext);
		}
		mapper.getTypeMap(ProdutoEntity.class, Produto.class).setPropertyCondition(new ConvertedObjectCondition(conversionContext));
	}
}
