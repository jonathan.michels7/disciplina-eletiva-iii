/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.List;
import br.com.senior.furb.basico.Produto_alugado.Id;
import br.com.senior.messaging.customspringdata.EntityInfo;
import br.com.senior.springbatchintegration.importer.CrudService;
import org.springframework.data.domain.Page;

public interface Produto_alugadoCrudService extends CrudService<Produto_alugadoEntity> {
	
	public Produto_alugadoEntity createProduto_alugado(Produto_alugadoEntity toCreate);
	
	@Deprecated
	public Produto_alugadoEntity createMergeProduto_alugado(Produto_alugadoEntity toCreateMerge);
	
	public Produto_alugadoEntity updateProduto_alugado(Produto_alugadoEntity toUpdate);
	
	@Deprecated
	public Produto_alugadoEntity updateMergeProduto_alugado(Produto_alugadoEntity toUpdateMerge);
	
	public void deleteProduto_alugado(Id id);
	
	public Produto_alugadoEntity retrieveProduto_alugado(Id id);
	
	@Deprecated
	public List<Produto_alugadoEntity> listProduto_alugado(int skip, int top);
	
	public Page<Produto_alugadoEntity> listProduto_alugadoPageable(int skip, int top);
	
	public Page<Produto_alugadoEntity> listProduto_alugadoPageable(int skip, int top, String orderBy);
	
	public Page<Produto_alugadoEntity> listProduto_alugadoPageable(int skip, int top, String orderBy, String filter);
	
	public Page<Produto_alugadoEntity> listProduto_alugadoPageable(int skip, int top, String orderBy, String filter, EntityInfo parentEntity);
	
	public void createBulkProduto_alugado(List<Produto_alugadoEntity> entities);

	public Produto_alugadoBaseRepository getRepository();
	
}
