/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;

@Entity
@Table(name="produtoalugado")
public class Produto_alugadoEntity {
	
	public static final String SECURITY_RESOURCE = "res://senior.com.br/furb/basico/entities/produto_alugado";

	/**
	 * Chave primária
	 */
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false)
	private java.util.UUID id;
	
	/**
	 * Produtos
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "produto")
	private ProdutoEntity produto;
	
	/**
	 * Data de vencimento
	 */
	@Column(name = "datavencimento")
	private java.time.LocalDate data_vencimento;
	
	/**
	 * Quantidade alugado
	 */
	@Column(name = "qtdalugado")
	private Long qtd_alugado;
	
	public java.util.UUID getId() {
		return id;
	}
	
	public ProdutoEntity getProduto() {
		return produto;
	}
	
	public java.time.LocalDate getData_vencimento() {
		return data_vencimento;
	}
	
	public Long getQtd_alugado() {
		return qtd_alugado;
	}
	
	public void setId(java.util.UUID id) {
		this.id = id;
	}
	
	public void setProduto(ProdutoEntity produto) {
		this.produto = produto;
	}
	
	public void setData_vencimento(java.time.LocalDate data_vencimento) {
		this.data_vencimento = data_vencimento;
	}
	
	public void setQtd_alugado(Long qtd_alugado) {
		this.qtd_alugado = qtd_alugado;
	}
	
	@Override
	public int hashCode() {
	    int ret = 1;
	    if (id != null) {
	        ret = 31 * ret + id.hashCode();
	    }
	    return ret;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (!(obj instanceof Produto_alugadoEntity)) {
	        return false;
	    }
	    Produto_alugadoEntity other = (Produto_alugadoEntity) obj;
	    if ((id == null) != (other.id == null)) {
	        return false;
	    }
	    if ((id != null) && !id.equals(other.id)) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, new ArrayList<>());
		return sb.toString();
	}
	
	void toString(StringBuilder sb, List<Object> appended) {
		sb.append(getClass().getSimpleName()).append(" [");
		if (appended.contains(this)) {
			sb.append("<Previously appended object>").append(']');
			return;
		}
		appended.add(this);
		sb.append("id=").append(id == null ? "null" : id).append(", ");
		sb.append("produto=<");
		if (produto == null) {
			sb.append("null");
		} else {
			produto.toString(sb, appended);
		}
		sb.append('>').append(", ");
		sb.append("data_vencimento=").append(data_vencimento == null ? "null" : data_vencimento).append(", ");
		sb.append("qtd_alugado=").append(qtd_alugado == null ? "null" : qtd_alugado);
		sb.append(']');
	}
	
}
