/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'retrieve' request primitive for the Locacao entity.
 */
@CommandDescription(name="retrieveLocacao", kind=CommandKind.Retrieve, requestPrimitive="retrieveLocacao", responsePrimitive="retrieveLocacaoResponse")
public interface RetrieveLocacao extends MessageHandler {
    
    public Locacao retrieveLocacao(Locacao.Id id);
    
}
