/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'retrieve' request primitive for the Locadora entity.
 */
@CommandDescription(name="retrieveLocadora", kind=CommandKind.Retrieve, requestPrimitive="retrieveLocadora", responsePrimitive="retrieveLocadoraResponse")
public interface RetrieveLocadora extends MessageHandler {
    
    public Locadora retrieveLocadora(Locadora.Id id);
    
}
