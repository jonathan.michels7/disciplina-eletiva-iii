/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'retrieve' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="retrieveProduto_alugado", kind=CommandKind.Retrieve, requestPrimitive="retrieveProduto_alugado", responsePrimitive="retrieveProduto_alugadoResponse")
public interface RetrieveProduto_alugado extends MessageHandler {
    
    public Produto_alugado retrieveProduto_alugado(Produto_alugado.Id id);
    
}
