/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'retrieve' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="retrieveTipoProduto", kind=CommandKind.Retrieve, requestPrimitive="retrieveTipoProduto", responsePrimitive="retrieveTipoProdutoResponse")
public interface RetrieveTipoProduto extends MessageHandler {
    
    public TipoProduto retrieveTipoProduto(TipoProduto.Id id);
    
}
