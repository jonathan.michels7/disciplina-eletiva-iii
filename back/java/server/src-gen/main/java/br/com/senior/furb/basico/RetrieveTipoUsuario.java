/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'retrieve' request primitive for the TipoUsuario entity.
 */
@CommandDescription(name="retrieveTipoUsuario", kind=CommandKind.Retrieve, requestPrimitive="retrieveTipoUsuario", responsePrimitive="retrieveTipoUsuarioResponse")
public interface RetrieveTipoUsuario extends MessageHandler {
    
    public TipoUsuario retrieveTipoUsuario(TipoUsuario.Id id);
    
}
