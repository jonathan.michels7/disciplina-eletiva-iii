/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.List;
import br.com.senior.furb.basico.TipoProduto.Id;
import br.com.senior.springbatchintegration.importer.CrudService;
import org.springframework.data.domain.Page;

public interface TipoProdutoCrudService extends CrudService<TipoProdutoEntity> {
	
	public TipoProdutoEntity createTipoProduto(TipoProdutoEntity toCreate);
	
	@Deprecated
	public TipoProdutoEntity createMergeTipoProduto(TipoProdutoEntity toCreateMerge);
	
	public TipoProdutoEntity updateTipoProduto(TipoProdutoEntity toUpdate);
	
	@Deprecated
	public TipoProdutoEntity updateMergeTipoProduto(TipoProdutoEntity toUpdateMerge);
	
	public void deleteTipoProduto(Id id);
	
	public TipoProdutoEntity retrieveTipoProduto(Id id);
	
	@Deprecated
	public List<TipoProdutoEntity> listTipoProduto(int skip, int top);
	
	public Page<TipoProdutoEntity> listTipoProdutoPageable(int skip, int top);
	
	public Page<TipoProdutoEntity> listTipoProdutoPageable(int skip, int top, String orderBy);
	
	public Page<TipoProdutoEntity> listTipoProdutoPageable(int skip, int top, String orderBy, String filter);
	
	public void createBulkTipoProduto(List<TipoProdutoEntity> entities);

	public TipoProdutoBaseRepository getRepository();
	
}
