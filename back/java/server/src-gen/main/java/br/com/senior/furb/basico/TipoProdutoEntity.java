/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="tipo_produto")
public class TipoProdutoEntity {
	
	public static final String SECURITY_RESOURCE = "res://senior.com.br/furb/basico/entities/tipoProduto";

	/**
	 * Chave primária
	 */
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", updatable = false)
	private java.util.UUID id;
	
	/**
	 * descrição
	 */
	@Column(name = "descricao")
	private String descricao;
	
	/**
	 * valor que é cobfado
	 */
	@Column(name = "valor")
	private Double valor;
	
	/**
	 * dias que pode ficar alugado
	 */
	@Column(name = "diasalugado")
	private Long dias_alugado;
	
	public java.util.UUID getId() {
		return id;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public Double getValor() {
		return valor;
	}
	
	public Long getDias_alugado() {
		return dias_alugado;
	}
	
	public void setId(java.util.UUID id) {
		this.id = id;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setValor(Double valor) {
		this.valor = valor;
	}
	
	public void setDias_alugado(Long dias_alugado) {
		this.dias_alugado = dias_alugado;
	}
	
	@Override
	public int hashCode() {
	    int ret = 1;
	    if (id != null) {
	        ret = 31 * ret + id.hashCode();
	    }
	    return ret;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {
	        return true;
	    }
	    if (!(obj instanceof TipoProdutoEntity)) {
	        return false;
	    }
	    TipoProdutoEntity other = (TipoProdutoEntity) obj;
	    if ((id == null) != (other.id == null)) {
	        return false;
	    }
	    if ((id != null) && !id.equals(other.id)) {
	        return false;
	    }
	    return true;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		toString(sb, new ArrayList<>());
		return sb.toString();
	}
	
	void toString(StringBuilder sb, List<Object> appended) {
		sb.append(getClass().getSimpleName()).append(" [");
		if (appended.contains(this)) {
			sb.append("<Previously appended object>").append(']');
			return;
		}
		appended.add(this);
		sb.append("id=").append(id == null ? "null" : id).append(", ");
		sb.append("descricao=").append(descricao == null ? "null" : descricao).append(", ");
		sb.append("valor=").append(valor == null ? "null" : valor).append(", ");
		sb.append("dias_alugado=").append(dias_alugado == null ? "null" : dias_alugado);
		sb.append(']');
	}
	
}
