/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import java.util.List;
import br.com.senior.furb.basico.TipoUsuario.Id;
import br.com.senior.springbatchintegration.importer.CrudService;
import org.springframework.data.domain.Page;

public interface TipoUsuarioCrudService extends CrudService<TipoUsuarioEntity> {
	
	public TipoUsuarioEntity createTipoUsuario(TipoUsuarioEntity toCreate);
	
	@Deprecated
	public TipoUsuarioEntity createMergeTipoUsuario(TipoUsuarioEntity toCreateMerge);
	
	public TipoUsuarioEntity updateTipoUsuario(TipoUsuarioEntity toUpdate);
	
	@Deprecated
	public TipoUsuarioEntity updateMergeTipoUsuario(TipoUsuarioEntity toUpdateMerge);
	
	public void deleteTipoUsuario(Id id);
	
	public TipoUsuarioEntity retrieveTipoUsuario(Id id);
	
	@Deprecated
	public List<TipoUsuarioEntity> listTipoUsuario(int skip, int top);
	
	public Page<TipoUsuarioEntity> listTipoUsuarioPageable(int skip, int top);
	
	public Page<TipoUsuarioEntity> listTipoUsuarioPageable(int skip, int top, String orderBy);
	
	public Page<TipoUsuarioEntity> listTipoUsuarioPageable(int skip, int top, String orderBy, String filter);
	
	public void createBulkTipoUsuario(List<TipoUsuarioEntity> entities);

	public TipoUsuarioBaseRepository getRepository();
	
}
