/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'update' request primitive for the Locadora entity.
 */
@CommandDescription(name="updateLocadora", kind=CommandKind.Update, requestPrimitive="updateLocadora", responsePrimitive="updateLocadoraResponse")
public interface UpdateLocadora extends MessageHandler {
    
    public Locadora updateLocadora(Locadora toUpdate);
    
}
