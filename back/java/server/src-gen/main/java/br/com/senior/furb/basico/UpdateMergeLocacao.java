/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'updateMerge' request primitive for the Locacao entity.
 */
@CommandDescription(name="updateMergeLocacao", kind=CommandKind.UpdateMerge, requestPrimitive="updateMergeLocacao", responsePrimitive="updateMergeLocacaoResponse")
public interface UpdateMergeLocacao extends MessageHandler {
    
    public Locacao updateMergeLocacao(Locacao toUpdateMerge);
    
}
