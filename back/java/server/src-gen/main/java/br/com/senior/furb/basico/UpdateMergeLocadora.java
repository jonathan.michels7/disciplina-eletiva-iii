/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'updateMerge' request primitive for the Locadora entity.
 */
@CommandDescription(name="updateMergeLocadora", kind=CommandKind.UpdateMerge, requestPrimitive="updateMergeLocadora", responsePrimitive="updateMergeLocadoraResponse")
public interface UpdateMergeLocadora extends MessageHandler {
    
    public Locadora updateMergeLocadora(Locadora toUpdateMerge);
    
}
