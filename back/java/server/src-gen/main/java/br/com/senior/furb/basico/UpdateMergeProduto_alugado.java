/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'updateMerge' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="updateMergeProduto_alugado", kind=CommandKind.UpdateMerge, requestPrimitive="updateMergeProduto_alugado", responsePrimitive="updateMergeProduto_alugadoResponse")
public interface UpdateMergeProduto_alugado extends MessageHandler {
    
    public Produto_alugado updateMergeProduto_alugado(Produto_alugado toUpdateMerge);
    
}
