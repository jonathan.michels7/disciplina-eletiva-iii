/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'updateMerge' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="updateMergeTipoProduto", kind=CommandKind.UpdateMerge, requestPrimitive="updateMergeTipoProduto", responsePrimitive="updateMergeTipoProdutoResponse")
public interface UpdateMergeTipoProduto extends MessageHandler {
    
    public TipoProduto updateMergeTipoProduto(TipoProduto toUpdateMerge);
    
}
