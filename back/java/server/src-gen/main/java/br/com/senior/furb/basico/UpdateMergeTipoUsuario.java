/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'updateMerge' request primitive for the TipoUsuario entity.
 */
@CommandDescription(name="updateMergeTipoUsuario", kind=CommandKind.UpdateMerge, requestPrimitive="updateMergeTipoUsuario", responsePrimitive="updateMergeTipoUsuarioResponse")
public interface UpdateMergeTipoUsuario extends MessageHandler {
    
    public TipoUsuario updateMergeTipoUsuario(TipoUsuario toUpdateMerge);
    
}
