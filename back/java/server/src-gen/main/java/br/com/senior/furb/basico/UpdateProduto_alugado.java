/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'update' request primitive for the Produto_alugado entity.
 */
@CommandDescription(name="updateProduto_alugado", kind=CommandKind.Update, requestPrimitive="updateProduto_alugado", responsePrimitive="updateProduto_alugadoResponse")
public interface UpdateProduto_alugado extends MessageHandler {
    
    public Produto_alugado updateProduto_alugado(Produto_alugado toUpdate);
    
}
