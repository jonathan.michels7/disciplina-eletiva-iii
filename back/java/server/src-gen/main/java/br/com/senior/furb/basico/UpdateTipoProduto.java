/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'update' request primitive for the TipoProduto entity.
 */
@CommandDescription(name="updateTipoProduto", kind=CommandKind.Update, requestPrimitive="updateTipoProduto", responsePrimitive="updateTipoProdutoResponse")
public interface UpdateTipoProduto extends MessageHandler {
    
    public TipoProduto updateTipoProduto(TipoProduto toUpdate);
    
}
