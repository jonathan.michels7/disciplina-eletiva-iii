/**
 * This is a generated file. DO NOT EDIT ANY CODE HERE, YOUR CHANGES WILL BE LOST.
 */
package br.com.senior.furb.basico;

import br.com.senior.messaging.model.*;

/**
 * The 'update' request primitive for the TipoUsuario entity.
 */
@CommandDescription(name="updateTipoUsuario", kind=CommandKind.Update, requestPrimitive="updateTipoUsuario", responsePrimitive="updateTipoUsuarioResponse")
public interface UpdateTipoUsuario extends MessageHandler {
    
    public TipoUsuario updateTipoUsuario(TipoUsuario toUpdate);
    
}
