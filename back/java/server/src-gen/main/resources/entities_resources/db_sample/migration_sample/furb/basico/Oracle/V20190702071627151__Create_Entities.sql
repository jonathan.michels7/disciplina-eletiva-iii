/* Database: Oracle. Generation date: 2019-07-02 07:16:27:151 */
/* Entity Locadora */
create table locadora (
	id RAW(16) NOT NULL,
	datacadastro DATE NOT NULL /* data_cadastro */,
	nome VARCHAR(255) NOT NULL
);



/* Entity TipoUsuario */
create table tipo_usuario (
	id RAW(16) NOT NULL,
	nome VARCHAR(255) NOT NULL
);



/* Entity Cliente */
create table cliente (
	id RAW(16) NOT NULL,
	nome VARCHAR(255) NOT NULL,
	datanascimento DATE /* data_nascimento */,
	cpf VARCHAR(255),
	nomelogin VARCHAR(255) NOT NULL /* nome_login */,
	senhalogin VARCHAR(255) NOT NULL /* senha_login */,
	tipo RAW(16) NOT NULL
);



/* Entity TipoProduto */
create table tipo_produto (
	id RAW(16) NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	valor NUMBER(19,4) NOT NULL,
	diasalugado NUMBER(19) NOT NULL /* dias_alugado */
);



/* Entity Produto */
create table produto (
	id RAW(16) NOT NULL,
	nome VARCHAR(255) NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	quantidade NUMBER(19) NOT NULL,
	tipo RAW(16) NOT NULL,
	id_imagem VARCHAR(255) /* idImagem */
);



/* Entity Produto_alugado */
create table produtoalugado (
	id RAW(16) NOT NULL,
	produto RAW(16) NOT NULL,
	datavencimento DATE NOT NULL /* data_vencimento */,
	qtdalugado NUMBER(19) NOT NULL /* qtd_alugado */
);



/* Entity Locacao */
create table locacao (
	id RAW(16) NOT NULL,
	datapedido DATE NOT NULL /* data_pedido */,
	obsevacao VARCHAR(255) NOT NULL,
	dataentrega DATE /* data_entrega */,
	valorpago NUMBER(19,4) /* valor_pago */,
	cliente RAW(16) NOT NULL
);



/* Join Tables */
/* master: Locacao as locacao, detail: Produto_alugado as produtoalugado */
create table locacao_produtos (
	locacao_id RAW(16) NOT NULL,
	produtos_id RAW(16) NOT NULL
);

/* Primary Key Constraints */
alter table locadora add constraint pk_locadora_id primary key(id);
alter table tipo_usuario add constraint pk_tipo_usuario_id primary key(id);
alter table cliente add constraint pk_cliente_id primary key(id);
alter table tipo_produto add constraint pk_tipo_produto_id primary key(id);
alter table produto add constraint pk_produto_id primary key(id);
alter table produtoalugado add constraint pk_produtoalugado_id primary key(id);
alter table locacao_produtos add constraint pk_locacao_produtos primary key(locacao_id, produtos_id);
alter table locacao add constraint pk_locacao_id primary key(id);

/* Foreign Key Constraints */
alter table cliente add constraint fkfzwrae5pi4bxsk5cprqba6pz7000 foreign key (tipo) references tipo_usuario (id);
alter table produto add constraint fkkdgm1bugt7pwyrplbv28zcuqyfho foreign key (tipo) references tipo_produto (id);
alter table produtoalugado add constraint fkkuc44lve7mragvm7ga8ons1vfmmc foreign key (produto) references produto (id);
alter table locacao_produtos add constraint fkvos1lxaetdl18mdcfqcg6xyl0ifa foreign key (locacao_id) references locacao (id);
alter table locacao_produtos add constraint fkelxtppr6vmsgjflx3m9gndupgxug foreign key (produtos_id) references produtoalugado (id);
alter table locacao add constraint fkeqoarzuo04fi78i1kvhlfryfhvt8 foreign key (cliente) references cliente (id);

/* Unique Key Constraints */

/* Sequences for auto increment entity ids */
