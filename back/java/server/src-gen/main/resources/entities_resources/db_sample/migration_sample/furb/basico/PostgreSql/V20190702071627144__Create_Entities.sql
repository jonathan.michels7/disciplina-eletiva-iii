/* Database: PostgreSql. Generation date: 2019-07-02 07:16:27:144 */
/* Entity Locadora */
create table locadora (
	id UUID NOT NULL,
	datacadastro DATE NOT NULL /* data_cadastro */,
	nome VARCHAR(255) NOT NULL
);



/* Entity TipoUsuario */
create table tipo_usuario (
	id UUID NOT NULL,
	nome VARCHAR(255) NOT NULL
);



/* Entity Cliente */
create table cliente (
	id UUID NOT NULL,
	nome VARCHAR(255) NOT NULL,
	datanascimento DATE /* data_nascimento */,
	cpf VARCHAR(255),
	nomelogin VARCHAR(255) NOT NULL /* nome_login */,
	senhalogin VARCHAR(255) NOT NULL /* senha_login */,
	tipo UUID NOT NULL
);



/* Entity TipoProduto */
create table tipo_produto (
	id UUID NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	valor NUMERIC(19,4) NOT NULL,
	diasalugado NUMERIC(19) NOT NULL /* dias_alugado */
);



/* Entity Produto */
create table produto (
	id UUID NOT NULL,
	nome VARCHAR(255) NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	quantidade NUMERIC(19) NOT NULL,
	tipo UUID NOT NULL,
	id_imagem VARCHAR(255) /* idImagem */
);



/* Entity Produto_alugado */
create table produtoalugado (
	id UUID NOT NULL,
	produto UUID NOT NULL,
	datavencimento DATE NOT NULL /* data_vencimento */,
	qtdalugado NUMERIC(19) NOT NULL /* qtd_alugado */
);



/* Entity Locacao */
create table locacao (
	id UUID NOT NULL,
	datapedido DATE NOT NULL /* data_pedido */,
	obsevacao VARCHAR(255) NOT NULL,
	dataentrega DATE /* data_entrega */,
	valorpago NUMERIC(19,4) /* valor_pago */,
	cliente UUID NOT NULL
);



/* Join Tables */
/* master: Locacao as locacao, detail: Produto_alugado as produtoalugado */
create table locacao_produtos (
	locacao_id UUID NOT NULL,
	produtos_id UUID NOT NULL
);

/* Primary Key Constraints */
alter table locadora add constraint pk_locadora_id primary key(id);
alter table tipo_usuario add constraint pk_tipo_usuario_id primary key(id);
alter table cliente add constraint pk_cliente_id primary key(id);
alter table tipo_produto add constraint pk_tipo_produto_id primary key(id);
alter table produto add constraint pk_produto_id primary key(id);
alter table produtoalugado add constraint pk_produtoalugado_id primary key(id);
alter table locacao_produtos add constraint pk_locacao_produtos primary key(locacao_id, produtos_id);
alter table locacao add constraint pk_locacao_id primary key(id);

/* Foreign Key Constraints */
alter table cliente add constraint fkgc41tuyf7mveeknr8xvteren4xyg foreign key (tipo) references tipo_usuario (id);
alter table produto add constraint fkhxsfkrp4li59dnnhs0ueu4yrc6yl foreign key (tipo) references tipo_produto (id);
alter table produtoalugado add constraint fkyauhnnmkjbeu3djlzjbtoneatfaq foreign key (produto) references produto (id);
alter table locacao_produtos add constraint fkta3uvysrxp9hx2r5zbxvdychxyqm foreign key (locacao_id) references locacao (id);
alter table locacao_produtos add constraint fkqp2zmvvnrqy2pxutnfvs0fwmq2mu foreign key (produtos_id) references produtoalugado (id);
alter table locacao add constraint fktwuoorycwjxz8cddrfrnsqj599fv foreign key (cliente) references cliente (id);

/* Unique Key Constraints */

/* Sequences for auto increment entity ids */
