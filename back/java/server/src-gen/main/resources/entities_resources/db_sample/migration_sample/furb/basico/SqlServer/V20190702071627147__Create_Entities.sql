/* Database: SqlServer. Generation date: 2019-07-02 07:16:27:147 */
/* Entity Locadora */
create table locadora (
	id UNIQUEIDENTIFIER NOT NULL,
	datacadastro DATE NOT NULL /* data_cadastro */,
	nome VARCHAR(255) NOT NULL
);



/* Entity TipoUsuario */
create table tipo_usuario (
	id UNIQUEIDENTIFIER NOT NULL,
	nome VARCHAR(255) NOT NULL
);



/* Entity Cliente */
create table cliente (
	id UNIQUEIDENTIFIER NOT NULL,
	nome VARCHAR(255) NOT NULL,
	datanascimento DATE /* data_nascimento */,
	cpf VARCHAR(255),
	nomelogin VARCHAR(255) NOT NULL /* nome_login */,
	senhalogin VARCHAR(255) NOT NULL /* senha_login */,
	tipo UNIQUEIDENTIFIER NOT NULL
);



/* Entity TipoProduto */
create table tipo_produto (
	id UNIQUEIDENTIFIER NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	valor FLOAT(32) NOT NULL,
	diasalugado NUMERIC(19) NOT NULL /* dias_alugado */
);



/* Entity Produto */
create table produto (
	id UNIQUEIDENTIFIER NOT NULL,
	nome VARCHAR(255) NOT NULL,
	descricao VARCHAR(255) NOT NULL,
	quantidade NUMERIC(19) NOT NULL,
	tipo UNIQUEIDENTIFIER NOT NULL,
	id_imagem VARCHAR(255) /* idImagem */
);



/* Entity Produto_alugado */
create table produtoalugado (
	id UNIQUEIDENTIFIER NOT NULL,
	produto UNIQUEIDENTIFIER NOT NULL,
	datavencimento DATE NOT NULL /* data_vencimento */,
	qtdalugado NUMERIC(19) NOT NULL /* qtd_alugado */
);



/* Entity Locacao */
create table locacao (
	id UNIQUEIDENTIFIER NOT NULL,
	datapedido DATE NOT NULL /* data_pedido */,
	obsevacao VARCHAR(255) NOT NULL,
	dataentrega DATE /* data_entrega */,
	valorpago FLOAT(32) /* valor_pago */,
	cliente UNIQUEIDENTIFIER NOT NULL
);



/* Join Tables */
/* master: Locacao as locacao, detail: Produto_alugado as produtoalugado */
create table locacao_produtos (
	locacao_id UNIQUEIDENTIFIER NOT NULL,
	produtos_id UNIQUEIDENTIFIER NOT NULL
);

/* Primary Key Constraints */
alter table locadora add constraint pk_locadora_id primary key(id);
alter table tipo_usuario add constraint pk_tipo_usuario_id primary key(id);
alter table cliente add constraint pk_cliente_id primary key(id);
alter table tipo_produto add constraint pk_tipo_produto_id primary key(id);
alter table produto add constraint pk_produto_id primary key(id);
alter table produtoalugado add constraint pk_produtoalugado_id primary key(id);
alter table locacao_produtos add constraint pk_locacao_produtos primary key(locacao_id, produtos_id);
alter table locacao add constraint pk_locacao_id primary key(id);

/* Foreign Key Constraints */
alter table cliente add constraint fkim6skcciycjnesdag7jlwftuhzuq foreign key (tipo) references tipo_usuario (id);
alter table produto add constraint fkl5pmw27ozohzc2is7hq1z1uhuz2n foreign key (tipo) references tipo_produto (id);
alter table produtoalugado add constraint fkp7zi39ysoq5kurezkslkm0xnjb92 foreign key (produto) references produto (id);
alter table locacao_produtos add constraint fkdyr7nesfircl6tqy5jgfoch4hf9o foreign key (locacao_id) references locacao (id);
alter table locacao_produtos add constraint fkgm1utimzmn3c1gauxkqrxypfpghm foreign key (produtos_id) references produtoalugado (id);
alter table locacao add constraint fkwsbqrvr73lyoekfauynfmszadszo foreign key (cliente) references cliente (id);

/* Unique Key Constraints */

/* Sequences for auto increment entity ids */
