import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FeaturesModule } from './features/features.module';
import { TranslateModule } from '@ngx-translate/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GrowlModule } from 'primeng/growl';
import {
    CyclicJsonInterceptorModule,
    LocaleModule,
    BreadcrumbModule,
    LoadingStateModule,
    ProductHeaderModule
} from '@seniorsistemas/angular-components';
import { CoreModule } from './core/core.module';
import { MessageService } from 'primeng/components/common/messageservice';
import { HeaderComponent } from './features/header/header.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CyclicJsonInterceptorModule,
    LocaleModule.forRoot(),
    BreadcrumbModule,
    LoadingStateModule,
    GrowlModule,
    ProductHeaderModule,
    TranslateModule.forRoot(),
    FeaturesModule,
    CoreModule,
  ],
  providers: [
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
