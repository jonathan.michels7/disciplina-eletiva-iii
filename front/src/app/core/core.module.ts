import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteModule } from './entities/cliente/cliente.module';
import { LocacaoModule } from './entities/locacao/locacao.module';
import { LocadoraModule } from './entities/locadora/locadora.module';
import { FilmeModule } from './entities/filme/filme.module';
import { TipoUsuarioModule } from './entities/tipoUsuario/tipoUsuario.module';
import { TipoProdutoModule } from './entities/tipoProduto/tipoProduto.module';

@NgModule({
  imports: [
    CommonModule,
    ClienteModule,
    LocacaoModule,
    TipoUsuarioModule,
    TipoProdutoModule,
    LocadoraModule,
    FilmeModule
  ],
  declarations: []
})
export class CoreModule { }
