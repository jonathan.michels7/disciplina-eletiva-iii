import { TipoUsuario } from "../tipoUsuario/tipoUsuario";

export interface ClienteDto {
     nome: string;
     id: number;
     data_nascimento: Date;
     cpf: string;
     nome_login: String;
     senha_login: String;
     tipo: TipoUsuario;
}
