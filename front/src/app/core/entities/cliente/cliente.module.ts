import { NgModule } from '@angular/core';
import { ClienteService } from './cliente.service';
import { TipoUsuarioService } from '~root/src/app/core/entities/tipoUsuario/tipoUsuario.service';

@NgModule({
    imports: [

    ],
    providers: [
      ClienteService,
      TipoUsuarioService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class ClienteModule {}
