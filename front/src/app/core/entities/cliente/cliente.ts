import * as moment from 'moment';
import { ClienteDto } from './cliente.dto';
import { TipoUsuario } from '../tipoUsuario/tipoUsuario';

export class Cliente {
    public nome: string;
    public data_nascimento: string;
    public cpf: string;
    public id: number;
    public nome_login: String;
    public senha_login: String;
    public tipo: TipoUsuario;


    public static fromDto(clienteDto: ClienteDto, originEntity?: string): Cliente {
        const model: any = { ...clienteDto };

        model.data_nascimento = model.data_nascimento && moment(model.data_nascimento).toDate();

        return model as Cliente;
    }

    public static toDto(cliente: Cliente, originEntity?: string): ClienteDto {
        const dto: any = { ...cliente };

        dto.data_nascimento = dto.data_nascimento && moment(dto.data_nascimento).format('YYYY-MM-DD');
        delete dto.label;

        return dto;
    }
}
