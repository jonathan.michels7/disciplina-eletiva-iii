import { TipoProduto } from "../tipoProduto/tipoProduto";

export interface FilmeDto {
     nome: string;
     id: number;
     tipo: TipoProduto;
     descricao: string;
     quantidade: number;
     idImagem: string;
}
