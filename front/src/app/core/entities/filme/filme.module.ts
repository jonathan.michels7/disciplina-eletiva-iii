import { NgModule } from '@angular/core';
import { FilmeService } from './filme.service';
import { TipoProdutoService } from '~root/src/app/core/entities/TipoProduto/tipoProduto.service';

@NgModule({
    imports: [

    ],
    providers: [
      FilmeService,
      TipoProdutoService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class FilmeModule {}
