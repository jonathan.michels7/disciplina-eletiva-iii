import * as moment from 'moment';
import { FilmeDto } from './filme.dto';
import { TipoProduto } from '../tipoProduto/tipoProduto';

export class Filme {
    public nome: string;
    public tipo: TipoProduto;
    public descricao: string;
    public quantidade: number;
    public id: number;
    public idImagem: string;

    public static fromDto(filmeDto: FilmeDto, originEntity?: string): Filme {
        const model: any = { ...filmeDto };
        return model as Filme;
    }

    public static toDto(filme: Filme, originEntity?: string): FilmeDto {
        const dto: any = { ...filme };
        delete dto.label;

        return dto;
    }
}
