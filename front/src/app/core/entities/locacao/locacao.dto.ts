import { ProdutoAlugado } from "../produtoAlugado/produtoAlugado";
import { Cliente } from "../cliente/cliente";

export interface LocacaoDto {
     produtos: ProdutoAlugado[];
     obsevacao: string;
     cliente: Cliente;
     data_entrega: Date;
     data_pedido: Date;
     valor_pago: number;
     id: number;
}
