import { NgModule } from '@angular/core';
import { LocacaoService } from './locacao.service';
import { FilmeService } from '../filme/filme.service';
import { ClienteService } from '../cliente/cliente.service';
import { ProdutoAlugadoService } from '../produtoAlugado/produtoAlugado.service';

@NgModule({
    imports: [

    ],
    providers: [
      LocacaoService,
      FilmeService,
      ClienteService,
      ProdutoAlugadoService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class LocacaoModule {}
