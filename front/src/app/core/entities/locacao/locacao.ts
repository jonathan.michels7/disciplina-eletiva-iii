import * as moment from 'moment';
import { LocacaoDto } from './locacao.dto';
import { ProdutoAlugado } from '../produtoAlugado/produtoAlugado';
import { Cliente } from '../cliente/cliente';

export class Locacao {
    public produtos: ProdutoAlugado[];
    public obsevacao: string;
    public data_entrega: Date;
    public data_pedido: Date;
    public valor_pago: number;
    public id: number;
    public cliente: Cliente;

    public static fromDto(locacaoDto: LocacaoDto, originEntity?: string): Locacao {
        const model: any = { ...locacaoDto };

        model.data_entrega = model.data_entrega && moment(model.data_entrega).toDate();
        model.data_pedido = model.data_pedido && moment(model.data_pedido).toDate();

        return model as Locacao;
    }

    public static toDto(locacao: Locacao, originEntity?: string): LocacaoDto {
        const dto: any = { ...locacao };

        dto.data_entrega = dto.data_entrega && moment(dto.data_entrega).format('YYYY-MM-DD');
        dto.data_pedido = dto.data_pedido && moment(dto.data_pedido).format('YYYY-MM-DD');
        delete dto.label;

        return dto;
    }
}
