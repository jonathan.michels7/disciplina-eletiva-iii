import { NgModule } from '@angular/core';
import { LocadoraService } from './locadora.service';

@NgModule({
    imports: [

    ],
    providers: [
      LocadoraService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class LocadoraModule {}
