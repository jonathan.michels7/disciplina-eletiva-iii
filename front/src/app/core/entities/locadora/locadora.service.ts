import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MessageService } from "primeng/components/common/messageservice";

import { LocadoraDto } from "~core/entities/locadora/locadora.dto";
import { EntityService } from "~core/entities/entity-service.service";

@Injectable()
export class LocadoraService extends EntityService<LocadoraDto> {
  constructor(protected http: HttpClient, protected messageService: MessageService) {
    super(http, `https://platform-homologx.senior.com.br/t/senior.com.br/bridge/1.0/rest/furb/basico/entities/locadora`);
  }
}