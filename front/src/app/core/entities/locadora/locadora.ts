import * as moment from 'moment';
import { LocadoraDto } from './locadora.dto';

export class Locadora {
    public nome: string;
    public LocadoraDto: string;
    public id: number;

    public static fromDto(locadoraDto: LocadoraDto, originEntity?: string): Locadora {
        const model: any = { ...locadoraDto };

        model.data_cadastro = model.data_cadastro && moment(model.data_cadastro).toDate();

        return model as Locadora;
    }

    public static toDto(locadora: Locadora, originEntity?: string): LocadoraDto {
        const dto: any = { ...locadora };

        dto.data_cadastro = dto.data_cadastro && moment(dto.data_cadastro).format('YYYY-MM-DD');
        delete dto.label;

        return dto;
    }
}
