import { Filme } from "../filme/filme";

export interface ProdutoAlugadoDto {
     nome: string;
     id: number;
     data_vencimento: string;
     qtd_alugado: number;
     produto: Filme;
}
