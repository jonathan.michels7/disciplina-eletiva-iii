import { NgModule } from '@angular/core';
import { ProdutoAlugadoService } from './produtoAlugado.service';

@NgModule({
    imports: [

    ],
    providers: [
      ProdutoAlugadoService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class ProdutoAlugadoModule {}
