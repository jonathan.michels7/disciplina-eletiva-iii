import * as moment from 'moment';
import { ProdutoAlugadoDto } from './produtoAlugado.dto';
import { Filme } from '../filme/filme';

export class ProdutoAlugado {
    public nome: string;
    public id: number;
    public data_vencimento: Date;
    public qtd_alugado: number;
    public produto: Filme;


    public static fromDto(produtoAlugadoDto: ProdutoAlugadoDto, originEntity?: string): ProdutoAlugado {
        const model: any = { ...produtoAlugadoDto };

        model.data_vencimento = model.data_vencimento && moment(model.data_vencimento).toDate();

        return model as ProdutoAlugado;
    }

    public static toDto(produtoAlugado: ProdutoAlugado, originEntity?: string): ProdutoAlugadoDto {
        const dto: any = { ...produtoAlugado };

        dto.data_vencimento = dto.data_vencimento && moment(dto.data_vencimento).format('YYYY-MM-DD');
        delete dto.label;

        return dto;
    }
}
