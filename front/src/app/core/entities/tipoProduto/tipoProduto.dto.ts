export interface TipoProdutoDto {
     descricao: string;
     id: number;
     valor: number;
     dias_alugado: number;
}
