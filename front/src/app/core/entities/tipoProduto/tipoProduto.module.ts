import { NgModule } from '@angular/core';
import { TipoProdutoService } from './tipoProduto.service';

@NgModule({
    imports: [

    ],
    providers: [
      TipoProdutoService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class TipoProdutoModule {}
