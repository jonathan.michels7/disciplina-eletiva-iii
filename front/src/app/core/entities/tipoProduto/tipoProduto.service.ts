import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MessageService } from "primeng/components/common/messageservice";

import { TipoProdutoDto } from "~core/entities/tipoProduto/tipoProduto.dto";
import { EntityService } from "~core/entities/entity-service.service";

@Injectable()
export class TipoProdutoService extends EntityService<TipoProdutoDto> {
  constructor(protected http: HttpClient, protected messageService: MessageService) {
    super(http, `https://platform-homologx.senior.com.br/t/senior.com.br/bridge/1.0/rest/furb/basico/entities/tipoProduto`);
  }
}