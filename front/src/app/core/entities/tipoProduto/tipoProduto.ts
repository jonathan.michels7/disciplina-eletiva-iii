import { TipoProdutoDto } from './tipoProduto.dto';

export class TipoProduto {
    public descricao: string;
    public id: number;
    public valor: number;
    public dias_alugado: number;

    public static fromDto(tipoProdutoDto: TipoProdutoDto, originEntity?: string): TipoProduto {
        const model: any = { ...tipoProdutoDto };
        return model as TipoProduto;
    }

    public static toDto(tipoProduto: TipoProduto, originEntity?: string): TipoProdutoDto {
        const dto: any = { ...tipoProduto };
        delete dto.label;
        return dto;
    }
}
