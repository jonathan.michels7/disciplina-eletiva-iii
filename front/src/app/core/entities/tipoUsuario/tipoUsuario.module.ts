import { NgModule } from '@angular/core';
import { TipoUsuarioService } from './tipoUsuario.service';

@NgModule({
    imports: [

    ],
    providers: [
      TipoUsuarioService,
    ],
    declarations: [
    ],
    exports: [
    ],

})
export class TipoUsuarioModule {}
