import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { MessageService } from "primeng/components/common/messageservice";
import { TipoUsuarioDto } from "~core/entities/tipoUsuario/tipoUsuario.dto";
import { EntityService } from "~core/entities/entity-service.service";

@Injectable()
export class TipoUsuarioService extends EntityService<TipoUsuarioDto> {
  constructor(protected http: HttpClient, protected messageService: MessageService) {
    super(http, `https://platform-homologx.senior.com.br/t/senior.com.br/bridge/1.0/rest/furb/basico/entities/tipoUsuario`);
  }
}