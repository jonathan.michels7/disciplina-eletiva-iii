import { TipoUsuarioDto } from './tipoUsuario.dto';

export class TipoUsuario {
    public nome: string;
    public id: number;

    public static fromDto(tipoUsuarioDto: TipoUsuarioDto, originEntity?: string): TipoUsuario {
        const model: any = { ...tipoUsuarioDto };
        return model as TipoUsuario;
    }

    public static toDto(tipoUsuario: TipoUsuario, originEntity?: string): TipoUsuarioDto {
        const dto: any = { ...tipoUsuario };
        delete dto.label;
        return dto;
    }
}
