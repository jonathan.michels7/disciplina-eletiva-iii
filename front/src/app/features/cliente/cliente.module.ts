import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClienteFormComponent } from './views/form/form.component';
import { ClienteListComponent } from './views/list/list.component';
import { ClienteRouterModule } from './cliente.routing';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { ClienteResolver } from './views/form/cliente.resolver';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {PanelModule} from 'primeng/panel';
import { DynamicFormModule } from '@seniorsistemas/angular-components';
import {DropdownModule} from 'primeng/dropdown';


@NgModule({
  declarations: [ClienteFormComponent, ClienteListComponent],
  imports: [
    CommonModule,
    DropdownModule,
    ClienteRouterModule,
    CardModule,
    ButtonModule,
    TableModule,
    DynamicFormModule,
    ReactiveFormsModule,
    InputTextModule,
    KeyFilterModule,
    ConfirmDialogModule,
    CalendarModule,
    PanelModule,
    RadioButtonModule,
    MessageModule,
    ToastModule
  ],
  providers: [
    MessageService,
    ClienteResolver
  ]
})
export class ClienteModule { }
