import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteFormComponent } from './views/form/form.component';
import { ClienteListComponent } from './views/list/list.component';
import { ClienteResolver } from './views/form/cliente.resolver';

const routes: Routes = [{
  path: 'cliente',  children: [
    {
      path: 'novo', component: ClienteFormComponent,
    },
    {
      path: 'list', component: ClienteListComponent
    },
    {
      path: 'edit/:id', component: ClienteFormComponent, resolve: [ClienteResolver]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ClienteRouterModule { }
