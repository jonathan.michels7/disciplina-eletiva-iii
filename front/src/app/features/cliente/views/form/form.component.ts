import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Cliente } from 'src/app/core/entities/cliente/cliente';
import { ClienteService } from 'src/app/core/entities/cliente/cliente.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { TipoUsuario } from '~root/src/app/core/entities/tipoUsuario/tipoUsuario';
import { TipoUsuarioService } from '~root/src/app/core/entities/tipoUsuario/tipoUsuario.service';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class ClienteFormComponent implements OnInit {
  public clienteForm: FormGroup;
  @Input() cliente: Cliente;
  private routeParams: any;
  private ngUnsubscribe = new Subject();
  private tipos: SelectItem[];

  constructor(
    private clienteService: ClienteService,
    private tipoUsuarioService: TipoUsuarioService,
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.tipos = [];
    debugger;
    this.clienteForm = this.getFormGroup();
    this.tipoUsuarioService.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
      for(let tipos of contents) {
        this.tipos.push({label: tipos.nome, value: tipos});
      
      }
    });

    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe((params: any) => this.onRouteParamsChange(params));
    this.route.data.pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: any) => this.onRouteDataChange(data));

  }

  private listErrorCatch() {
    return catchError((err: any) => {
      if (err) {
        this.messageService.add({
          key: 'remove-toast',
          severity: 'error',
          summary: 'Erro!',
          detail: `Erro ao carregar a lista!`
        });
      }
      return throwError(err);
    });
}

  private getFormGroup() {
    return this.formBuilder.group({
      nome: new FormControl(undefined, Validators.compose([Validators.required])),
      data_nascimento: new FormControl(undefined, Validators.compose([Validators.required])),
      cpf: new FormControl(undefined, Validators.compose([Validators.required])),
      nome_login: new FormControl(undefined, Validators.compose([Validators.required])),
      senha_login: new FormControl(undefined, Validators.compose([Validators.required])),
      tipo: new FormControl(undefined, Validators.compose([Validators.required])),
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
    });
  }

  public onSave() {

    this.getSaveObservable()
    .pipe(
      catchError((err: any) => {
      console.log(err);
      return throwError(err);
    })
    ).subscribe(() => {
      this.goBack();
      console.log(`Saved`);
    });
  }

  public isNew() {
    return this.routeParams.id === undefined;
  }

  private goBack() {
    const previousRoute = '/cliente/list';
    this.router.navigate([previousRoute], { relativeTo: this.route.parent });
  }

  onCancel(){
    location.reload();
  }

  public onRouteDataChange(data: any) {
    const entity = data[0];
    if (data[0]) {
        const value: any = Cliente.fromDto(entity);
        this.clienteForm.patchValue(value);
    } else {
        this.clienteForm.patchValue(new Cliente());
    }
  }

  public onRouteParamsChange(params: any) {
    this.routeParams = params;
}

  private getSaveObservable() {
    debugger;
    const { value } = this.clienteForm;
    const clienteDto = Cliente.toDto(value);

    let observable;

    if (this.isNew()) {
        observable = this.clienteService.insert(clienteDto);
        this.messageService.add({
          key: 'form-toast',
          severity: 'success',
          summary: `Sucesso!`,
          detail: `O cliente foi inserido com sucesso!`
        });
    } else {
        const id = this.routeParams.id;
        observable = this.clienteService.update(id, clienteDto);
    }

    return observable;
  }

}
