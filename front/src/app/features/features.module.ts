import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeaturesRouterModule } from './features.routing';
import { ClienteModule } from './cliente/cliente.module';
import { FilmeModule } from './filme/filme.module';
import { LocacaoModule } from './locacao/locacao.module';
import { LocadoraModule } from './locadora/locadora.module';
import { TipoProdutoModule } from './tipoProduto/tipoProduto.module';
import { TipoUsuarioModule } from './tipoUsuario/tipoUsuario.module';
import { RouterModule } from '@angular/router';
import { MainModule } from './main/main.module';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [HeaderComponent],
  imports: [
    CommonModule,
    MainModule,
    FeaturesRouterModule,
    ClienteModule,
    TipoUsuarioModule,
    TipoProdutoModule,
    LocadoraModule,
    FilmeModule,
    LocacaoModule
  ],
  exports: [
    RouterModule
  ]
})
export class FeaturesModule { }
