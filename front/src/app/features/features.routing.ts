import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [{
  path: 'clientes', redirectTo: 'cliente/list'
},
{path: 'filmes', redirectTo: 'filme/list'},
{path: 'locacoes', redirectTo: 'locacao/list'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FeaturesRouterModule { }
