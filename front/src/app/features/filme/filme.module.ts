import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilmeFormComponent } from './views/form/form.component';
import { FilmeListComponent } from './views/list/list.component';
import { FilmeRouterModule } from './filme.routing';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { FilmeResolver } from './views/form/filme.resolver';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {PanelModule} from 'primeng/panel';
import { DynamicFormModule } from '@seniorsistemas/angular-components';
import {DropdownModule} from 'primeng/dropdown';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {SpinnerModule} from 'primeng/spinner';
import {GalleriaModule} from 'primeng/galleria';
import {FileUploadModule} from 'primeng/fileupload';


@NgModule({
  declarations: [FilmeFormComponent, FilmeListComponent],
  imports: [
    CommonModule,
    FilmeRouterModule,
    CardModule,
    ButtonModule,
    TableModule,
    DynamicFormModule,
    FileUploadModule,
    GalleriaModule,
    ReactiveFormsModule,
    InputTextModule,
    KeyFilterModule,
    DropdownModule,
    ConfirmDialogModule,
    CalendarModule,
    PanelModule,
    RadioButtonModule,
    MessageModule,
    SpinnerModule,
    ToastModule,
    InputTextareaModule
  ],
  providers: [
    MessageService,
    FilmeResolver
  ]
})
export class FilmeModule { }
