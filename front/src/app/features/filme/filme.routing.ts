import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmeFormComponent } from './views/form/form.component';
import { FilmeListComponent } from './views/list/list.component';
import { FilmeResolver } from './views/form/filme.resolver';

const routes: Routes = [{
  path: 'filme',  children: [
    {
      path: 'novo', component: FilmeFormComponent, data: {
    },
    },
    {
      path: 'list', component: FilmeListComponent
    },
    {
      path: 'editar/:id', component: FilmeFormComponent, resolve: [FilmeResolver]
    }
  ]
}]; 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class FilmeRouterModule { }
