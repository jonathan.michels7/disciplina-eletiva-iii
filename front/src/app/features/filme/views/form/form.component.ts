import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Filme } from 'src/app/core/entities/filme/filme';
import { FilmeService } from 'src/app/core/entities/filme/filme.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import {SelectItem} from 'primeng/api';
import { TipoProdutoService } from '~root/src/app/core/entities/tipoProduto/tipoProduto.service';

@Component({
  selector: 'app-filme-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FilmeFormComponent implements OnInit {
  public filmeForm: FormGroup;
  @Input() filme: Filme;
  private routeParams: any;
  private ngUnsubscribe = new Subject();
  tipos: SelectItem[];
  images: any[];
  uploadedFiles: any[] = [];

  constructor(
    private filmeService: FilmeService,
    private formBuilder: FormBuilder,
    private router: Router,
    private tipoProdutoService: TipoProdutoService,
    private messageService: MessageService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.images = [];
    this.tipos = [];
    this.tipoProdutoService.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
      for(let tipos of contents) {
        this.tipos.push({label: tipos.descricao, value: tipos});
      
      }
    });
    this.filmeForm = this.getFormGroup();
  
    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe((params: any) => this.onRouteParamsChange(params));
    this.route.data.pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: any) => this.onRouteDataChange(data));

  }

  onCancel(){
    location.reload();
  }

  private listErrorCatch() {
    return catchError((err: any) => {
      if (err) {
        this.messageService.add({
          key: 'remove-toast',
          severity: 'error',
          summary: 'Erro!',
          detail: `Erro ao carregar a lista!`
        });
      }
      return throwError(err);
    });
}

  onSelect(event) {
    for(let file of event.files) {
      const path = 'src/app/features/filme/imagens/' + file.name;
      this.filme.idImagem = 'src/app/features/filme/imagens/' + file.name;
      this.images.push({source: path, alt:'Imagem do filme', title:'Imagem do filme'});
    }
}

  private getFormGroup() {
    return this.formBuilder.group({
      nome: new FormControl(undefined, Validators.compose([Validators.required])),
      descricao: new FormControl(undefined, Validators.compose([Validators.required])),
      quantidade: new FormControl(undefined, Validators.compose([Validators.required])),
      tipo: new FormControl(undefined, Validators.compose([Validators.required])),
    });
  }
  

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
    });
  }

  public onSave() {
   
    this.getSaveObservable()
    .pipe(
      catchError((err: any) => {
      console.log(err);
      return throwError(err);
    })
    ).subscribe(() => {
      this.goBack();
      console.log(`Saved`);
    });
  }

  public isNew() {
    return this.routeParams.id === undefined;
  }

  private goBack() {
    const previousRoute = '/filme/list';
    this.router.navigate([previousRoute], { relativeTo: this.route.parent });
  }

  public onRouteDataChange(data: any) {
    const entity = data[0];
    if (data[0]) {
        const value: any = Filme.fromDto(entity);
        debugger;
        this.filmeForm.patchValue(value);
    } else {
        this.filmeForm.patchValue(new Filme());
    }
  }

  public onRouteParamsChange(params: any) {
    this.routeParams = params;
}

  private getSaveObservable() {
    const { value } = this.filmeForm;
    const filmeDto = Filme.toDto(value);

    let observable;

    if (this.isNew()) {
        observable = this.filmeService.insert(filmeDto);
        this.messageService.add({
          key: 'form-toast',
          severity: 'success',
          summary: `Sucesso!`,
          detail: `O filme foi inserido com sucesso!`
        });
    } else {
        const id = this.routeParams.id;
        observable = this.filmeService.update(id, filmeDto);
    }

    return observable;
  }

}
