import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocacaoFormComponent } from './views/form/form.component';
import { LocacaoListComponent } from './views/list/list.component';
import { LocacaoRouterModule } from './locacao.routing';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { LocacaoResolver } from './views/form/locacao.resolver';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {PanelModule} from 'primeng/panel';
import { DynamicFormModule } from '@seniorsistemas/angular-components';
import {PickListModule} from 'primeng/picklist';
import {OrderListModule} from 'primeng/orderlist';


@NgModule({
  declarations: [LocacaoFormComponent, LocacaoListComponent],
  imports: [
    CommonModule,
    LocacaoRouterModule,
    CardModule,
    ButtonModule,
    OrderListModule,
    TableModule,
    DynamicFormModule,
    ReactiveFormsModule,
    InputTextModule,
    PickListModule,
    KeyFilterModule,
    ConfirmDialogModule,
    CalendarModule,
    PanelModule,
    RadioButtonModule,
    MessageModule,
    ToastModule
  ],
  providers: [
    MessageService,
    LocacaoResolver
  ]
})
export class LocacaoModule { }
