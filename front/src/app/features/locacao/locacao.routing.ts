import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocacaoFormComponent } from './views/form/form.component';
import { LocacaoListComponent } from './views/list/list.component';
import { LocacaoResolver } from './views/form/locacao.resolver';

const routes: Routes = [{
  path: 'locacao',  children: [
    {
      path: 'novo', component: LocacaoFormComponent, data: {
    },
    },
    {
      path: 'list', component: LocacaoListComponent
    },
    {
      path: 'edit/:id', component: LocacaoFormComponent, resolve: [LocacaoResolver]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LocacaoRouterModule { }
