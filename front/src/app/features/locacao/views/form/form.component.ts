import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Locacao } from 'src/app/core/entities/locacao/locacao';
import { LocacaoService } from 'src/app/core/entities/locacao/locacao.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Filme } from '~root/src/app/core/entities/filme/filme';
import { FilmeService } from '~root/src/app/core/entities/filme/filme.service';
import { ClienteService } from '~root/src/app/core/entities/cliente/cliente.service';
import {SelectItem} from 'primeng/api';
import { ProdutoAlugadoService } from '~root/src/app/core/entities/produtoAlugado/produtoAlugado.service';
import { ProdutoAlugado } from '~root/src/app/core/entities/produtoAlugado/produtoAlugado';
import { Cliente } from '~root/src/app/core/entities/cliente/cliente';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-locacao-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class LocacaoFormComponent implements OnInit {
  public locacaoForm: FormGroup;
  public produtosAlugadosForm: FormGroup;
  @Input() locacao: Locacao;
  private produtos: ProdutoAlugado;
  private routeParams: any;
  private ngUnsubscribe = new Subject();
  private filmes: any;
  private clientes: any;
  private filmesALocar: any;
  private clienteSelecionado: any;
  private cliente: Cliente;

  constructor(
    private locacaoService: LocacaoService,
    private formBuilder: FormBuilder,
    private router: Router,
    private filmeServer: FilmeService,
    private clienteServer: ClienteService,
    private produtoAlugadoService: ProdutoAlugadoService,
    private messageService: MessageService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {

    debugger;
    this.locacaoForm = this.getFormGroup();
    this.produtosAlugadosForm = this.getFormGroupProdut();
    this.filmesALocar = [];
    this.clienteSelecionado = [];
    this.filmeServer.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
        this.filmes = contents
    });

    this.clienteServer.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
        this.clientes = contents
    });

    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe((params: any) => this.onRouteParamsChange(params));
    this.route.data.pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: any) => this.onRouteDataChange(data));

  }

  onCancel(){
    location.reload();
  }

  private pesquisarCliente(event){
    this.clienteServer.buscaCliente(event)
    .pipe(map(contents => {
      debugger;
      const { value } = this.locacaoForm;
      const locacaoDto = Locacao.toDto(value);
      locacaoDto.cliente = contents[0];
    })).subscribe();
    
  }

  private getFormGroup() {
    return this.formBuilder.group({
      cliente: new FormControl(undefined, Validators.compose([Validators.required])),
      data_pedido: new FormControl(undefined, Validators.compose([Validators.required])),
      data_entrega: new FormControl(undefined, Validators.compose([Validators.required])),
      valor_pago: new FormControl(undefined, Validators.compose([Validators.required])),
      produtos: new FormControl(undefined, Validators.compose([Validators.required])),

    });
  }

  private getFormGroupProdut() {
    return this.formBuilder.group({
      produto: new FormControl(undefined, Validators.compose([Validators.required])),
      data_vencimento: new FormControl(undefined, Validators.compose([Validators.required])),
      qtd_alugado: new FormControl(undefined, Validators.compose([Validators.required])),
      nome: new FormControl(undefined, Validators.compose([Validators.required])),
    });
  }

  private listErrorCatch() {
    return catchError((err: any) => {
      if (err) {
        this.messageService.add({
          key: 'remove-toast',
          severity: 'error',
          summary: 'Erro!',
          detail: `Erro ao carregar a lista!`
        });
      }
      return throwError(err);
    });
}

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
    });
  }

  public onSave() {
    this.getSaveObservable()
  }

  public isNew() {
    return this.routeParams.id === undefined;
  }

  private goBack() {
    const previousRoute = '/locacao/list';
    this.router.navigate([previousRoute], { relativeTo: this.route.parent });
  }

  public onRouteDataChange(data: any) {
    const entity = data[0];
    if (data[0]) {
        const value: any = Locacao.fromDto(entity);
        this.locacaoForm.patchValue(value);
    } else {
        this.locacaoForm.patchValue(new Locacao());
    }
  }

  public onRouteParamsChange(params: any) {
    this.routeParams = params;
}

  private async getSaveObservable() {
    
    const { value } = this.locacaoForm;
    const locacaoDto = Locacao.toDto(value);
    locacaoDto.produtos = [];
    const produtoAlugadoDto = ProdutoAlugado.toDto(this.produtosAlugadosForm.value);
    
    for(let prod of this.filmesALocar){
      produtoAlugadoDto.produto = prod;
      produtoAlugadoDto.data_vencimento = "2019-07-03";
      produtoAlugadoDto.qtd_alugado = 1;
      produtoAlugadoDto.nome = prod.nome;

      await this.produtoAlugadoService.insert2(produtoAlugadoDto)
      .subscribe(({ contents }) => {
      debugger;
      locacaoDto.produtos.push(contents);
    });
    }
    
    locacaoDto.cliente = this.clienteSelecionado[0];

    
    let observable;

    if (this.isNew()) {
        observable = this.locacaoService.insert(locacaoDto).subscribe();
        this.messageService.add({
          key: 'form-toast',
          severity: 'success',
          summary: `Sucesso!`,
          detail: `A locação foi inserida com sucesso!`
        });
    } else {
        const id = this.routeParams.id;
        observable = this.locacaoService.update(id, locacaoDto);
    }

    return observable;
  }

}
