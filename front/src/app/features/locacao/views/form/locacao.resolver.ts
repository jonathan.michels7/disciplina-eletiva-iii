import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Locacao } from 'src/app/core/entities/locacao/locacao';
import { LocacaoService } from 'src/app/core/entities/locacao/locacao.service';


@Injectable()
export class LocacaoResolver implements Resolve<Locacao []> {

    constructor(private service: LocacaoService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return this.service.get(route.paramMap.get('id'));
    }
}
