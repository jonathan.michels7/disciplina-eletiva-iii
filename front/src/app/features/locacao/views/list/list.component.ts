import { Component, OnInit } from '@angular/core';
import { LocacaoService } from 'src/app/core/entities/locacao/locacao.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { Locacao } from 'src/app/core/entities/locacao/locacao';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cliente-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class LocacaoListComponent implements OnInit {

  locacoes: Locacao[];
  columns: any[];
  public locacaoFiltersForm: FormGroup;

  constructor(
    private locacaoService: LocacaoService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    this.locacaoService.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
      this.locacoes = contents;
    });

    this.locacaoFiltersForm = this.formBuilder.group({
      valor: [undefined, Validators.compose([])],
      data_pedido: [undefined, Validators.compose([])],
      cliente: [undefined, Validators.compose([])],
    });

    this.columns = this.getGridColumns();

  }

  private getGridColumns() {
    debugger;
    const gridcloumns = [
      { field: 'cliente', header: 'Cliente' },
      { field: 'data_pedido', header: 'Filme' },
      { field: 'data_pedido', header: 'Data de locação' },
      { field: 'valor', header: 'Valor' },
    ];

    return gridcloumns;
  }

  public onRemove(item: Locacao) {
    this.messageService.add({
      key: 'removeConfirm',
      data: item, sticky: true,
      severity: 'info',
      summary: 'Voce tem certeza?',
      detail: 'Confirme para DELETAR'
    });
  }

  public onAdd() {
    this.router.navigate(['/locacao/novo'], { relativeTo: this.route });
  }

  public editItem(locacao: Locacao) {
    this.router.navigate([`/locacao/edit/${locacao.id}`], { relativeTo: this.route });
  }

  public onRemoveConfirm(item: any) {
    const { id, nome } = item.data;

    this.locacaoService.delete(id).subscribe(() => {
      this.messageService.clear('removeConfirm');
      this.locacoes = this.locacoes.filter(locacao => locacao.id !== id);
      this.locacoes.find((locacao: Locacao) => locacao.id === id);
      this.messageService.add({
        key: 'remove-toast',
        severity: 'success',
        summary: `Sucesso!`,
        detail: `Locacao ${nome} deletado!`
      });
    });
  }

  public onRemoveReject() {
    this.messageService.clear('removeConfirm');
  }

  private listErrorCatch() {
      return catchError((err: any) => {
        if (err) {
          this.messageService.add({
            key: 'remove-toast',
            severity: 'error',
            summary: 'Erro!',
            detail: `Erro ao carregar a lista!`
          });
        }
        return throwError(err);
      });
  }

}
