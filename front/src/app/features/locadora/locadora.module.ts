import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocadoraFormComponent } from './views/form/form.component';
import { LocadoraListComponent } from './views/list/list.component';
import { LocadoraRouterModule } from './locadora.routing';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { LocadoraResolver } from './views/form/locadora.resolver';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {PanelModule} from 'primeng/panel';
import { DynamicFormModule } from '@seniorsistemas/angular-components';


@NgModule({
  declarations: [LocadoraFormComponent, LocadoraListComponent],
  imports: [
    CommonModule,
    LocadoraRouterModule,
    CardModule,
    ButtonModule,
    TableModule,
    DynamicFormModule,
    ReactiveFormsModule,
    InputTextModule,
    KeyFilterModule,
    ConfirmDialogModule,
    CalendarModule,
    PanelModule,
    RadioButtonModule,
    MessageModule,
    ToastModule
  ],
  providers: [
    MessageService,
    LocadoraResolver
  ]
})
export class LocadoraModule { }
