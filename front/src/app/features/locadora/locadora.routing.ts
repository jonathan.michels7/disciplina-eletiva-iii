import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocadoraFormComponent } from './views/form/form.component';
import { LocadoraListComponent } from './views/list/list.component';
import { LocadoraResolver } from './views/form/locadora.resolver';

const routes: Routes = [{
  path: 'locadora',  children: [
    {
      path: 'novo', component: LocadoraFormComponent,
    },
    {
      path: 'list', component: LocadoraListComponent
    },
    {
      path: 'edit/:id', component: LocadoraFormComponent, resolve: [LocadoraResolver]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class LocadoraRouterModule { }
