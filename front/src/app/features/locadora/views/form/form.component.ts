import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Locadora } from 'src/app/core/entities/locadora/locadora';
import { LocadoraService } from 'src/app/core/entities/locadora/locadora.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-locadora-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class LocadoraFormComponent implements OnInit {
  public locadoraForm: FormGroup;
  @Input() locadora: Locadora;
  private routeParams: any;
  private ngUnsubscribe = new Subject();

  constructor(
    private locadoraService: LocadoraService,
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    debugger;
    this.locadoraForm = this.getFormGroup();

    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe((params: any) => this.onRouteParamsChange(params));
    this.route.data.pipe(takeUntil(this.ngUnsubscribe)).subscribe((data: any) => this.onRouteDataChange(data));
  }

  private getFormGroup() {
    return this.formBuilder.group({
      nome: new FormControl(undefined, Validators.compose([Validators.required])),
      data_cadastro: new FormControl(undefined, Validators.compose([Validators.required])),
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
    });
  }

  public onSave() {
    debugger;
    if (!this.locadoraForm.valid) {
      return this.validateAllFormFields(this.locadoraForm);
    }

    this.getSaveObservable()
    .pipe(
      catchError((err: any) => {
      console.log(err);
      return throwError(err);
    })
    ).subscribe(() => {
      this.goBack();
      console.log(`Saved`);
    });
  }

  public isNew() {
    return this.routeParams.id === undefined;
  }

  private goBack() {
    const previousRoute = '/locadora/list';
    this.router.navigate([previousRoute], { relativeTo: this.route.parent });
  }

  public onRouteDataChange(data: any) {
    const entity = data[0];
    if (data[0]) {
        const value: any = Locadora.fromDto(entity);
        this.locadoraForm.patchValue(value);
    } else {
        this.locadoraForm.patchValue(new Locadora());
    }
  }

  public onRouteParamsChange(params: any) {
    this.routeParams = params;
}

onCancel(){
  location.reload();
}

  private getSaveObservable() {
    const { value } = this.locadoraForm;
    const locadoraDto = Locadora.toDto(value);

    let observable;

    if (this.isNew()) {
        observable = this.locadoraService.insert(locadoraDto);
        this.messageService.add({
          key: 'form-toast',
          severity: 'success',
          summary: `Sucesso!`,
          detail: `A locadora foi inserida com sucesso!`
        });
    } else {
        const id = this.routeParams.id;
        observable = this.locadoraService.update(id, locadoraDto);
    }

    return observable;
  }

}
