import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Locadora } from 'src/app/core/entities/locadora/locadora';
import { LocadoraService } from 'src/app/core/entities/locadora/locadora.service';


@Injectable()
export class LocadoraResolver implements Resolve<Locadora []> {

    constructor(private service: LocadoraService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return this.service.get(route.paramMap.get('id'));
    }
}
