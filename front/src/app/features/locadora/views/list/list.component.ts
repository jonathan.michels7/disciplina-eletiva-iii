import { Component, OnInit } from '@angular/core';
import { LocadoraService } from 'src/app/core/entities/locadora/locadora.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Locadora } from '~root/src/app/core/entities/locadora/locadora';

@Component({
  selector: 'app-locadora-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class LocadoraListComponent implements OnInit {

  locadoras: Locadora[];
  columns: any[];
  public locadoraFiltersForm: FormGroup;

  constructor(
    private locadoraService: LocadoraService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    this.locadoraService.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
      this.locadoras = contents;
    });

    this.locadoraFiltersForm = this.formBuilder.group({
      nome: [undefined, Validators.compose([])],
      data_cadastro: [undefined, Validators.compose([])],
    });

    this.columns = this.getGridColumns();

  }

  private getGridColumns() {
    debugger;
    const gridcloumns = [
      { field: 'nome', header: 'Nome' },
      { field: 'data_cadastro', header: 'Data de cadastro' },
    ];

    return gridcloumns;
  }

  public onRemove(item: Locadora) {
    this.messageService.add({
      key: 'removeConfirm',
      data: item, sticky: true,
      severity: 'info',
      summary: 'Voce tem certeza?',
      detail: 'Confirme para DELETAR'
    });
  }

  public onAdd() {
    this.router.navigate(['/locadora/novo'], { relativeTo: this.route });
  }

  public editItem(locadora: Locadora) {
    this.router.navigate([`/locadora/edit/${locadora.id}`], { relativeTo: this.route });
  }

  public onRemoveConfirm(item: any) {
    const { id, nome } = item.data;

    this.locadoraService.delete(id).subscribe(() => {
      this.messageService.clear('removeConfirm');
      this.locadoras = this.locadoras.filter(locadora => locadora.id !== id);
      this.locadoras.find((locadora: Locadora) => locadora.id === id);
      this.messageService.add({
        key: 'remove-toast',
        severity: 'success',
        summary: `Sucesso!`,
        detail: `Locadora ${nome} deletado!`
      });
    });
  }

  public onRemoveReject() {
    this.messageService.clear('removeConfirm');
  }

  private listErrorCatch() {
      return catchError((err: any) => {
        if (err) {
          this.messageService.add({
            key: 'remove-toast',
            severity: 'error',
            summary: 'Erro!',
            detail: `Erro ao carregar a lista!`
          });
        }
        return throwError(err);
      });
  }

}
