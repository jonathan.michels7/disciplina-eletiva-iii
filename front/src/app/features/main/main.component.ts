import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor() { }

  items1: MenuItem[];
  items2: MenuItem[];

    ngOnInit() {
        this.items1 = [
            {
                label: 'Cliente',
                icon: 'pi pi-fw pi-user',
                items: [{
                        label: 'Novo', 
                        icon: 'pi pi-fw pi-user-plus',
                        routerLink: "../cliente/novo"
                    },
                    {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../cliente/list"}
                ]
            },
            {
                label: 'Filmes',
                icon: 'pi pi-fw pi-tablet',
                items: [
                    {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../filme/novo"},
                    {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../filme/list"}
                ]
            },
            {
                label: 'Locação',
                icon: 'pi pi-fw pi-eject',
                items: [
                    {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../locacao/novo"},
                    {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../locacao/list"}
                ]
            },
            {
                label: 'Locadora',
                icon: 'pi pi-fw pi-home',
                items: [{
                        label: 'Novo', 
                        icon: 'pi pi-fw pi-user-plus',
                        routerLink: "../locadora/novo"
                    },
                    {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../locadora/list"}
                ]
            },
            {
                label: 'Tipo de usuário',
                icon: 'pi pi-fw pi-cog',
                items: [{
                        label: 'Novo', 
                        icon: 'pi pi-fw pi-user-plus',
                        routerLink: "../tipoUsuario/novo"
                    },
                    {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../tipoUsuario/list"}
                ]
            },
            {
                label: 'Tipo de produto',
                icon: 'pi pi-fw pi-briefcase',
                items: [{
                        label: 'Novo', 
                        icon: 'pi pi-fw pi-user-plus',
                        routerLink: "../tipoProduto/novo"
                    },
                    {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../tipoProduto/list"}
                ]
            }
        ];


        this.items2 = [
            {
                label: 'Cliente', icon: 'pi pi-fw pi-user',
                items: [
                    [
                        {
                            label: '',
                            items: [
                                {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../cliente/novo"},
                                {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../cliente/novo"}
                            ]
                            
                        }
                    ]
                ]
            },
            {
                label: 'Filmes', icon: 'pi pi-fw pi-tablet',
                items: [
                    [
                        {
                            label: '',
                            items: [
                                {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../filme/novo"},
                                {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../filme/list"}
                            ]
                        }
                    ]
                ]
            },

            {
                label: 'Locação', icon: 'pi pi-fw pi-eject',
                items: [
                    [
                        {
                            label: '',
                            items: [
                                {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../locacao/novo"},
                                {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../locacao/list"}
                            ]
                        }

                    ]
                ]
            },
            {
                label: 'Locadora', icon: 'pi pi-fw pi-home',
                items: [
                    [
                        {
                            label: '',
                            items: [
                                {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../locadora/novo"},
                                {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../locadora/list"}
                            ]
                        }

                    ]
                ]
            },
            {
                label: 'Tipo de usuário', icon: 'pi pi-fw pi-cog',
                items: [
                    [
                        {
                            label: '',
                            items: [
                                {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../tipoUsuario/novo"},
                                {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../tipoUsuario/list"}
                            ]
                        }

                    ]
                ]
            },
            {
                label: 'Tipo de produto', icon: 'pi pi-fw pi-briefcase',
                items: [
                    [
                        {
                            label: '',
                            items: [
                                {label: 'Novo', icon: 'pi pi-fw pi-user-plus', routerLink: "../tipoProduto/novo"},
                                {label: 'Listar', icon: 'pi pi-fw pi-list', routerLink: "../tipoProduto/list"}
                            ]
                        }

                    ]
                ]
            }

        ];
    }

}
