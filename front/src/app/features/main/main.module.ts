import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '~shared/shared.module'
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import {PanelModule} from 'primeng/panel';
import {TieredMenuModule} from 'primeng/tieredmenu';
import {MegaMenuModule} from 'primeng/megamenu';

@NgModule({
  imports: [
    CommonModule,
    TieredMenuModule,
    MegaMenuModule,
    PanelModule,
    SharedModule,
    MainRoutingModule
  ],
  declarations: [MainComponent]
})
export class MainModule { }
