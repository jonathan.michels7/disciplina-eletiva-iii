import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TipoProdutoFormComponent } from './views/form/form.component';
import { TipoProdutoListComponent } from './views/list/list.component';
import { TipoProdutoRouterModule } from './tipoProduto.routing';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { KeyFilterModule } from 'primeng/keyfilter';
import { CalendarModule } from 'primeng/calendar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MessageService } from 'primeng/api';
import { MessageModule } from 'primeng/message';
import { ToastModule } from 'primeng/toast';
import { TipoProdutoResolver } from './views/form/tipoProduto.resolver';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {PanelModule} from 'primeng/panel';
import { DynamicFormModule } from '@seniorsistemas/angular-components';
import {SpinnerModule} from 'primeng/spinner';
import {InputTextareaModule} from 'primeng/inputtextarea';


@NgModule({
  declarations: [TipoProdutoFormComponent, TipoProdutoListComponent],
  imports: [
    CommonModule,
    InputTextareaModule,
    TipoProdutoRouterModule,
    CardModule,
    SpinnerModule,
    ButtonModule,
    TableModule,
    DynamicFormModule,
    ReactiveFormsModule,
    InputTextModule,
    KeyFilterModule,
    ConfirmDialogModule,
    CalendarModule,
    PanelModule,
    RadioButtonModule,
    MessageModule,
    ToastModule
  ],
  providers: [
    MessageService,
    TipoProdutoResolver
  ]
})
export class TipoProdutoModule { }
