import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipoProdutoFormComponent } from './views/form/form.component';
import { TipoProdutoListComponent } from './views/list/list.component';
import { TipoProdutoResolver } from './views/form/tipoProduto.resolver';

const routes: Routes = [{
  path: 'tipoProduto',  children: [
    {
      path: 'novo', component: TipoProdutoFormComponent,
    },
    {
      path: 'list', component: TipoProdutoListComponent
    },
    {
      path: 'edit/:id', component: TipoProdutoFormComponent, resolve: [TipoProdutoResolver]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TipoProdutoRouterModule { }
