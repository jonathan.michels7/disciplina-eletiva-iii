import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TipoProduto } from 'src/app/core/entities/tipoProduto/tipoProduto';
import { TipoProdutoService } from 'src/app/core/entities/tipoProduto/tipoProduto.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-tipoProduto-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class TipoProdutoFormComponent implements OnInit {
  public tipoProdutoForm: FormGroup;
  @Input() tipoProduto: TipoProduto;
  private routeParams: any;
  private ngUnsubscribe = new Subject();

  constructor(
    private tipoProdutoService: TipoProdutoService,
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    debugger;
    this.tipoProdutoForm = this.getFormGroup();

    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe((params: any) => this.onRouteParamsChange(params));

  }

  onCancel(){
    location.reload();
  }

  private getFormGroup() {
    return this.formBuilder.group({
      descricao: new FormControl(undefined, Validators.compose([Validators.required])),
      valor: new FormControl(undefined, Validators.compose([Validators.required])),
      dias_alugado: new FormControl(undefined, Validators.compose([Validators.required])),
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
    });
  }

  public onSave() {
    debugger;
    if (!this.tipoProdutoForm.valid) {
      return this.validateAllFormFields(this.tipoProdutoForm);
    }

    this.getSaveObservable()
    .pipe(
      catchError((err: any) => {
      console.log(err);
      return throwError(err);
    })
    ).subscribe(() => {
      this.goBack();
      console.log(`Saved`);
    });
  }

  public isNew() {
    return this.routeParams.id === undefined;
  }

  private goBack() {
    const previousRoute = '/tipoProduto/list';
    this.router.navigate([previousRoute], { relativeTo: this.route.parent });
  }

  public onRouteParamsChange(params: any) {
    this.routeParams = params;
}

  private getSaveObservable() {
    const { value } = this.tipoProdutoForm;
    const tipoProdutoDto = TipoProduto.toDto(value);

    let observable;

    if (this.isNew()) {
        observable = this.tipoProdutoService.insert(tipoProdutoDto);
        this.messageService.add({
          key: 'form-toast',
          severity: 'success',
          summary: `Sucesso!`,
          detail: `Tipo de produto foi inserido com sucesso!`
        });
    } else {
        const id = this.routeParams.id;
        observable = this.tipoProdutoService.update(id, tipoProdutoDto);
    }

    return observable;
  }

}
