import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TipoProduto } from 'src/app/core/entities/tipoProduto/tipoProduto';
import { TipoProdutoService } from 'src/app/core/entities/tipoProduto/tipoProduto.service';


@Injectable()
export class TipoProdutoResolver implements Resolve<TipoProduto[]> {

    constructor(private service: TipoProdutoService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return this.service.get(route.paramMap.get('id'));
    }
}
