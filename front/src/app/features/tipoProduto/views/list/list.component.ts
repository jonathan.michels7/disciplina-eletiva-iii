import { Component, OnInit } from '@angular/core';
import { TipoProdutoService } from 'src/app/core/entities/tipoProduto/tipoProduto.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { TipoProduto } from 'src/app/core/entities/tipoProduto/tipoProduto';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-tipoProduto-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class TipoProdutoListComponent implements OnInit {

  tipoProdutos: TipoProduto[];
  columns: any[];
  public tipoProdutoFiltersForm: FormGroup;

  constructor(
    private tipoProdutoService: TipoProdutoService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    this.tipoProdutoService.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
      this.tipoProdutos = contents;
    });

    this.tipoProdutoFiltersForm = this.formBuilder.group({
      descricao: [undefined, Validators.compose([])],
      valor: [undefined, Validators.compose([])],
    });

    this.columns = this.getGridColumns();

  }

  private getGridColumns() {
    debugger;
    const gridcloumns = [
      { field: 'descricao', header: 'Descrição' },
      { field: 'dias_alugado', header: 'Dias alugado' },
      { field: 'valor', header: 'Valor' },
    ];

    return gridcloumns;
  }

  public onRemove(item: TipoProduto) {
    this.messageService.add({
      key: 'removeConfirm',
      data: item, sticky: true,
      severity: 'info',
      summary: 'Voce tem certeza?',
      detail: 'Confirme para DELETAR'
    });
  }

  public onAdd() {
    this.router.navigate(['/tipoProduto/novo'], { relativeTo: this.route });
  }

  public editItem(tipoProduto: TipoProduto) {
    this.router.navigate([`/tipoProduto/edit/${tipoProduto.id}`], { relativeTo: this.route });
  }

  public onRemoveConfirm(item: any) {
    const { id, descricao } = item.data;

    this.tipoProdutoService.delete(id).subscribe(() => {
      this.messageService.clear('removeConfirm');
      this.tipoProdutos = this.tipoProdutos.filter(tipoProduto => tipoProduto.id !== id);
      this.tipoProdutos.find((tipoProduto: TipoProduto) => tipoProduto.id === id);
      this.messageService.add({
        key: 'remove-toast',
        severity: 'success',
        summary: `Sucesso!`,
        detail: `TipoProduto ${descricao} deletado!`
      });
    });
  }

  public onRemoveReject() {
    this.messageService.clear('removeConfirm');
  }

  private listErrorCatch() {
      return catchError((err: any) => {
        if (err) {
          this.messageService.add({
            key: 'remove-toast',
            severity: 'error',
            summary: 'Erro!',
            detail: `Erro ao carregar a lista!`
          });
        }
        return throwError(err);
      });
  }

}
