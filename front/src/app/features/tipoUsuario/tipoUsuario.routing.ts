import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TipoUsuarioFormComponent } from './views/form/form.component';
import { TipoUsuarioListComponent } from './views/list/list.component';
import { TipoUsuarioResolver } from './views/form/tipoUsuario.resolver';

const routes: Routes = [{
  path: 'tipoUsuario',  children: [
    {
      path: 'novo', component: TipoUsuarioFormComponent,
    },
    {
      path: 'list', component: TipoUsuarioListComponent
    },
    {
      path: 'edit/:id', component: TipoUsuarioFormComponent, resolve: [TipoUsuarioResolver]
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TipoUsuarioRouterModule { }
