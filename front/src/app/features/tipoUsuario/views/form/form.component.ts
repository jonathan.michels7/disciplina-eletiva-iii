import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { TipoUsuario } from 'src/app/core/entities/tipoUsuario/tipoUsuario';
import { TipoUsuarioService } from 'src/app/core/entities/tipoUsuario/tipoUsuario.service';
import { catchError, takeUntil } from 'rxjs/operators';
import { throwError, Subject } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-tipoUsuario-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class TipoUsuarioFormComponent implements OnInit {
  public tipoUsuarioForm: FormGroup;
  @Input() tipoUsuario: TipoUsuario;
  private routeParams: any;
  private ngUnsubscribe = new Subject();

  constructor(
    private tipoUsuarioService: TipoUsuarioService,
    private formBuilder: FormBuilder,
    private router: Router,
    private messageService: MessageService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    debugger;
    this.tipoUsuarioForm = this.getFormGroup();

    this.route.params.pipe(takeUntil(this.ngUnsubscribe)).subscribe((params: any) => this.onRouteParamsChange(params));

  }

  private getFormGroup() {
    return this.formBuilder.group({
      nome: new FormControl(undefined, Validators.compose([Validators.required])),
    });
  }

  private validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof FormControl) {
          control.markAsDirty({ onlySelf: true });
        } else if (control instanceof FormGroup) {
          this.validateAllFormFields(control);
        }
    });
  }

  public onSave() {
    debugger;
    if (!this.tipoUsuarioForm.valid) {
      return this.validateAllFormFields(this.tipoUsuarioForm);
    }

    this.getSaveObservable()
    .pipe(
      catchError((err: any) => {
      console.log(err);
      return throwError(err);
    })
    ).subscribe(() => {
      this.goBack();
      console.log(`Saved`);
    });
  }

  public isNew() {
    return this.routeParams.id === undefined;
  }

  private goBack() {
    const previousRoute = '/tipoUsuario/list';
    this.router.navigate([previousRoute], { relativeTo: this.route.parent });
  }

  onCancel(){
    location.reload();
  }

  public onRouteDataChange(data: any) {
    const entity = data[0];
    if (data[0]) {
        const value: any = TipoUsuario.fromDto(entity);
        this.tipoUsuarioForm.patchValue(value);
    } else {
        this.tipoUsuarioForm.patchValue(new TipoUsuario());
    }
  }

  public onRouteParamsChange(params: any) {
    this.routeParams = params;
}

  private getSaveObservable() {
    const { value } = this.tipoUsuarioForm;
    const tipoUsuarioDto = TipoUsuario.toDto(value);

    let observable;

    if (this.isNew()) {
        observable = this.tipoUsuarioService.insert(tipoUsuarioDto);
        this.messageService.add({
          key: 'form-toast',
          severity: 'success',
          summary: `Sucesso!`,
          detail: `Tipo de usuário foi inserido com sucesso!`
        });
    } else {
        const id = this.routeParams.id;
        observable = this.tipoUsuarioService.update(id, tipoUsuarioDto);
    }

    return observable;
  }

}
