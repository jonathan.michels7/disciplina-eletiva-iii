import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TipoUsuario } from 'src/app/core/entities/tipoUsuario/tipoUsuario';
import { TipoUsuarioService } from 'src/app/core/entities/tipoUsuario/tipoUsuario.service';


@Injectable()
export class TipoUsuarioResolver implements Resolve<TipoUsuario []> {

    constructor(private service: TipoUsuarioService) { }

    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<any> | Promise<any> | any {
        return this.service.get(route.paramMap.get('id'));
    }
}
