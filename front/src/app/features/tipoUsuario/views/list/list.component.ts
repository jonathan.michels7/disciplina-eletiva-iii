import { Component, OnInit } from '@angular/core';
import { TipoUsuarioService } from 'src/app/core/entities/tipoUsuario/tipoUsuario.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { FormBuilder,  FormGroup, Validators } from '@angular/forms';
import { TipoUsuario } from '~root/src/app/core/entities/tipoUsuario/tipoUsuario';

@Component({
  selector: 'app-tipoUsuario-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class TipoUsuarioListComponent implements OnInit {

  tipoUsuarios: TipoUsuario[];
  columns: any[];
  public tipoUsuarioFiltersForm: FormGroup;

  constructor(
    private tipoUsuarioService: TipoUsuarioService,
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    this.tipoUsuarioService.list()
    .pipe(this.listErrorCatch())
    .subscribe(({ contents }) => {
      this.tipoUsuarios = contents;
    });

    this.tipoUsuarioFiltersForm = this.formBuilder.group({
      nome: [undefined, Validators.compose([])],
    });

    this.columns = this.getGridColumns();

  }

  private getGridColumns() {
    debugger;
    const gridcloumns = [
      { field: 'nome', header: 'Nome' },
    ];

    return gridcloumns;
  }

  public onRemove(item: TipoUsuario) {
    this.messageService.add({
      key: 'removeConfirm',
      data: item, sticky: true,
      severity: 'info',
      summary: 'Voce tem certeza?',
      detail: 'Confirme para DELETAR'
    });
  }

  public onAdd() {
    this.router.navigate(['/tipoUsuario/novo'], { relativeTo: this.route });
  }

  public editItem(tipoUsuario: TipoUsuario) {
    this.router.navigate([`/tipoUsuario/edit/${tipoUsuario.id}`], { relativeTo: this.route });
  }

  public onRemoveConfirm(item: any) {
    const { id, nome } = item.data;

    this.tipoUsuarioService.delete(id).subscribe(() => {
      this.messageService.clear('removeConfirm');
      this.tipoUsuarios = this.tipoUsuarios.filter(tipoUsuario => tipoUsuario.nome !== id);
      this.tipoUsuarios.find((tipoUsuario: TipoUsuario) => tipoUsuario.id === id);
      this.messageService.add({
        key: 'remove-toast',
        severity: 'success',
        summary: `Sucesso!`,
        detail: `TipoUsuario ${nome} deletado!`
      });
    });
  }

  public onRemoveReject() {
    this.messageService.clear('removeConfirm');
  }

  private listErrorCatch() {
      return catchError((err: any) => {
        if (err) {
          this.messageService.add({
            key: 'remove-toast',
            severity: 'error',
            summary: 'Erro!',
            detail: `Erro ao carregar a lista!`
          });
        }
        return throwError(err);
      });
  }

}
