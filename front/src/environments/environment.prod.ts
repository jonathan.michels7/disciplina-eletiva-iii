import { environment as def } from "~environments/environment.default";

export const environment: any = {
    ...def,
    production: true,
};